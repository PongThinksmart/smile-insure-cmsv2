@extends('dashboard.dashboard', [
    'activeMenu' => 'model',
    'breadcrumb' => [
        array(
            'name' => 'Cars Management',
            'uri' => ''   
        ),
        array(
            'name' => 'Model List',
            'uri' => '' 
        )
    ]

] )
@section('css')
<link rel="stylesheet" href="{{ asset('css/table.css') }}"/>
@endsection

@section('content')
<!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header ">
            <!-- searching layout -->
            <div class="row">
                <div class="form-group offset-lg-1 col-lg-4 col-md-4">
                    <label for="manuId"><?php echo __('messages.brandcar'); ?></label>
                    <select class="custom-select" id="manuId">
                        @foreach ($selectOptionList['manuList'] as $item)
                            <option value="{{$item['manuId']}}" >{{$item['manuName']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-lg-4 col-md-4">
                    <label for="keyword"><?php echo __('messages.modelcar'); ?></label>
                    <input type="text" class="form-control" id="keyword" >
                </div>
                <div class="form-group col-lg-2 col-md-4">
                    <label for="status"><?php echo __('messages.status'); ?></label>
                    <select class="custom-select" id="status">
                        <option value="0"><?php echo __('messages.all'); ?></option>
                        <option value="1"><?php echo __('messages.active'); ?></option>
                        <option value="2"><?php echo __('messages.inactive'); ?></option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group offset-lg-4 col-lg-4 col-md-12">
                    <button type="button" class="btn btn-primary" style="width:100%;" id="searchButton"><i class="fas fa-search"></i> <?php echo __('messages.search'); ?></button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group offset-lg-10 col-lg-2 col-md-12">
                    <a href="<?php echo url('model'); ?>/new"><button type="button" class="btn btn-success" style="width:100%;" id="createButton"><?php echo __('messages.new'); ?></button></a>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              </table>
            </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#dataTable').on( 'error.dt', function ( e, settings, techNote, message ) {
            $("#alertModalBody").text(message);
            $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
            $("#alertModal").modal('show');
            //console.log( 'An error has been reported by DataTables: ', message );
        })
        .DataTable({
            columns: [
                {   "title": "ลำดับ", "data" : "order" },
                {   "title": "<?php echo __('messages.brandcar'); ?>", "data": "manuName" },
                {   "title": "<?php echo __('messages.modelcar'); ?>", "data": "modelName" },
                {
                    "title": "สถานะ",
                    "data": "status",
                    "render": function (data, type, row) {
                          return (data == '1') ? 'Active' : 'Inactive';
                    }
                },
                {   "title": "วันที่สร้าง", "data": "createDate"},
                {   "title": "",
                    "data": "modelId",
                    "render": function (data, type, row) {
                          return '<a href="<?php echo url('model'); ?>/' + data + '"><i class="fas fa-edit"></i>';
                    }
                },
            ],
            lengthMenu: [ 50, 100, 200, 500 ],
            ordering: false,
            searching: false,
            processing: true,
            serverSide: true,
            //deferLoading: 0,
            language: {
                emptyTable: "<?php echo __('messages.notfounddata'); ?>"
            },
            ajax:{
                url: "<?php echo url('model/getListModel'); ?>",
                type: 'GET',
                data:function (d) {
                    d.manuId = $("#manuId").val();
                    d.keyword = $("#keyword").val();
                    if($("#status").val() == 1){
                        d.status = 1;
                    }else if($("#status").val() == 2){
                        d.status = 0;
                    }
                   
                },
            },
            dom: 'pltip',
        });
        
        $('#searchButton').click( function( e ){
            $('#dataTable').DataTable().draw(true);    
        });  
    });
</script>
@endsection