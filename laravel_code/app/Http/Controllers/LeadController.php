<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Model\Api;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class LeadController extends Controller
{
    public function index(Request $request){
        $isEmpty = $request->has('isError') ? true : false;
        return view('lead.index', [ 'isEmpty' => $isEmpty ]);    
    }
    
    public function exportExcel(Request $request){
        $params = array(
            "startDate" => (date('Y-m-d',strtotime($request->input('startDate'))))." 00:00:00",
            "endDate" => (date('Y-m-d',strtotime($request->input('endDate'))))." 00:00:00"
        );
        $returnData = Api::request('getInquiry', $params);
        if(isset($returnData['result']['inquiries'])){
            $data = $returnData['result']['inquiries'];        
            $spreadsheet = new Spreadsheet();
            $spreadsheet->getDefaultStyle()->getFont()->setName('Tahoma');
            $spreadsheet->getDefaultStyle()->getFont()->setSize(10);
            $spreadsheet->getDefaultStyle()->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);
            $spreadsheet->getDefaultStyle()->getAlignment()->setVertical(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
            $spreadsheet->removeSheetByIndex(0);
            $sheet = $spreadsheet->createSheet()->setTitle('Subscribers');
            $sheet->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
            $kdata = 0;
            $arrayKeydata = array(
                array(
                    "width" => "",
                    "name" => "ลำดับ",
                    "value" => "key",
                ),
                array(
                    "width" => "15",
                    "name" => "ยี่ห้อรถ",
                    "value" => "manuName",
                ),
                array(
                    "width" => "15",
                    "name" => "ชื่อรุ่นรถ",
                    "value" => "modelName",
                ),
                array(
                    "width" => "20",
                    "name" => "ชื่อรุ่นย่อย",
                    "value" => "gradeName",
                ),
                array(
                    "width" => "8",
                    "name" => "ปีรถ",
                    "value" => "modelYear",
                ),
                array(
                    "width" => "30",
                    "name" => "ชื่อผู้สอบถามเบี้ยประกัน",
                    "value" => "contactName",
                ),
                array(
                    "width" => "20",
                    "name" => "เบอร์โทรศัพท์",
                    "value" => "phoneNo",
                ),
                array(
                    "width" => "25",
                    "name" => "วันที่สร้าง",
                    "value" => "createDate",
                ),
                array(
                    "width" => "6",
                    "name" => "1",
                    "value" => "data_2",
                ),
                array(
                    "width" => "6",
                    "name" => "2+",
                    "value" => "data_3",
                ),
                array(
                    "width" => "6",
                    "name" => "2",
                    "value" => "data_4",
                ),
                array(
                    "width" => "6",
                    "name" => "3+",
                    "value" => "data_5",
                ),
                array(
                    "width" => "6",
                    "name" => "3",
                    "value" => "data_6",
                ),
                array(
                    "width" => "6",
                    "name" => "ID",
                    "value" => "campId",
                ),
                array(
                    "width" => "45",
                    "name" => "ชื่อแคมเปญ",
                    "value" => "campName",
                ),
            );
            $keyword = "A";
            foreach($arrayKeydata as $titlekey => $titlevalue){
                if($titlevalue['width'] !== ""){
                    $spreadsheet->getActiveSheet()->getColumnDimension($keyword)->setWidth($titlevalue['width']);
                }
                $keyword++;
            }
            $keyword = "A";
            foreach($arrayKeydata as $titlekey => $titlevalue){
                $spreadsheet->setActiveSheetIndex(0)->setCellValue($keyword.'1', $titlevalue['name']);
                $keyword++;
            }
            foreach($data as $kdata => $vdata){
                $vdata['key'] = $kdata+1;
                $vdata['data_2'] = ' ';
                $vdata['data_3'] = ' ';
                $vdata['data_4'] = ' ';
                $vdata['data_5'] = ' ';
                $vdata['data_6'] = ' ';
                if (count($vdata['inquiryMappings']) > 0) {
                    foreach($vdata['inquiryMappings'] as $mappingkey => $mappingdata){
                        switch($mappingdata['prodTypeId']){
                            case 2:
                                $vdata['data_2'] = 'Y';
                                break;
                            case 3:
                                $vdata['data_3'] = 'Y';
                                break;
                            case 4:
                                $vdata['data_4'] = 'Y';
                                break;
                            case 5:
                                $vdata['data_5'] = 'Y';
                                break;
                            case 6:
                                $vdata['data_6'] = 'Y';
                                break;
                        }
                    }
                }
                //$objPHPExcel->getStyle('B'.($kdata+2))->applyFromArray($style);
                $spreadsheet->getActiveSheet()->getStyle('B'.($kdata+2))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $spreadsheet->getActiveSheet()->getStyle('C'.($kdata+2))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $spreadsheet->getActiveSheet()->getStyle('D'.($kdata+2))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $spreadsheet->getActiveSheet()->getStyle('F'.($kdata+2))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $spreadsheet->getActiveSheet()->getStyle('O'.($kdata+2))->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);
                $keyword = "A";
                foreach($arrayKeydata as $titlekey => $titlevalue){
                    $spreadsheet->setActiveSheetIndex(0)->setCellValue($keyword.($kdata+2), $vdata[$titlevalue['value']]);
                    $keyword++;
                }
            }
            
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment;filename="RepSubscribe.xlsx"');
            header('Cache-Control: max-age=0');
            
            $writer = new Xlsx($spreadsheet);
            $writer->save('php://output');
            
        }else{
            return redirect('lead?isError=1');
        }       
    }
}
