<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Model\Api;

class ViriyaController extends Controller
{
    public function index(Request $request){
        $workType = 0;
        if($request->has('workType')){
            if(intval($request->input('workType')) >= 3){
                $workType = 1;    
            }else{
                $workType = intval($request->input('workType'));
            }
        }
        
        $insureType = 0;
        $insureTypeOther = '';
        if($request->has('insureType')){
            switch(intval($request->input('insureType'))){
                case 4:
                case 6:
                case 8: $insureType = intval($request->input('insureType'));
                        break;
                case 5: $insureType = 9;
                        $insureTypeOther = "2+";
                        break;
                case 7: $insureType = 9;
                        $insureTypeOther = "3+";
                        break;
                default:$insureType = 0;
            }    
        }
        
        $fullAddr = '';
        $zipCode = '';
        if( $request->has('addressListSelect') && $request->has('addressList') ){
            $select = intval($request->input('addressListSelect'));
            foreach( $request->input('addressList') as $key => $item){
                if( $key == $select ){
                    $fullAddr = $item['address'] . " " . $item['distName'] . " " . $item['amphName'] . " " . $item['provName'];
                    $zipCode = $item['zipCode'];    
                }
            }
        }
        
        $phone = '';
        if( $request->has('phoneListSelect') && $request->has('phoneList') ){
            $select = intval($request->input('phoneListSelect'));
            foreach( $request->input('phoneList') as $key => $item){
                if( $key == $select ){
                    $phone = $item['phoneNo'];    
                }
            }
        }
        
        $driverReceive = 0;
        $driverList = array();
        if( $request->has('driverRecrive') && $request->has('driverList') ){
            if($request->input('driverRecrive')){
                foreach( $request->input('driverList') as $key => $item){
                    if(strlen($item['driverName'])){
                        if(!empty($item['birthDate'])){
                            $item['birthDateY'] = date( "Y", strtotime($item['birthDate']) );
                            $item['birthDateM'] = date( "m", strtotime($item['birthDate']) );
                            $item['birthDateD'] = date( "d", strtotime($item['birthDate']) );
                        }
                        if(!empty($item['driveExpiryDate'])){
                            $item['driveExpiryDateY'] = date( "Y", strtotime($item['driveExpiryDate']) );
                            $item['driveExpiryDateM'] = date( "m", strtotime($item['driveExpiryDate']) );
                            $item['driveExpiryDateD'] = date( "d", strtotime($item['driveExpiryDate']) );
                        }
                        $driverList[] = $item;
                        $driverReceive = 1;    
                    }        
                }    
            } 
        }
        
        $receiveDate = [];
        $receiveDate['full'] = $request->has('receiveDate') ? $request->input('receiveDate') : null;
        if(!empty($receiveDate['full'])){
            $receiveDate['Y'] = date( "Y", strtotime($receiveDate['full']) );
            $receiveDate['M'] = date( "m", strtotime($receiveDate['full']) );
            $receiveDate['D'] = date( "d", strtotime($receiveDate['full']) );
        }else{
            $receiveDate['Y'] = null;
            $receiveDate['M'] = null;
            $receiveDate['D'] = null;
        }
        
        $receiveExpireDate = [];
        $receiveExpireDate['full'] = $request->has('receiveExpireDate') ? $request->input('receiveExpireDate') : null;
        if(!empty($receiveExpireDate['full'])){
            $receiveExpireDate['Y'] = date( "Y", strtotime($receiveExpireDate['full']) );
            $receiveExpireDate['M'] = date( "m", strtotime($receiveExpireDate['full']) );
            $receiveExpireDate['D'] = date( "d", strtotime($receiveExpireDate['full']) );
        }else{
            $receiveExpireDate['Y'] = null;
            $receiveExpireDate['M'] = null;
            $receiveExpireDate['D'] = null;
        }
        
        $vehTypeSelect = 1;
        $vehTypeId = 0;
        $vehTypeCode = "";
        $vehTypeName = "";
        if($request->has('vehTypeId')){
            $vehTypeId = intval($request->input('vehTypeId'));
            
            switch($vehTypeId){
                case 67 : 
                case 58 : $vehTypeSelect++;
                case 63 : $vehTypeSelect++;
                case 65 :
                case 62 : 
                case 61 : 
                case 59 : 
                case 57 : $vehTypeSelect++;
                case 64 : 
                case 56 : 
                case 54 : break;
                default : $vehTypeSelect = 5;
                          $vehTypeName = $request->has('vehTypeName') ? $request->input('vehTypeName') : "";
                          break;
            }
            
            switch($vehTypeId){
                case 54 : $vehTypeCode = "110"; break;
                case 55 : $vehTypeCode = "120"; break;
                case 56 : $vehTypeCode = "210"; break;
                case 57 : $vehTypeCode = "220"; break;
                case 58 : $vehTypeCode = "230"; break;
                case 59 : $vehTypeCode = "320"; break;
                case 61 : $vehTypeCode = "420"; break;
                case 62 : $vehTypeCode = "520"; break;
                case 63 : $vehTypeCode = "540"; break;
                case 64 : $vehTypeCode = "610"; break;
                case 65 : $vehTypeCode = "620"; break;
                case 66 : $vehTypeCode = "630"; break;
                case 67 : $vehTypeCode = "730"; break;
            }
        }
        
        $pcw = $request->has('passenger') ? $request->input('passenger') : "" ;
        if($request->has('engineCapacity')){
            $pcw = empty($pcw) ? "-/" . $request->input('engineCapacity') . "/-" : $pcw . "/" . $request->input('engineCapacity') . "/-";        
        }else{
            $pcw = empty($pcw) ? "" : $pcw . "/-/-";
        }
        
        $beneficiary = $request->has('beneficiary') ? $request->input('beneficiary') : null;
        
        $data = array(
            'vehId' => $request->has('vehId') ? intval($request->input('vehId')) : 0,
            'insureNo' => $request->has('insureNo') ? $request->input('insureNo') : null,
            'contractDate' => $request->has('contractDate') ? $request->input('contractDate') : null,
            'email' => $request->has('email') ? $request->input('email') : null,
            'idCardNumber' => $request->has('idCardNumber') ? $request->input('idCardNumber') : null,
            'custName' => $request->has('custName') ? $request->input('custName') : null,
            'receiveExpireDate' => $request->has('receiveExpireDate') ? $request->input('receiveExpireDate') : null,
            'manuName' => $request->has('manuName') ? $request->input('manuName') : null,
            'modelName' => $request->has('modelName') ? $request->input('modelName') : null,
            'vehLicenseNo' => $request->has('vehLicenseNo') ? $request->input('vehLicenseNo') : null,
            'vehProvName' => $request->has('vehProvName') ? $request->input('vehProvName') : null,
            'chasisNo' => $request->has('chasisNo') ? $request->input('chasisNo') : null,
            'engineNo' => $request->has('engineNo') ? $request->input('engineNo') : null,
            'modelYear' => $request->has('modelYear') ? $request->input('modelYear') : null,
            'bodyType' => $request->has('bodyType') ? $request->input('bodyType') : null,
            'totalInsure' => $request->has('totalInsure') ? $request->input('totalInsure') : null,
            'accessory' => $request->has('accessory') ? $request->input('accessory') : null,
            'accessoryPrice' => $request->has('accessoryPrice') ? $request->input('accessoryPrice') : null,
            'extLifeDamagePerPerson' => $request->has('extLifeDamagePerPerson') ? $request->input('extLifeDamagePerPerson') : null,
            'extLifeDamagePerTime' => $request->has('extLifeDamagePerTime') ? $request->input('extLifeDamagePerTime') : null,
            'extAssetDamagePerTime' => $request->has('extAssetDamagePerTime') ? $request->input('extAssetDamagePerTime') : null,
            'extExcessPerTime' => $request->has('extExcessPerTime') ? $request->input('extExcessPerTime') : null,
            'vehDamagePerTime' => $request->has('vehDamagePerTime') ? $request->input('vehDamagePerTime') : null,
            'vehExcessPerTime' => $request->has('vehExcessPerTime') ? $request->input('vehExcessPerTime') : null,
            'vehLostPerTime' => $request->has('vehLostPerTime') ? $request->input('vehLostPerTime') : null,
            'permDisabilityDriver' => $request->has('permDisabilityDriver') ? $request->input('permDisabilityDriver') : null,
            'permDisabilityPassengerPerson' => $request->has('permDisabilityPassengerPerson') ? $request->input('permDisabilityPassengerPerson') : null,
            'permDisabilityPassenger' => $request->has('permDisabilityPassenger') ? $request->input('permDisabilityPassenger') : null,
            'tempDisabilityDriver' => $request->has('tempDisabilityDriver') ? $request->input('tempDisabilityDriver') : null,
            'tempDisabilityPassengerPerson' => $request->has('tempDisabilityPassengerPerson') ? $request->input('tempDisabilityPassengerPerson') : null,
            'tempDisabilityPassenger' => $request->has('tempDisabilityPassenger') ? $request->input('tempDisabilityPassenger') : null,
            'treatmentDriver' => $request->has('treatmentDriver') ? $request->input('treatmentDriver') : null,
            'bailDriver' => $request->has('bailDriver') ? $request->input('bailDriver') : null,
            'vehTypeId' => $request->has('vehTypeId') ? intval($request->input('vehTypeId')) : null,
            'beneficiary' => $beneficiary,
            'beneficiary_name' => $beneficiary ? ($request->has('beneficiary_name') ? $request->input('beneficiary_name') : "") : "",
            'workType' => $workType,
            'insureType' => $insureType,
            'insureTypeOther' => $insureTypeOther,
            'fullAddr' => $fullAddr,
            'zipCode' => $zipCode,
            'phoneNo' => $phone,
            'driverReceive' => $driverReceive,
            'driverList' => $driverList,
            'vehTypeName' => $vehTypeName,
            'vehTypeSelect' => $vehTypeSelect,
            'vehTypeCode' => $vehTypeCode,
            'receiveDate' => $receiveDate,
            'receiveExpireDateD' => $receiveExpireDate['D'],
            'receiveExpireDateM' => $receiveExpireDate['M'],
            'receiveExpireDateY' => $receiveExpireDate['Y'],
            'receiveDateD' => $receiveDate['D'],
            'receiveDateM' => $receiveDate['M'],
            'receiveDateY' => $receiveDate['Y'],
            'pcw' => $pcw,
        );
        return view('viriya.index', ['data' => $data]);    
    }
}
