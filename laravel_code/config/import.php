<?php

return [
    1 => array(
        'name' => "แบบ 1  วัน|เดือน|ปี ค.ศ. Ex. 9|11|2017",
        'format' => null, // data1-data2-data3
        'isCE' => true,
    ),
    2 => array(
        'name' => "แบบ 2  ปี พ.ศ. เดือน วัน Ex. 25601109",
        'format' => "Ymd",
        'isCE' => false,
    ),
    3 => array(
        'name' => "แบบ 3  ปี ค.ศ. เดือน วัน Ex. 20171109",
        'format' => "Ymd",
        'isCE' => true,
    ),
    4 => array(
        'name' => "แบบ 4  วัน/เดือน/ปี พ.ศ. Ex. 9/11/2560",
        'format' => "d/m/y",
        'isCE' => false,
    ),
    5 => array(
        'name' => "แบบ 7  วัน/เดือน/ปี ค.ศ. Ex. 9/11/2017",
        'format' => "d/m/y",
        'isCE' => true,
    ),
    6 => array(
        'name' => "แบบ 5  วัน-ชื่อเดือน-ปี ค.ศ. Ex. 09-NOV-2017",
        'format' => "d-M-y",
        'isCE' => true,
    ),
    7 => array(
        'name' => "แบบ 6 วัน.เดือน.ปี ค.ศ. Ex. 09.11.2017",
        'format' => "d.m.y",
        'isCE' => true,
    ),
];
