<?php

namespace App\Http\Middleware;

use Closure;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if ($request->session()->get('authenticated',false) === true){
            return $next($request);
        }
        $request->session()->forget('authenticated');
        $request->session()->forget('username');
        $request->session()->forget('userlevelId');
        $request->session()->forget('userId');
        $request->session()->forget('agentLevelName');
        $request->session()->forget('nickname');
        $request->session()->forget('agentName');   
        return redirect('login');
    }
}
