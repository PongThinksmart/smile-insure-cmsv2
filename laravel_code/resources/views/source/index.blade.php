@extends('dashboard.dashboard', [
    'activeMenu' => 'source',
    'breadcrumb' => [
        array(
            'name' => 'Customer Management',
            'uri' => ''   
        ),
        array(
            'name' => 'Source List',
            'uri' => '' 
        )
    ]

] )
@section('css')

@endsection

@section('content')
    <div class="card mb-3">
        <div class="card-body">
            <div class="row">
                <div class="form-group offset-lg-10 col-lg-2 col-md-12">
                    <a href="<?php echo url('source'); ?>/new"><button type="button" class="btn btn-success" style="width:100%;" id="createButton"><?php echo __('messages.new'); ?></button></a>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0"></table>
            </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#dataTable').on( 'error.dt', function ( e, settings, techNote, message ) {
            $("#alertModalBody").text(message);
            $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
            $("#alertModal").modal('show');
            //console.log( 'An error has been reported by DataTables: ', message );
        })
        .DataTable({
            columns: [
                {   "title": "ลำดับ", "data" : "order" },
                {   "title": "<?php echo __('messages.typedata'); ?>", "data": "sourceTypeName" },
                {   "title": "<?php echo __('messages.source'); ?>", "data": "sourceName" },
                {   "title": "<?php echo __('messages.abbr'); ?>", "data": "sourceAbbr" },
                {
                    "title": "สถานะ",
                    "data": "status",
                    "render": function (data, type, row) {
                          return (data == '1') ? 'Active' : 'Inactive';
                    }
                },
                {   "title": "<?php echo __('messages.editdate'); ?>", "data": "updateDate"},
                {   "title": "<?php echo __('messages.editauthor'); ?>", "data": "updateAgentName"},
                {   "title": "",
                    "data": "sourceId",
                    "render": function (data, type, row) {
                          return '<a href="<?php echo url('source'); ?>/' + data + '"><i class="fas fa-edit"></i>';
                    }
                },
            ],
            lengthMenu: [ 50, 100, 200, 500 ],
            ordering: false,
            searching: false,
            processing: true,
            serverSide: true,
            ajax:{
                url: "<?php echo url('source/getListSource'); ?>",
                type: 'GET',
                data:function (d) {},
            },
            "dom": 'pltip',
        });  
    });
</script>
@endsection

