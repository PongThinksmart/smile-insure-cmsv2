@extends('dashboard.dashboard', [
    'activeMenu' => 'messenger/list',
    'breadcrumb' => [
        array(
            'name' => 'Messenger Management',
            'uri' => ''   
        ),
        array(
            'name' => 'Messenger List',
            'uri' => '' 
        ),
    ]

] )
@section('css')

@endsection

@section('content')
@if( count($data) <= 0 )
<div class="page-wrap d-flex flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <span class="display-1 d-block">ยังไม่มีข้อมูล ณ ขณะนี้</span>
            </div>
        </div>
    </div>
</div>
@else
<form id="dataForm" method="POST">
{{ csrf_field() }}
<div class="row">
    <div class="form-group offset-lg-8 col-lg-2 offset-md-6 col-md-3">
        <button type="button" class="btn btn-primary" style="width:100%;" id="createButton" onClick="clickForm()">Print</button>
    </div>
    <div class="form-group col-lg-2 col-md-3">
        <button type="button" class="btn btn-success" style="width:100%;" id="createButton">Save</button>
    </div>
    @foreach($data as $item)
    <div class="col-lg-1 mb-3 col-md-1" style="text-align: right;">
        <div class="form-check">
            <input class="form-check-input" name="sentMessengerId[]" type="checkbox" value="{{$item['sentMessengerId']}}" style="transform: scale(4);margin-top: 20px;margin-left: -10px;">
        </div>
    </div>
    <div class="col-lg-5 mb-3 col-md-11">
        <div class="card">
            <input type="hidden" name="sentMessenger[{{$item['sentMessengerId']}}][notificationDate]" value="<?php echo empty($item['notificationDate']) ? '-' : $item['notificationDate']; ?>">
            <div class="card-header form-inline">{{ __('messages.notificationDate') }} : <div class="datared pl-1"><?php echo empty($item['notificationDate']) ? '-' : $item['notificationDate']; ?></div></div>
            <div class="card-body">
                <!--<input type="hidden" name="sentMessengerId" value="{{ $item['sentMessengerId'] }}">-->
                <div class="row">
                    <div class="form-group col-6">
                        <textarea class="form-control detail" name="sentMessenger[{{$item['sentMessengerId']}}][detail]" placeholder="<?php echo __('messages.customerData'); ?>" rows="7" readonly>{{ $item['detail'] }}</textarea>
                        <input type="hidden" name="sentMessenger[{{$item['sentMessengerId']}}][dueDateSM]" value="<?php echo empty($item['dueDateSM']) ? '-' : $item['dueDateSM']; ?>">
                        <input type="hidden" name="sentMessenger[{{$item['sentMessengerId']}}][dueTimeSM]" value="<?php echo empty($item['dueTimeSM']) ? '-' : $item['dueTimeSM']; ?>">
                        <div style="padding-top: 10px;">
                            <span>{{ __('messages.dueDateShort') }} : <span class="datared pl-1"><?php echo empty($item['dueDateSM']) ? '-' : $item['dueDateSM']; ?></span> {{ __('messages.time') }} : <span class="datared pl-1"><?php echo empty($item['dueTimeSM']) ? '-' : $item['dueTimeSM']; ?></span></span>
                        </div>
                        <div class="form-row align-items-center pt-2">
                            <div class="col-auto" style="padding-bottom: 143px;">
                                <label class="form-check-label">{{ __('messages.messengerNote') }}</label>
                            </div>
                            <div class="col">
                                <textarea class="form-control detail" name="sentMessenger[{{$item['sentMessengerId']}}][note]" placeholder="<?php echo __('messages.note'); ?>" rows="7" readonly>{{ $item['note'] }}</textarea>
                            </div>
                        </div> 
                    </div>
                    <div class="form-row col-lg-6" style="height: fit-content;">
                        @if(!empty($item['receiveDocTag']))
                        <input type="hidden" name="sentMessenger[{{$item['sentMessengerId']}}][receiveDocTag]" value="1">
                        <div class="form-group" style="width: 40%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <label class="form-check-label">{{ __('messages.receiveDoc') }}</label>
                            </div>
                        </div>
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <input name="sentMessenger[{{$item['sentMessengerId']}}][receiveDocDetail]" value="{{ $item['receiveDocDetail'] }}" type="text" class="form-control" readonly >
                        </div>
                        @endif
                        
                        @if(!empty($item['sentBill']))
                        <input type="hidden" name="sentMessenger[{{$item['sentMessengerId']}}][sentBill]" value="1">
                        <div class="form-group" style="width: 40%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <label class="form-check-label">{{ __('messages.sentReceipt') }}</label>
                            </div>
                        </div>
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;height: 29px;"></div>
                        @endif
                        
                        @if(!empty($item['sentReq']))
                        <input type="hidden" name="sentMessenger[{{$item['sentMessengerId']}}][sentReq]" value="1">
                        <div class="form-group" style="width: 40%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <label class="form-check-label">{{ __('messages.sentReq') }}</label>
                            </div>
                        </div>
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;height: 29px;"></div>
                        @endif
                        
                        @if(!empty($item['sentAct']))
                        <input type="hidden" name="sentMessenger[{{$item['sentMessengerId']}}][sentAct]" value="1">
                        <div class="form-group" style="width: 40%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <label class="form-check-label">{{ __('messages.sentAct') }}</label>
                            </div>
                        </div>
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;height: 29px;"></div>
                        @endif
                        
                        @if(!empty($item['sentDepartment']))
                        <input type="hidden" name="sentMessenger[{{$item['sentMessengerId']}}][sentDepartment]" value="1">
                        <div class="form-group" style="width: 40%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <label class="form-check-label">{{ __('messages.sentDepartment') }}</label>
                            </div>
                        </div>
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;height: 29px;"></div>
                        @endif
                        
                        @if(!empty($item['receiveFirstInstallmentTag']))
                        <input type="hidden" name="sentMessenger[{{$item['sentMessengerId']}}][receiveFirstInstallmentTag]" value="1">
                        <div class="form-group" style="width: 40%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <label class="form-check-label">{{ __('messages.receiveFirstInstallment') }}</label>
                            </div>
                        </div>
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <input name="sentMessenger[{{$item['sentMessengerId']}}][receiveFirstInstallmentDetail]" value="{{ $item['receiveFirstInstallmentDetail'] }}" type="text" class="form-control" readonly >
                        </div>
                        @endif
                        
                        @if(!empty($item['photographTag']))
                        <input type="hidden" name="sentMessenger[{{$item['sentMessengerId']}}][photographTag]" value="1">
                        <div class="form-group" style="width: 40%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <label class="form-check-label">{{ __('messages.photograph') }}</label>
                            </div>
                        </div>
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <input name="sentMessenger[{{$item['sentMessengerId']}}][photographDetail]" value="{{ $item['photographDetail'] }}" type="text" class="form-control" readonly >
                        </div>
                        @endif
                        
                        @if(!empty($item['receiveLastInstallmentTag']))
                        <input type="hidden" name="sentMessenger[{{$item['sentMessengerId']}}][receiveLastInstallmentTag]" value="1">
                        <div class="form-group" style="width: 40%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <label class="form-check-label">{{ __('messages.receiveLastInstallment') }}</label>
                            </div>
                        </div>
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <input name="sentMessenger[{{$item['sentMessengerId']}}][receiveLastInstallmentDetail]" value="{{ $item['receiveLastInstallmentDetail'] }}" type="text" class="form-control" readonly >
                        </div>
                        @endif
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
</form>
@endif
@endsection

@section('script')
<script type="text/javascript">
    function clickForm(){
        var form = document.getElementById('dataForm');
        form.action = "{{ url('pdf/sentMessenger') }}";
        form.method = "POST";
        form.target = "blank";
        form.submit();
    }
</script>
@endsection

