@extends('dashboard.dashboard', [
    'activeMenu' => 'model/mapping',
    'breadcrumb' => [
        array(
            'name' => 'Cars Management',
            'uri' => ''   
        ),
        array(
            'name' => 'Replace Models Name',
            'uri' => '' 
        )
    ]

] )
@section('css')

@endsection

@section('content')
    <div class="card mb-3">
        <div class="card-header ">
            <!-- searching layout -->
            <div class="row">
                <div class="form-group offset-lg-2 col-lg-2 col-md-4">
                    <label for="lotId"><?php echo __('messages.lotno'); ?></label>
                    <select class="custom-select" id="lotId">
                        @foreach ($selectOptionList['lotId'] as $item)
                            <option value="{{$item['id']}}" >{{$item['name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-lg-3 col-md-4">
                    <label for="manuName"><?php echo __('messages.nearybrandcar'); ?></label>
                    <input type="text" class="form-control" id="manuName" >
                </div>
                <div class="form-group col-lg-3 col-md-4">
                    <label for="modelName"><?php echo __('messages.nearymodelcar'); ?></label>
                    <input type="text" class="form-control" id="modelName" >
                </div>
            </div>
            <div class="row">
                <div class="form-group offset-lg-4 col-lg-4 col-md-12">
                    <button type="button" class="btn btn-primary" style="width:100%;" id="searchButton"><i class="fas fa-search"></i> <?php echo __('messages.search'); ?></button>
                </div>
            </div>
            </div> 
        </div>
        <form method="put" id="formUpdate">
            {{ csrf_field() }}
            <div class="card-body row">
                <div class="table-responsive col-lg-6 col-md-12">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    </table>
                </div>
                <div class="col-lg-6 col-md-12" id="replaceForm">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title">Mismatch</h5>
                            <h6 class="card-subtitle mb-2 text-muted">วิธีใช้: เลือกรุ่นรถยนต์ที่ใกล้เคียงจากทางด้านซ้าย เพื่อแทนที่ด้วยรุ่นรถยนต์ที่มีในระบบ</h6>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="manuId"><?php echo __('messages.brandcar'); ?> <span style="color:red;">*</span></label>
                                <select class="custom-select" id="manuId" name="manuId" required>
                                    <option value="" ><?php echo __('messages.selectbrand'); ?></option>
                                    @foreach ($selectOptionList['manuList'] as $item)
                                        <option value="{{$item['manuId']}}" >{{$item['manuName']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="modelId"><?php echo __('messages.modelcar'); ?> <span style="color:red;">*</span></label>
                                <select class="custom-select" id="modelId" name="modelId" required>
                                    <option value="" ><?php echo __('messages.selectmodel'); ?></option>
                                </select>
                            </div>
                            <button type="submit" id="replaceButton" class="btn btn-primary"><?php echo __('messages.replace'); ?></button>    
                        </div>
                    </div>    
                </div>
            </div>
        </form>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('js/jquery.form.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#dataTable').on( 'error.dt', function ( e, settings, techNote, message ) {
            $("#alertModalBody").text(message);
            $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
            $("#alertModal").modal('show');
            //console.log( 'An error has been reported by DataTables: ', message );
        })
        .DataTable({
            columns: [
                {
                    "title": "เลือก",
                    "render" : function (data, type, full) {
                          return '<input name="checkList[' + full['manuName'] + '][]" type="checkbox" value="' + full['modelName'] + '" class="checkItem">';
                    }
                },
                {   "title": "<?php echo __('messages.brandcar'); ?>", "data": "manuName" },
                {   "title": "<?php echo __('messages.modelcar'); ?>", "data": "modelName" },
            ],
            lengthMenu: [ 50, 100, 200, 500 ],
            ordering: false,
            searching: false,
            processing: true,
            serverSide: true,
            language: {
                emptyTable: "<?php echo __('messages.notfounddata'); ?>"
            },
            ajax:{
                url: "<?php echo url('model/getMismatch'); ?>",
                type: 'GET',
                data:function (d) {
                    d.lotId = $("#lotId").val();
                    d.manuName = $("#manuName").val();
                    d.modelName = $("#modelName").val();
                },
            },
            dom: 'pltip',
            "drawCallback": function( settings ) {
                var api = this.api();
                if ( ! api.data().count() ) {
                    $('#replaceForm :input').prop("disabled", true);
                }else{
                    $('#replaceForm :input').prop("disabled", false);    
                }    
            }
        });
        
        $('#searchButton').click( function( e ){
            $('#dataTable').DataTable().draw(true);    
        });

        $('#manuId').change( function( e ){
            $("#modelId").empty();
            $("#modelId").append('<option value="" ><?php echo __('messages.selectmodel'); ?></option>');
            if( parseInt($(this).val()) > 0  ){                                   
                $.ajax({
                    url: "<?php echo url('model/getAllModel'); ?>?manuId=" + $(this).val(),
                    success: function(resp){
                        jsonObj = resp;
                        if(typeof jsonObj.error !== 'undefined'){
                            $("#alertModalBody").text("Error : " + jsonObj.error.message);    
                            $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                            $("#alertModal").modal('show');    
                        }else if(jsonObj.result != null ){
                            $.each( jsonObj.result.models, function( key, value ) {
                                if( value.status == 1 ){
                                    $("#modelId").append('<option value="' + value.modelId + '" >' + value.modelName + '</option>');    
                                }
                            }); 
                        }           
                    },
                    error:function(resp) {
                        $("#alertModalBody").text("Network Error : " + resp.statusText);
                        $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                        $("#alertModal").modal('show');
                    },
                });
            }   
        });
        
        $("#replaceButton").click(function(){
            $("#formUpdate").ajaxForm({
                url: "<?php echo url('model/replace'); ?>/" + $("#lotId").val(),
                type: "PUT",
                beforeSubmit: function() {
                    var isUnSelected = true;
                    $.each($('.checkItem'), function( k, v ) {
                        if(v.checked){ isUnSelected = false; return false; }
                    });
                    if(isUnSelected){
                        $("#alertModalBody").text("Error : <?php echo __('messages.pleasecheckbox'); ?>");
                        $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                        $("#alertModal").modal('show');
                        return false;        
                    }                 
                },
                success: function(resp) {
                    //var jsonObj = jQuery.parseJSON(resp);
                    jsonObj = resp;
                    if(typeof jsonObj.error !== 'undefined'){
                        $("#alertModalBody").text("Error : " + jsonObj.error.message);    
                        $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                        $("#alertModal").modal('show');    
                    }else{
                        $("#alertModalBody").text("Success : <?php echo __('messages.saved'); ?>");    
                        $("#alertModalLabel").text("<?php echo __('messages.success'); ?>");
                        $("#alertModal")
                        $('#alertModal').on('hidden.bs.modal', function () {
                            window.open("<?php echo url('model/mapping'); ?>","_self");    
                        }).modal('show');
                    }
                },
                error:function(resp) {
                    $("#alertModalBody").text("Network Error : " + resp.statusText);
                    $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                    $("#alertModal").modal('show');
                },
                        
            });  
        });
          
    });
</script>
@endsection

