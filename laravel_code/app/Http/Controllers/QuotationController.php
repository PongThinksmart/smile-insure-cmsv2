<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Model\Api;

class QuotationController extends Controller
{
    
    public static function update($input){
        $driverList = array();
        if($input['isSpecifyDriver']){
            foreach($input['driverList'] as $item){
                if(!empty($item['driverName'])){
                    $item['id'] = !empty($item['id']) ? intval($item['id']) : null;
                    $item['age'] = !empty($item['age']) ? intval($item['age']) : null;
                    $driverList[] = $item;   
                }
            }
        }
        $itemList = array();
        if(!empty($input['itemList'])){
            foreach($input['itemList'] as $item){
                $item['id'] = !empty($item['id']) ? intval($item['id']) : null;
                $item['extLifeDamagePerPerson'] = !empty($item['extLifeDamagePerPerson']) ? floatval($item['extLifeDamagePerPerson']) : null;
                $item['extLifeDamagePerTime'] = !empty($item['extLifeDamagePerTime']) ? floatval($item['extLifeDamagePerTime']) : null;
                $item['extAssetDamagePerTime'] = !empty($item['extAssetDamagePerTime']) ? floatval($item['extAssetDamagePerTime']) : null;
                $item['vehDamagePerTime'] = !empty($item['vehDamagePerTime']) ? floatval($item['vehDamagePerTime']) : null;
                $item['vehLostPerTime'] = !empty($item['vehLostPerTime']) ? floatval($item['vehLostPerTime']) : null;
                $item['vehExcessPerTime'] = !empty($item['vehExcessPerTime']) ? floatval($item['vehExcessPerTime']) : null;
                $item['permDisabilityPassengerPerson'] = !empty($item['permDisabilityPassengerPerson']) ? $item['permDisabilityPassengerPerson'] : null;
                $item['permDisabilityPassenger'] = !empty($item['permDisabilityPassenger']) ? floatval($item['permDisabilityPassenger']) : null;
                $item['treatmentPassenger'] = !empty($item['treatmentPassenger']) ? floatval($item['treatmentPassenger']) : null;
                $item['bailDriver'] = !empty($item['bailDriver']) ? floatval($item['bailDriver']) : null;
                $item['repairTypeName'] = !empty($item['repairTypeName']) ? $item['repairTypeName'] : null;
                $item['promotionCustom1'] = !empty($item['promotionCustom1']) ? $item['promotionCustom1'] : null;
                $item['promotionCustom2'] = !empty($item['promotionCustom2']) ? $item['promotionCustom2'] : null;
                $item['promotionCustom3'] = !empty($item['promotionCustom3']) ? $item['promotionCustom3'] : null;
                $item['voluntaryPrice'] = !empty($item['voluntaryPrice']) ? floatval($item['voluntaryPrice']) : null;
                $item['compulsoryPrice'] = !empty($item['compulsoryPrice']) ? floatval($item['compulsoryPrice']) : null;
                $item['totalPrice'] = !empty($item['totalPrice']) ? floatval($item['totalPrice']) : null;
                $itemList[] = $item;
            }
        }
        $data = array(
            'quotId' => !empty($input['quotId']) ? intval($input['quotId']) : null,
            'vehId' => intval($input['vehId']),
            'refNo' => $input['refNo'],
            'proposedDate' => $input['proposedDate'],
            'promotionStaticRow1' => !empty($input['promotionStaticRow1']) ? $input['promotionStaticRow1'] : "",
            'promotionStaticRow2' => !empty($input['promotionStaticRow2']) ? $input['promotionStaticRow2'] : "",
            'promotionStaticRow3' => !empty($input['promotionStaticRow3']) ? $input['promotionStaticRow3'] : "",
            'updateAgentId' => intval(session('userId',0)),
            'driverList' => $driverList,
            'itemList' => $itemList,
        );
        $returnData = Api::request('saveQuotationDetails', $data);
        return response()->json($returnData);
    }
    
    public function index($id = 0,Request $request){
        $data = array();
        $returnData = null;
        if($id > 0){
            $returnData = Api::request("getQuotationDetailsByVehId", array(
                "vehId" => intval($id),
            ));
            if(!empty($returnData['result'])){
                $returnData = $returnData['result'];
            }
        }else{
            $data = array();
            $data['vehId'] = 0;
        }
        
        $data['vehId'] = intval($id);
        $data['refNo'] = !empty($returnData['refNo']) ? $returnData['refNo'] : "";
        $data['quotId'] = !empty($returnData['quotId']) ? $returnData['quotId'] : null;
        $data['proposedDate'] = !empty($returnData['proposedDate']) ? $returnData['proposedDate'] : "";
        $data['proposedAgentName'] = $request->has('saleAgentName') ? $request->input('saleAgentName') : "";
        $data['custName'] = $request->has('custName') ? $request->input('custName') : "";
        $data['manuName'] = $request->has('manuName') ? $request->input('manuName') : "";
        $data['modelName'] = $request->has('modelName') ? $request->input('modelName') : "";
        $data['engineCapacity'] = $request->has('engineCapacity') ? intval($request->input('engineCapacity')) : 0;
        $data['vehLicenseNo'] = $request->has('vehLicenseNo') ? $request->input('vehLicenseNo') : "";
        $data['vehProvName'] = $request->has('vehProvName') ? $request->input('vehProvName') : "";
        $data['phoneNo'] = !empty($returnData['phoneNo']) ? $returnData['phoneNo'] : "";
        
        if( $request->has('phoneListSelect') && $request->has('phoneList') ){
            $select = intval($request->input('phoneListSelect'));
            foreach( $request->input('phoneList') as $key => $item){
                if( $key == $select ){
                    $data['phoneNo'] = $item['phoneNo'];    
                }
            }
        }
        
        $data['isSpecifyDriver'] = !empty($returnData['isSpecifyDriver']) ? intval($returnData['isSpecifyDriver']) : "";
        $data['driverList'] = array();   
        for($i=0;$i<2;$i++){
            $data['driverList'][$i] = array();    
            $data['driverList'][$i]['id'] = !empty($returnData['driverList'][$i]['id']) ? $returnData['driverList'][$i]['id'] : null;
            $data['driverList'][$i]['driverName'] = !empty($returnData['driverList'][$i]['driverName']) ? $returnData['driverList'][$i]['driverName'] : null;
            $data['driverList'][$i]['age'] = !empty($returnData['driverList'][$i]['age']) ? intval($returnData['driverList'][$i]['age']) : null;
        }
        $data['itemList'] = array();
        for($i=0;$i<3;$i++){
            $data['itemList'][$i] = array();    
            $data['itemList'][$i]['quotItemId'] = !empty($returnData['itemList'][$i]['quotItemId']) ? $returnData['itemList'][$i]['quotItemId'] : null;
            $data['itemList'][$i]['quotItemName'] = !empty($returnData['itemList'][$i]['quotItemName']) ? $returnData['itemList'][$i]['quotItemName'] : null;
            $data['itemList'][$i]['extLifeDamagePerPerson'] = !empty($returnData['itemList'][$i]['extLifeDamagePerPerson']) ? $returnData['itemList'][$i]['extLifeDamagePerPerson'] : null;
            $data['itemList'][$i]['extLifeDamagePerTime'] = !empty($returnData['itemList'][$i]['extLifeDamagePerTime']) ? $returnData['itemList'][$i]['extLifeDamagePerTime'] : null;
            $data['itemList'][$i]['extAssetDamagePerTime'] = !empty($returnData['itemList'][$i]['extAssetDamagePerTime']) ? $returnData['itemList'][$i]['extAssetDamagePerTime'] : null;
            $data['itemList'][$i]['vehDamagePerTime'] = !empty($returnData['itemList'][$i]['vehDamagePerTime']) ? $returnData['itemList'][$i]['vehDamagePerTime'] : null;
            $data['itemList'][$i]['vehLostPerTime'] = !empty($returnData['itemList'][$i]['vehLostPerTime']) ? $returnData['itemList'][$i]['vehLostPerTime'] : null;
            $data['itemList'][$i]['vehExcessPerTime'] = !empty($returnData['itemList'][$i]['vehExcessPerTime']) ? $returnData['itemList'][$i]['vehExcessPerTime'] : null;
            $data['itemList'][$i]['permDisabilityPassengerPerson'] = !empty($returnData['itemList'][$i]['permDisabilityPassengerPerson']) ? $returnData['itemList'][$i]['permDisabilityPassengerPerson'] : null;
            $data['itemList'][$i]['permDisabilityPassenger'] = !empty($returnData['itemList'][$i]['permDisabilityPassenger']) ? $returnData['itemList'][$i]['permDisabilityPassenger'] : null;
            $data['itemList'][$i]['treatmentPassenger'] = !empty($returnData['itemList'][$i]['treatmentPassenger']) ? $returnData['itemList'][$i]['treatmentPassenger'] : null;
            $data['itemList'][$i]['bailDriver'] = !empty($returnData['itemList'][$i]['bailDriver']) ? $returnData['itemList'][$i]['bailDriver'] : null;
            $data['itemList'][$i]['repairTypeName'] = !empty($returnData['itemList'][$i]['repairTypeName']) ? $returnData['itemList'][$i]['repairTypeName'] : null;
            $data['itemList'][$i]['voluntaryPrice'] = !empty($returnData['itemList'][$i]['voluntaryPrice']) ? $returnData['itemList'][$i]['voluntaryPrice'] : null;
            $data['itemList'][$i]['compulsoryPrice'] = !empty($returnData['itemList'][$i]['compulsoryPrice']) ? $returnData['itemList'][$i]['compulsoryPrice'] : null;
            $data['itemList'][$i]['totalPrice'] = !empty($returnData['itemList'][$i]['totalPrice']) ? $returnData['itemList'][$i]['totalPrice'] : null;
            $data['itemList'][$i]['promotionCustom1'] = !empty($returnData['itemList'][$i]['promotionCustom1']) ? $returnData['itemList'][$i]['promotionCustom1'] : null;
            $data['itemList'][$i]['promotionCustom2'] = !empty($returnData['itemList'][$i]['promotionCustom2']) ? $returnData['itemList'][$i]['promotionCustom2'] : null;
            $data['itemList'][$i]['promotionCustom3'] = !empty($returnData['itemList'][$i]['promotionCustom3']) ? $returnData['itemList'][$i]['promotionCustom3'] : null;  
        }
        $data['promotionStaticRow1'] = !empty($returnData['promotionStaticRow1']) ? $returnData['promotionStaticRow1'] : null;
        $data['promotionStaticRow2'] = !empty($returnData['promotionStaticRow2']) ? $returnData['promotionStaticRow2'] : null; 
        $data['promotionStaticRow3'] = !empty($returnData['promotionStaticRow3']) ? $returnData['promotionStaticRow3'] : null;
        return view('quotation.index', ['data' => $data]);    
    }
}
