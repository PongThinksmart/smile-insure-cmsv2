<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Model\Api;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ApproveController extends Controller
{
    public function index(){
        $optionData = Api::request("listWaitingImportLots");
        $selectOptionList['listSources'] = !empty($optionData['result']['importLots']) ? $optionData['result']['importLots'] : array();
        return view('approve.index', [ 'selectOptionList' => $selectOptionList ]);    
    }
    
    public function getCustomerList(Request $request){
        $params = array(
            'lotId' => $request->has('lotId') ? intval($request->input('lotId')) : null,
            'offset' => $request->has('start') ? intval($request->input('start')) : 0 ,
            'limit' => $request->has('length') ? intval($request->input('length')) : 0,
        );
        $returnData = Api::request('getNewCustomerByLotId', $params);
        
        $responseData = array();
        if(!empty($returnData['result'])){
            $total = !empty($returnData['result']['total']) ? intval($returnData['result']['total']) : count($returnData['result']['customers']);
            $listData = $returnData['result']['customers'];
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'recordsTotal' => $total,
                'recordsFiltered' => $total,
                'data' => !empty($listData) ? $listData : [],
                'isMisMatch' => !empty($returnData['result']['totalMismatch']) ? true : false,
            );        
        }else{
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'error' => $returnData['error']['message'],
                'data' => [],
            );    
        }
        return response()->json($responseData);
    }
    
    public function update(Request $request){
        $params = array(
            "lotId" => $request->has('lotId') ? intval($request->input('lotId')) : 0,
            "agentId" => intval(session('userId',0)),
        );
        $status = $request->has('status') ? intval($request->input('status')) : 1;
        $returnData = [
            "id" => null,
            "error" => [
                "code" => 400,
                "message" => "Invalid Status.",
            ],
            "jsonrpc" => "2.0"
        ];
        switch($status){
            case 2:
                $returnData = Api::request("approveNewCustomerForSale",$params);
                break;
            case 3:
                $returnData = Api::request("rejectNewCustomerByLotId",$params);
                break;
        }
        return response()->json($returnData);
    }
}
