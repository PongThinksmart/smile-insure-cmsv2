@extends('dashboard.dashboard', [
    'activeMenu' => 'province/mapping',
    'breadcrumb' => [
        array(
            'name' => 'Cars Management',
            'uri' => ''   
        ),
        array(
            'name' => 'Replace Provinces Name',
            'uri' => '' 
        )
    ]

] )
@section('css')

@endsection

@section('content')
    <div class="card mb-3">
        <div class="card-header ">
            <!-- searching layout -->
            <div class="row">
                <div class="form-group offset-lg-3 col-lg-2 col-md-6">
                    <label for="lotId"><?php echo __('messages.lotno'); ?></label>
                    <select class="custom-select" id="lotId">
                        @foreach ($selectOptionList['lotId'] as $item)
                            <option value="{{$item['id']}}" >{{$item['name']}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-lg-4 col-md-6">
                    <label for="provName"><?php echo __('messages.provincename'); ?></label>
                    <input type="text" class="form-control" id="provName" >
                </div>
            </div>
            <div class="row">
                <div class="form-group offset-lg-4 col-lg-4 col-md-12">
                    <button type="button" class="btn btn-primary" style="width:100%;" id="searchButton"><i class="fas fa-search"></i> <?php echo __('messages.search'); ?></button>
                </div>
            </div>
        </div> 
    </div>
    <form method="put" id="formUpdate">
        {{ csrf_field() }}
        <div class="card-body row">
            <div class="table-responsive col-lg-6 col-md-12">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0"></table>
            </div>
            <div class="col-lg-6 col-md-12" id="replaceForm">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Mismatch</h5>
                        <h6 class="card-subtitle mb-2 text-muted">วิธีใช้: เลือกจังหวัดที่ไม่ถูกต้องจากทางด้านซ้าย เพื่อแทนที่ด้วยจังหวัดที่มีในระบบ</h6>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label for="provId"><?php echo __('messages.provincename'); ?> <span style="color:red;">*</span></label>
                            <select class="custom-select" id="provId" name="provId" required>
                                @foreach ($selectOptionList['listProvinces'] as $item)
                                    <option value="{{$item['id']}}" >{{$item['name']}}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" id="replaceButton" class="btn btn-primary"><?php echo __('messages.replace'); ?></button>    
                    </div>
                </div>    
            </div>
        </div>
    </form>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('js/jquery.form.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#dataTable').on( 'error.dt', function ( e, settings, techNote, message ) {
            $("#alertModalBody").text(message);
            $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
            $("#alertModal").modal('show');
            //console.log( 'An error has been reported by DataTables: ', message );
        })
        .DataTable({
            columns: [
                {
                    "title": "เลือก",
                    "render" : function (data, type, full) {
                          return '<input name="checkList[]" type="checkbox" value="' + full['name'] + '" class="checkItem">';
                    }
                },
                {   "title": "<?php echo __('messages.provincename'); ?>", "data": "name" },
            ],
            lengthMenu: [ 50, 100, 200, 500 ],
            ordering: false,
            searching: false,
            processing: true,
            serverSide: true,
            language: {
                emptyTable: "<?php echo __('messages.notfounddata'); ?>"
            },
            ajax:{
                url: "<?php echo url('province/getMismatch'); ?>",
                type: 'GET',
                data:function (d) {
                    d.lotId = $("#lotId").val();
                    d.provName = $("#provName").val();
                },
            },
            dom: 'pltip',
            "drawCallback": function( settings ) {
                var api = this.api();
                if ( ! api.data().count() ) {
                    $('#replaceForm :input').prop("disabled", true);
                }else{
                    $('#replaceForm :input').prop("disabled", false);    
                }    
            }
        });
        
        $('#searchButton').click( function( e ){
            $('#dataTable').DataTable().draw(true);    
        });

        $("#replaceButton").click(function(){
            $("#formUpdate").ajaxForm({
                url: "<?php echo url('province/replace'); ?>/" + $("#lotId").val(),
                type: "PUT",
                beforeSubmit: function() {
                    var isUnSelected = true;
                    $.each($('.checkItem'), function( k, v ) {
                        if(v.checked){ isUnSelected = false; return false; }
                    });
                    if(isUnSelected){
                        $("#alertModalBody").text("Error : <?php echo __('messages.pleasecheckbox'); ?>");
                        $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                        $("#alertModal").modal('show');
                        return false;        
                    }                 
                },
                success: function(resp) {
                    //var jsonObj = jQuery.parseJSON(resp);
                    jsonObj = resp;
                    if(typeof jsonObj.error !== 'undefined'){
                        $("#alertModalBody").text("Error : " + jsonObj.error.message);    
                        $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                        $("#alertModal").modal('show');    
                    }else{
                        $("#alertModalBody").text("Success : <?php echo __('messages.saved'); ?>");    
                        $("#alertModalLabel").text("<?php echo __('messages.success'); ?>");
                        $("#alertModal")
                        $('#alertModal').on('hidden.bs.modal', function () {
                            window.open("<?php echo url('province/mapping'); ?>","_self");    
                        }).modal('show');
                    }
                },
                error:function(resp) {
                    $("#alertModalBody").text("Network Error : " + resp.statusText);
                    $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                    $("#alertModal").modal('show');
                },
                        
            });  
        });
          
    });
</script>
@endsection

