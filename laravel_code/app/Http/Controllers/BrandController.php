<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Model\Api;

class BrandController extends Controller
{
    public function index(){
        return view('brand.index', []);    
    }
    
    public function new_detail(Request $request){
        return view('brand.detail', [ 'data' => [ 'manuId' => -1 ] ]);    
    }
    
    public function detail($id, Request $request){
        $params = array(
            'manuId' => intval($id),
        );
        $returnData = Api::request('getManufacturerById', $params);
        if($returnData['result']==null){
            $sidebarAndBreadcrumb = [
                'activeMenu' => 'brand',
                'breadcrumb' => [
                array(
                    'name' => 'Cars Management',
                    'uri' => ''   
                ),
                array(
                    'name' => 'Brand List',
                    'uri' => 'brand' 
                ),
                array(
                    'name' => ( intval($id) >= 0 ) ? 'Edit a Brand' : 'Create a new Brand',
                    'uri' => '' 
                )
            ]
            ];
            return view('notfound', [ 'sidebarAndBreadcrumb' => $sidebarAndBreadcrumb, 'backlink' => [ 'url' => url('brand'), 'text' => 'Back to Brand List', ], ] );    
        }
        return view('brand.detail', [ 'data' => $returnData['result'] ]);    
    }
    
    public function getListBrand(Request $request){
        $params = array(
            'keyword' => $request->has('keyword') ? $request->input('keyword') : null,
            'status' => $request->has('status') ? intval($request->input('status')) : null,
            'offset' => $request->has('start') ? intval($request->input('start')) : 0 ,
            'limit' => $request->has('length') ? intval($request->input('length')) : 0,
        );
        $returnData = Api::request('searchManufacturer', $params);
        
        $params = array(
            'keyword' => $request->has('keyword') ? $request->input('keyword') : null,
            'status' => $request->has('status') ? intval($request->input('status')) : null,
        );
        $countdata = Api::request("countManufacturer",$params);
        
        $responseData = array();
        if(!empty($returnData['result'])){
            $total = !empty($countdata['result']['total']) ? intval($countdata['result']['total']) : count($returnData['result']['manufacturers']);
            $listData = $returnData['result']['manufacturers'];
            $i = $request->has('start') ? intval($request->input('start')) : 0;
            if(!empty($listData)){
                foreach($listData as $key => $item){
                    $listData[$key]['order'] = $i+1;
                    $i++;       
                }
            }else{
                $listData = [];   
            }
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'recordsTotal' => $total,
                'recordsFiltered' => $total,
                'data' => $listData,
            );        
        }else{
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'error' => $returnData['error']['message'],
                'data' => [],
            );    
        }
        return response()->json($responseData);
        
    }
    
    public function create(Request $request){  
        $data = array(
            "manuName" => $request->has('manuName') ? $request->input('manuName') : "",
            "status" => $request->has('status') ? intval($request->input('status')) : 1,
            "agentId" => intval(session('userId',0)),
        );
        $returnData = Api::request('addManufacturer', $data);
        return response()->json($returnData);
    }
    
    public function update($id, Request $request){  
        $data = array(
            "manuId" => intval($id),
            "manuName" => $request->has('manuName') ? $request->input('manuName') : "",
            "status" => $request->has('status') ? intval($request->input('status')) : 1,
            "agentId" => intval(session('userId',0)),
        );
        $returnData = Api::request('editManufacturer', $data);
        return response()->json($returnData);
    }
    
}
