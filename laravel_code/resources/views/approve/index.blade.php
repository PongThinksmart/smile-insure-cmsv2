@extends('dashboard.dashboard', [
    'activeMenu' => 'approve',
    'breadcrumb' => [
        array(
            'name' => 'Customer Management',
            'uri' => ''   
        ),
        array(
            'name' => 'Approve New Customer',
            'uri' => '' 
        )
    ]

] )
@section('css')

@endsection

@section('content')
    <div class="card mb-3">
        <div class="card-header ">
            <!-- searching layout -->
            <div class="row">
                <div class="form-group offset-lg-3 col-lg-4 col-md-8">
                    <label for="lotId"><?php echo __('messages.lotno'); ?></label>
                    <select class="custom-select" id="lotId" name="lotId">
                        @foreach ($selectOptionList['listSources'] as $item)
                            <option value="{{$item['id']}}" >{{$item['name']}}</option>
                        @endforeach    
                    </select>
                </div>
                <div class="form-group col-lg-2 col-md-4" style="margin-top: 2rem;">
                    <button type="button" class="btn btn-primary" style="width:100%;" id="searchButton"><i class="fas fa-search"></i> <?php echo __('messages.search'); ?></button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row" id="MisMatch" style="display:none;">
                <div class="form-group col-lg-12 col-md-12 text-right">
                    <span style="color:red;"><?php echo __('messages.mismatchData'); ?></span>
                </div>
            </div>
            <div class="row" id="noMisMatch" style="display:none;">
                <div class="form-group offset-lg-8 col-lg-2 col-md-6">
                    <button type="button" class="btn btn-success" style="width:100%;" id="readyButton"><?php echo __('messages.readyForSale'); ?></button>
                </div>
                <div class="form-group col-lg-2 col-md-6">
                    <button type="button" class="btn btn-danger" style="width:100%;" id="rejectButton"><?php echo __('messages.rejectAll'); ?></button>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              </table>
            </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>
@endsection

@section('script')
<script type="text/javascript">
    var formStatus = 0;
    $(document).ready(function() {
        $('#dataTable').on( 'error.dt', function ( e, settings, techNote, message ) {
            $("#alertModalBody").text(message);
            $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
            $("#alertModal").modal('show');
        })
        .on('xhr.dt', function ( e, settings, json, xhr ) {
            if(json['isMisMatch']){
                $("#MisMatch").show();
                $("#noMisMatch").hide();        
            }else{
                $("#MisMatch").hide();
                $("#noMisMatch").show();
            }
        } )
        .DataTable({
            columns: [
                {   "title": "<?php echo __('messages.name'); ?>", "data": "custName" },
                {   "title": "<?php echo __('messages.phoneno'); ?>", "data": "phoneNo" },
                {   "title": "<?php echo __('messages.licenseNo'); ?>", "data": "vehLicenseNo" },
                {
                    "title": "<?php echo __('messages.brandcar'); ?>",
                    "render": function (data, type, row) {
                        if (row.manuId){
                            return row.manuName;    
                        }else if((row.manuName == null)||(row.manuName == "")){
                            return "<span style=\"color:red;\">-</span>";
                        }else{
                            return "<span style=\"color:red;\">" + row.manuName + "</span>";
                        }
                    }
                },
                {
                    "title": "<?php echo __('messages.modelcar'); ?>",
                    "render": function (data, type, row) {
                        if (row.modelId){
                            return row.modelName;    
                        }else if((row.modelName == null)||(row.modelName == "")){
                            return "<span style=\"color:red;\">-</span>";
                        }else{
                            return "<span style=\"color:red;\">" + row.modelName + "</span>";
                        }
                    }
                },
                {
                    "title": "<?php echo __('messages.provincename'); ?>",
                    "render": function (data, type, row) {
                        if (row.custProvId){
                            return row.provName;    
                        }else if((row.provName == null)||(row.provName == "")){
                            return "<span style=\"color:red;\">-</span>";
                        }else{
                            return "<span style=\"color:red;\">" + row.provName + "</span>";
                        }
                    }
                },
                {   "title": "<?php echo __('messages.taxDate'); ?>", "data": "taxDate" },
                {   "title": "<?php echo __('messages.lastExpireDate'); ?>", "data": "lastExpireDate" },
                /*
                {   "title": "วันที่สร้าง", "data": "createDate"},
                {   "title": "",
                    "data": "modelId",
                    "render": function (data, type, row) {
                          return '<a href="<?php echo url('model'); ?>/' + data + '"><i class="fas fa-edit"></i>';
                    }
                }, */
            ],
            lengthMenu: [ 50, 100, 200, 500 ],
            ordering: false,
            searching: false,
            processing: true,
            serverSide: true,
            deferLoading: 0,
            language: {
                emptyTable: "<?php echo __('messages.notfounddata'); ?>"
            },
            ajax:{
                url: "<?php echo url('approve/getCustomerList'); ?>",
                type: 'GET',
                data:function (d) {
                    d.lotId = $("#lotId").val();
                },
            },
            dom: 'pltip'
        });
        
        $('#searchButton').click( function( e ){
            $('#dataTable').DataTable().draw(true);    
        });
        
        $('#readyButton').click( function( e ){
            approve(2);       
        });
        
        $('#rejectButton').click( function( e ){
            approve(3);        
        });     
    });
    
    function approve(status){
        formStatus = status;
        $("#alertModalBody").text("Confirm Action?");
        $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
        $("#modelSuccess").show();
        $('#alertModal').on('hidden.bs.modal', function () {
            $("#modelSuccess").hide();    
        }).modal('show');
    }
    
    $("#modelSuccess").click(function() {
        $.ajax({
            url: '<?php echo url('approve/update'); ?>',
            type: "PUT",
            data:{
                lotId: $("#lotId").val(),
                status: formStatus,
                _token: "{{ csrf_token() }}",
            },
            success: function(resp) {
                jsonObj = resp;
                if(typeof jsonObj.error !== 'undefined'){
                    $("#alertModalBody").text("Error : " + jsonObj.error.message);    
                    $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                    $("#alertModal").modal('show');    
                }else{
                    window.open("<?php echo url('approve'); ?>","_self");
                }
            },
            error:function(resp) {
                //var jsonObj = jQuery.parseJSON(resp);
                jsonObj = resp;
                $("#alertModalBody").text("Error : " + jsonObj.error.message);    
                $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                $("#alertModal").modal('show');
            },
        });    
    });
    
</script>
@endsection

