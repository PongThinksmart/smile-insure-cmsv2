@extends('dashboard.dashboard', $sidebarAndBreadcrumb )
@section('css')
<style>
.page-wrap {
    min-height: 60vh;
}
</style>
@endsection

@section('content')
<div class="page-wrap d-flex flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <span class="display-1 d-block">404</span>
                <div class="mb-4 lead"><?php echo __('messages.notfounddata'); ?></div>
                <a href="{{ $backlink['url'] }}" class="btn btn-link">{{ $backlink['text'] }}</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        
    });
</script>
@endsection

