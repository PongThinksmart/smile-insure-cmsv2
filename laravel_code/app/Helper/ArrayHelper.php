<?php

namespace App\Helper;

class ArrayHelper
{
    
    private $keyAttr;
    private $sorting;
    
    function sort_count($a, $b) {
        if ($a[$this->keyAttr] === $b[$this->keyAttr]) {
            return 0;
        } elseif($this->sorting == "ASC") {
            return ($a[$this->keyAttr] > $b[$this->keyAttr] ? 1:-1);
        } else{
            return ($a[$this->keyAttr] < $b[$this->keyAttr] ? 1:-1);
        }
    }
    
    public function sortArray($array, $key, $sort = "ASC"){
        $this->keyAttr = $key;
        $this->sorting = $sort;
        uasort($array, array($this, 'sort_count'));
        return $array;      
    }
    
    
}
