<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Model\Api;

class UserController extends Controller
{
    public function index(){
        $returnData = Api::request("listAgentLevels");
        $selectOptionList['agentLevels'] = !empty($returnData['result']['agentLevels']) ? $returnData['result']['agentLevels'] : array();
        return view('user.index', [ 'selectOptionList' => $selectOptionList ]);    
    }
    
    public function new_detail(Request $request){
        // listAgentLevels
        $returnData = Api::request("listAgentLevels");
        $selectOptionList['agentLevels'] = !empty($returnData['result']['agentLevels']) ? $returnData['result']['agentLevels'] : array();
        
        // employmentTypes
        $returnData = Api::request("listEmploymentTypes");
        $selectOptionList['employmentTypes'] = !empty($returnData['result']['employmentTypes']) ? $returnData['result']['employmentTypes'] : array();
        
        return view('user.detail', [ 'data' => [ 'agentId' => -1, 'status' => 1 ], 'selectOptionList' => $selectOptionList ]);    
    }
    
    public function detail($id, Request $request){
        $params = array(
            'agentId' => intval($id),
        );
        $resultData = Api::request('getAgentDetailsById', $params);
        if(empty($resultData['result'])){
            $sidebarAndBreadcrumb = [
                'activeMenu' => 'user',
                'breadcrumb' => [
                    array(
                        'name' => 'User Management',
                        'uri' => ''   
                    ),
                    array(
                        'name' => 'User List',
                        'uri' => 'user' 
                    ),
                    array(
                        'name' => ( intval($id) >= 0 ) ? 'Edit a User' : 'Create a new User',
                        'uri' => '' 
                    )
                ]
            ];
            return view('notfound', [ 'sidebarAndBreadcrumb' => $sidebarAndBreadcrumb, 'backlink' => [ 'url' => url('user'), 'text' => 'Back to User List', ], ] );    
        }
        // listAgentLevels
        $returnData = Api::request("listAgentLevels");
        $selectOptionList['agentLevels'] = !empty($returnData['result']['agentLevels']) ? $returnData['result']['agentLevels'] : array();
        
        // employmentTypes
        $returnData = Api::request("listEmploymentTypes");
        $selectOptionList['employmentTypes'] = !empty($returnData['result']['employmentTypes']) ? $returnData['result']['employmentTypes'] : array();
        
        return view('user.detail', [ 'data' => $resultData['result'], 'selectOptionList' => $selectOptionList ]);    
    }
    
    public function getListUser(Request $request){
        $data = array(
            'keyword' => $request->has('keyword') ? $request->input('keyword') : null,
            'status' => $request->has('status') ? intval($request->input('status')) : null,
            'agentLevelId' => $request->has('agentLevelId') ? intval($request->input('agentLevelId')) : null,
            'offset' => $request->has('start') ? intval($request->input('start')) : 0 ,
            'limit' => $request->has('length') ? intval($request->input('length')) : 0,
        );
        $returnData = Api::request('searchAgents', $data);
        
        $responseData = array();
        if(!empty($returnData['result'])){
            $total = !empty($returnData['result']['total']) ? intval($returnData['result']['total']) : count($returnData['result']['agents']);
            $listData = $returnData['result']['agents'];
            $i = $request->has('start') ? intval($request->input('start')) : 0;
            if(!empty($listData)){
                foreach($listData as $key => $item){
                    $listData[$key]['order'] = $i+1;
                    $i++;       
                }
            }else{
                $listData = [];   
            }
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'recordsTotal' => $total,
                'recordsFiltered' => $total,
                'data' => $listData,
            );        
        }else{
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'error' => $returnData['error']['message'],
                'data' => [],
            );    
        }
        return response()->json($responseData);    
    }
    
    public function create(Request $request){  
        $data = array(
            "agentId" => null,
            "agentLevelId" => $request->has('agentLevelId') ? intval($request->input('agentLevelId')) : null,
            "username" => $request->has('username') ? $request->input('username') : "",
            "password" => $request->has('password') ? base64_encode($request->input('password')) : null,
            "agentName" => $request->has('agentName') ? $request->input('agentName') : "",
            "nickname" => $request->has('nickname') ? $request->input('nickname') : "",
            "phoneNo" => $request->has('phoneNo') ? $request->input('phoneNo') : "",
            "startDate" => $request->has('startDate') ? $request->input('startDate') : null,
            "endDate" => $request->has('endDate') ? $request->input('endDate') : null,
            "employDate" => $request->has('employDate') ? $request->input('employDate') : null,
            "employTypeId" => $request->has('employTypeId') ? intval($request->input('employTypeId')) : null,
            "updateAgentId" => intval(session('userId',0)),
        );
        $returnData = Api::request('saveAgentDetails', $data);
        return response()->json($returnData);
    }
    
    public function update($id, Request $request){  
        $data = array(
            "agentId" => intval($id),
            "agentLevelId" => $request->has('agentLevelId') ? intval($request->input('agentLevelId')) : null,
            "username" => $request->has('username') ? $request->input('username') : "",
            "password" => $request->has('password') ? base64_encode($request->input('password')) : null,
            "agentName" => $request->has('agentName') ? $request->input('agentName') : "",
            "nickname" => $request->has('nickname') ? $request->input('nickname') : "",
            "phoneNo" => $request->has('phoneNo') ? $request->input('phoneNo') : "",
            "startDate" => $request->has('startDate') ? $request->input('startDate') : null,
            "endDate" => $request->has('endDate') ? $request->input('endDate') : null,
            "employDate" => $request->has('employDate') ? $request->input('employDate') : null,
            "employTypeId" => $request->has('employTypeId') ? intval($request->input('employTypeId')) : null,
            "updateAgentId" => intval(session('userId',0)),
        );
        $returnData = Api::request('saveAgentDetails', $data);
        return response()->json($returnData);
    }
    
    public function inactiveUser($id, Request $request){  
        $data = array(
            "agentId" => intval($id),
            "updateAgentId" => intval(session('userId',0)),
        );
        $returnData = Api::request('inactiveAgent', $data);
        return response()->json($returnData);
    }
    
}
