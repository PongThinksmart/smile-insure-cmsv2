<?php
if( $data['vehId'] > 0 ){
    $menu = [
        'activeMenu' => 'customer',
        'breadcrumb' => [
            array(
                'name' => 'Customer Management',
                'uri' => ''   
            ),
            array(
                'name' => 'Customer List',
                'uri' => 'customer' 
            ),
            array(
                'name' => 'Edit a Customer',
                'uri' => '' 
            )
        ]
    ];    
}else{
    $menu = [
        'activeMenu' => 'customer/new',
        'breadcrumb' => [
            array(
                'name' => 'Customer Management',
                'uri' => ''   
            ),
            array(
                'name' => 'Customer List',
                'uri' => 'customer' 
            ),
            array(
                'name' => 'Create a new Customer',
                'uri' => '' 
            )
        ]
    ];
}

?>
@extends('dashboard.dashboard', $menu)
@section('css')
<style>
.centerTable td {
    vertical-align: initial;    
}

.listClose{
    cursor: pointer;
}

#formUpdate{
    -webkit-transition: fadein 5s; /* Safari prior 6.1 */
    transition: opacity 0.5s;
    opacity: 0.01;
}

</style>
@endsection

@section('content')
<form id="formUpdate">
    {{ csrf_field() }}
    <div class="row">
        <div class="col-lg-12 col-md-12 mb-3">
            <div class="card">
                <div class="card-header"><div class="row">
                    <div class="col-lg-12 col-md-12 form-inline mb-1">
                    @if ( $data['vehId'] > 0 )
                    {{ __('messages.detailCustomer') }} : <div class="datared mr-2 ml-2">{{ !empty($data['custName']) ? $data['custName'] : "-" }}</div> |
                    @endif
                    {{ __('messages.sourceTypeName') }} <?php echo $data['vehId']>0 ? '' : '<span style="color:red;">*</span>'; ?> : <select class="form-control mr-2 ml-2" id="sourceId" name="sourceId" {{ $data['vehId']>0 ? 'disabled' : 'required' }}>
                        <option value>เลือกแหล่งที่มาของข้อมูล</option>
                        @foreach ($selectOptionList['listSources'] as $item)
                        <option value="{{ $item['id'] }}" {{ ($data['sourceId']==$item['id']) ? "selected" : "" }}>{{ $item['name'] }}</option>
                        @endforeach
                    </select> |
                    @if ( $data['vehId'] > 0 )
                    {{ __('messages.receiveDate') }} : <div class="datared mr-2 ml-2">{{ !empty($data['receiveDate']) ? $data['receiveDate'] : "-" }}</div> |
                    {{ __('messages.statusName') }} : <div class="datared mr-2 ml-2">{{ !empty($data['statusName']) ? $data['statusName'] : "-" }}</div> |
                    @endif ( $data['vehId'] > 0 )
                    {{ __('messages.saleAgentName') }} : <select class="form-control mr-2 ml-2" id="saleAgentId" name="saleAgentId">
                        <option value="0">เลือกพนักงานที่ต้องการขาย</option>
                        @foreach ($selectOptionList['listSaleAgents'] as $item)
                        <option value="{{ $item['id'] }}" {{ ($data['saleAgentId']==$item['id']) ? "selected" : "" }}>{{ $item['name'] }}</option>
                        @endforeach
                    </select>
                    </div>
                    <div class="col-lg-12 col-md-12 form-inline my-1">
                        <label for="status" class="mr-2 my-1"><?php echo __('messages.statusCustomer'); ?></label>
                        <select class="form-control mr-2 my-1" id="status" name="status">
                            @foreach ($selectOptionList['customerStatus'] as $item)
                            <option value="{{ $item['custStatusid'] }}" {{ ($data['status']==$item['custStatusid']) ? "selected" : "" }}>{{ $item['custStatusid'] }} - {{ $item['custStatusName'] }}</option>
                            @endforeach
                        </select>
                        <label for="status" class="mr-2 my-1" ><?php echo __('messages.dueDate'); ?></label>
                        <input data-provide="datepicker" type="text" class="form-control mr-2 my-1" id="dueDate" name="dueDate" value="{{$data['dueDate']}}" placeholder="<?php echo __('messages.dueDatePlaceholder'); ?>">
                        <label for="dueDateHours" class="mr-2 my-1"><?php echo __('messages.hours'); ?></label>
                        <select class="form-control mr-2 my-1" id="dueDateHours" name="dueDateHours">
                            @for ($i = 0; $i < 24; $i++)
                            <option value="{{$i}}" {{ ($data['dueDateHours']==$i) ? "selected" : "" }} >{{ ($i<10) ? "0" . $i : $i }}</option>
                            @endfor
                        </select>
                        <label for="dueDateMinutes" class="mr-2 my-1"><?php echo __('messages.minutes'); ?></label>
                        <select class="form-control mr-2 my-1" id="dueDateMinutes" name="dueDateMinutes">
                            @for ($i = 0; $i < 60; $i++)
                            <option value="{{$i}}" {{ ($data['dueDateMinutes']==$i) ? "selected" : "" }} >{{ ($i<10) ? "0" . $i : $i }}</option>
                            @endfor
                        </select>
                        <button type="submit" class="btn btn-success mr-2 my-1 col-2 saveButton" ><?php echo ($data['vehId'] > 0) ? __('messages.save') : __('messages.create'); ?></button>    
                    </div>
                </div></div>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <ul class="list-group">
                <li class="list-group-item" style="padding: 0.5rem;"><div class="row">
                    <div class="form-group col-8">
                        <label for="staffNote"><?php echo __('messages.note'); ?></label>
                        <textarea class="form-control" id="staffNote" name="staffNote" placeholder="<?php echo __('messages.note'); ?>" rows="7">{{ $data['staffNote'] }}</textarea>
                    </div>
                    <div class="form-group col-4 listpdf">
                    @if($data['vehId'] > 0)
                        <button type="button" class="btn btn-danger btn-block" onclick="pdfform('sqa')">{{ __('messages.createSqaForm') }}</button>
                        <button type="button" class="btn btn-primary btn-block" onclick="quotation()">{{ __('messages.createQuotation') }}</button>
                        <button type="button" class="btn btn-primary btn-block" id="viriya" onclick="viriyaform()">{{ __('messages.createViriya') }}</button>
                        <button type="button" class="btn btn-primary btn-block" onclick="pdfform('invoice')">{{ __('messages.createInvoice') }}</button>
                        <button type="button" class="btn btn-primary btn-block" onclick="pdfform('bill')">{{ __('messages.createMultiBill') }}</button>
                    @endif
                    </div>
                    <div class="form-group col-6">
                        <label for="custName">{{ __('messages.customer') }} {{ __('messages.name') }} <span style="color:red;">*</span></label>
                        <input type="text" class="form-control cloneDes" name="custName" id="custName" value="{{$data['custName']}}" required>
                    </div>
                    <div class="form-group col-6">
                        <label for="idCardNumber">{{ __('messages.idCardNo') }}</label>
                        <input type="text" class="form-control" name="idCardNumber" id="idCardNumber" value="{{$data['idCardNo']}}">
                    </div>
                    <div class="form-group col-6">
                        <label for="birthDate">{{ __('messages.birthDate') }}</label>
                        <input data-provide="datepicker" type="text" class="form-control" name="birthDate" id="birthDate" placeholder="{{ __('messages.birthDate') }}" value="{{$data['birthDate']}}">
                    </div>
                    <div class="form-group col-6">
                        <label for="email">{{ __('messages.email') }}</label>
                        <input type="email" class="form-control" name="email" id="email" value="{{$data['email']}}">
                    </div>
                    <div class="table-responsive col-6">
                        <table class="table table-striped" border="2" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 10%;"></th>
                                    <th scope="col" style="width: 45%;">{{ __('messages.name') }}</th>
                                    <th scope="col" style="width: 35%;">{{ __('messages.phoneno') }}</th>
                                    <th scope="col" style="width: 10%;"></th>
                                </tr>
                            </thead>
                            <tbody id="phoneList">
                                @if(!empty($data['phoneList']))
                                @foreach ($data['phoneList'] as $item)
                                <tr class="centerTable">
                                    <td class="text-center"><input class="cloneDes" type="radio" name="phoneListSelect" id="phoneListSelect_{{$loop->index+1}}" value="{{$loop->index+1}}" <?php echo $item['isDefault'] ? "checked" : ""; ?>></td>
                                    <td><input type="text" class="form-control-plaintext" name="phoneList[{{$loop->index+1}}][contactName]" value="{{$item['contactName']}}"></td>
                                    <td><input type="text" id="phoneList_{{$loop->index+1}}_phoneNo" class="cloneDes form-control-plaintext" name="phoneList[{{$loop->index+1}}][phoneNo]" value="{{$item['phoneNo']}}"></td>
                                    <td class="text-center">
                                        <i class="cloneDes listClose fas fa-times"></i>
                                        <input type="hidden" name="phoneList[{{$loop->index+1}}][id]" value="{{$item['id']}}">
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                        <button type="button" id="phoneListAdd" class="btn btn-success btn-block mb-3">{{ __('messages.add') }}</button>
                    </div>
                    <div class="table-responsive col-6">
                        <table class="table table-striped" border="2" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 5%;"></th>
                                    <th scope="col" style="width: 30%;">{{ __('messages.name') }}</th>
                                    <th scope="col" style="width: 30%;">{{ __('messages.lineid') }}</th>
                                    <th scope="col" style="width: 30%;">{{ __('messages.linename') }}</th>
                                    <th scope="col" style="width: 5%;"></th>
                                </tr>
                            </thead>
                            <tbody id="lineList">
                                @if(!empty($data['lineList']))
                                @foreach ($data['lineList'] as $item)
                                <tr class="centerTable">
                                    <td class="text-center"><input type="radio" name="lineListSelect" id="lineListSelect_{{$loop->index+1}}" value="{{$loop->index+1}}" <?php echo $item['isDefault'] ? "checked" : ""; ?>></td>
                                    <td><input type="text" class="form-control-plaintext" name="lineList[{{$loop->index+1}}][contactName]" value="{{$item['contactName']}}"></td>
                                    <td><input type="text" class="form-control-plaintext" name="lineList[{{$loop->index+1}}][lineId]" value="{{$item['lineId']}}"></td>
                                    <td><input type="text" class="form-control-plaintext" name="lineList[{{$loop->index+1}}][lineName]" value="{{$item['lineName']}}"></td>
                                    <td class="text-center">
                                        <i class="listClose fas fa-times"></i>
                                        <input type="hidden" name="lineList[{{$loop->index+1}}][id]" value="{{$item['id']}}">
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                        <button type="button" id="lineListAdd" class="btn btn-success btn-block mb-3">{{ __('messages.add') }}</button>
                    </div>
                    
                    <div class="table-responsive col-12">
                        <table class="table table-striped table-bordered" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col" style="width: 3%;"></th>
                                    <th scope="col" style="width: 16%;"></th>
                                    <th scope="col" style="width: 29%;">{{ __('messages.address') }}</th>
                                    <th scope="col" style="width: 13%;">{{ __('messages.distName') }}</th>
                                    <th scope="col" style="width: 13%;">{{ __('messages.amphName') }}</th>
                                    <th scope="col" style="width: 13%;">{{ __('messages.provName') }}</th>
                                    <th scope="col" style="width: 10%;">{{ __('messages.zipCode') }}</th>
                                    <th scope="col" style="width: 3%;"></th>
                                </tr>
                            </thead>
                            <tbody id="addrList">
                                @if(!empty($data['addressList']))
                                @foreach ($data['addressList'] as $item)
                                <tr class="centerTable">
                                    <td class="text-center"><input class="cloneDes" type="radio" name="addressListSelect" id="addressListSelect_{{$loop->index+1}}" value="{{$loop->index+1}}" <?php echo $item['isDefault'] ? "checked" : ""; ?>></td>
                                    <td>
                                        <select class="form-control mr-2 my-1" name="addressList[{{$loop->index+1}}][addrTypeId]">
                                            <?php foreach ($selectOptionList['addressTypes'] as $subitem) { ?>
                                            <option value="{{ $subitem['id'] }}" {{ ($item['addrTypeId']==$subitem['id']) ? "selected" : "" }}>{{ $subitem['name'] }}</option>
                                            <?php } ?>
                                        </select>
                                    </td>
                                    <td><input type="text" class="cloneDes form-control-plaintext" name="addressList[{{$loop->index+1}}][address]" id="addressList_{{$loop->index+1}}_address" value="{{$item['address']}}"></td>
                                    <td><input type="text" class="cloneDes form-control-plaintext" name="addressList[{{$loop->index+1}}][distName]" id="addressList_{{$loop->index+1}}_distName" value="{{$item['distName']}}"></td>
                                    <td><input type="text" class="cloneDes form-control-plaintext" name="addressList[{{$loop->index+1}}][amphName]" id="addressList_{{$loop->index+1}}_amphName" value="{{$item['amphName']}}"></td>
                                    <td><input type="text" class="cloneDes form-control-plaintext" name="addressList[{{$loop->index+1}}][provName]" id="addressList_{{$loop->index+1}}_provName" value="{{$item['provName']}}"></td>
                                    <td><input type="text" class="cloneDes form-control-plaintext" name="addressList[{{$loop->index+1}}][zipCode]" id="addressList_{{$loop->index+1}}_zipCode" value="{{$item['zipCode']}}"></td>
                                    <td class="text-center">
                                        <i class="cloneDes listClose fas fa-times"></i>
                                        <input type="hidden" name="addressList[{{$loop->index+1}}][addrId]" value="{{$item['addrId']}}">
                                    </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                        </table>
                        <button type="button" id="addrListAdd" class="btn btn-success btn-block mb-3">{{ __('messages.add') }}</button>
                    </div>
                </div></li>
                <li class="list-group-item" style="padding: 0.5rem;"><div class="row">
                    <div class="form-row col-lg-8 col-md-12">
                        <div class="form-group col-12">
                            <label for="vehTypeId">{{ __('messages.vehTypeId') }}</label>
                            <select class="form-control mr-2 my-1" name="vehTypeId" id="vehTypeId">
                                @foreach ($selectOptionList['vehicleTypes'] as $subitem)
                                <option value="{{ $subitem['id'] }}" {{ ($data['vehTypeId']==$subitem['id']) ? "selected" : "" }}>{{ $subitem['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-4">
                            <label for="manuId">{{ __('messages.brandcar') }}  <span style="color:red;">*</span></label>
                            <select class="form-control selectpicker" id="manuId" name="manuId" data-live-search="true" required>
                                <option value="">{{ __('messages.pleaseSelectManu') }}</option>
                                @foreach ($selectOptionList['listManu'] as $subitem)
                                <option value="{{ $subitem['manuId'] }}" {{ $data['manuId']==$subitem['manuId'] ? "selected" : "" }}>{{ $subitem['manuName'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-4">
                            <label for="modelId">{{ __('messages.modelcar') }}  <span style="color:red;">*</span></label>
                            <select class="form-control selectpicker" id="modelId" name="modelId" data-live-search="true" required>
                                <option value="">{{ __('messages.pleaseSelectModel') }}</option>
                                <?php foreach ($selectOptionList['listModel'] as $subitem){ ?>
                                <option value="{{ $subitem['modelId'] }}" {{ $data['modelId']==$subitem['modelId'] ? "selected" : "" }}>{{ $subitem['modelName'] }}</option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group col-4">
                            <label for="vehLicenseNo">{{ __('messages.licenseNo') }}  <span style="color:red;">*</span></label>
                            <input value="{{$data['vehLicenseNo']}}" type="text" class="form-control" id="vehLicenseNo" name="vehLicenseNo" required >
                        </div>
                        <div class="form-group col-5">
                            <label for="chasisNo">{{ __('messages.chasisNo') }}</label>
                            <input value="{{$data['chasisNo']}}" type="text" class="form-control" id="chasisNo" name="chasisNo">
                        </div>
                        <button class="btn btn-primary col-3" type="button" id="checkChasisNo" style="height: max-content;margin-top: 1.7rem;">ตรวจเลขถัง</button>
                        <div class="form-group col-4">
                            <label for="vehProvId">{{ __('messages.vehProvId') }}</label>
                            <select class="form-control selectpicker" id="vehProvId" name="vehProvId" data-live-search="true">
                                @foreach ($selectOptionList['provinces'] as $subitem)
                                <option value="{{ $subitem['id'] }}" {{ ($data['vehProvId']==$subitem['id']) ? "selected" : "" }}>{{ $subitem['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-6">
                            <label for="engineCapacity">{{ __('messages.engineCapacity') }}</label>
                            <input value="{{$data['engineCapacity']}}" type="text" class="form-control" id="engineCapacity" name="engineCapacity">
                        </div>
                        <div class="form-group col-6">
                            <label for="engineNo">{{ __('messages.engineNo') }}</label>
                            <input value="{{$data['engineNo']}}" type="text" class="form-control" id="engineNo" name="engineNo">
                        </div>
                        <div class="form-group col-5">
                            <label for="modelYear">{{ __('messages.modelYear') }}</label>
                            <input value="{{$data['modelYear']}}" type="text" class="form-control" id="modelYear" name="modelYear">
                        </div>
                        <div class="form-group col-2">
                            <label for="passenger">{{ __('messages.passenger') }}</label>
                            <input value="{{$data['passenger']}}" type="text" class="form-control" id="passenger" name="passenger">
                        </div>
                        <div class="form-group col-5">
                            <label for="bodyType">{{ __('messages.bodyType') }}</label>
                            <input value="{{$data['bodyType']}}" type="text" class="form-control" id="bodyType" name="bodyType">
                        </div>
                    </div>
                    <div class="form-row col-lg-4 col-md-12">
                        <div class="form-group col-12">
                            <label for="accessory">{{ __('messages.accessory') }}</label>
                            <textarea class="form-control" id="accessory" name="accessory" rows="12" placeholder="{{ __('messages.accessory') }}">{{$data['accessory']}}</textarea>    
                        </div>
                        <div class="form-group col-12">
                            <label for="accessoryPrice">{{ __('messages.accessoryPrice') }}</label>
                            <input value="{{$data['accessoryPrice']}}" type="text" class="form-control" id="accessoryPrice" name="accessoryPrice">
                        </div>
                    </div>
                    <div class="form-row col-lg-12 col-md-12">
                        <div class="form-group col-4">
                            <label for="taxDate">{{ __('messages.taxPayDate') }}</label>
                            <input data-provide="datepicker" value="{{$data['taxDate']}}" type="text" class="form-control" id="taxDate" name="taxDate">
                        </div>
                        <div class="form-group col-4">
                            <label for="lastExpireDate">{{ __('messages.lastExpireDate') }}</label>
                            <input data-provide="datepicker" value="{{$data['lastExpireDate']}}" type="text" class="form-control" id="lastExpireDate" name="lastExpireDate">
                        </div>
                        <div class="form-group col-4">
                            <label for="buyDate">{{ __('messages.buyDate') }}</label>
                            <input data-provide="datepicker" value="{{$data['buyDate']}}" type="text" class="form-control" id="buyDate" name="buyDate">
                        </div>
                        <div class="form-group col-12">
                            <label for="lastPartnerName">{{ __('messages.lastPartnerName') }}</label>
                            <input value="{{$data['lastPartnerName']}}" type="text" class="form-control" id="lastPartnerName" name="lastPartnerName">
                        </div>
                    </div>
                </div></li>
            </ul>
        </div>
        <div class="col-lg-6 col-md-12" >
            <div class="card-group">
                <div class="card" style="padding: 0.5rem;">
                    <div class="row">
                        <div class="form-group col-12">
                            <label for="insureNo">{{ __('messages.insureNo') }}</label>
                            <input name="insureNo" value="{{ $data['insureNo'] }}" type="text" class="form-control" id="insureNo" >
                        </div>
                        <div class="form-group col-12">
                            <label for="postNo">{{ __('messages.postNo') }}</label>
                            <input name="postNo" value="{{ $data['postNo'] }}" type="text" class="form-control" id="postNo" >
                        </div>
                        <div class="form-group col-12">
                            <label for="workType">{{ __('messages.workType') }}</label><br/>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" name="workType" type="radio" name="inlineRadioOptions" id="workType_1" value="1" {{ $data['workType'] == 1 ? "checked" : "" }} >
                                <label class="form-check-label" for="workType_1">{{ __('messages.workType_1') }}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" name="workType" type="radio" name="inlineRadioOptions" id="workType_2" value="2" {{ $data['workType'] == 2 ? "checked" : "" }} >
                                <label class="form-check-label" for="workType_2">{{ __('messages.workType_2') }}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" name="workType" type="radio" name="inlineRadioOptions" id="workType_3" value="3" {{ $data['workType'] == 3 ? "checked" : "" }} >
                                <label class="form-check-label" for="workType_3">{{ __('messages.workType_3') }}</label>
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="insureType">{{ __('messages.insureType') }}</label>
                            <select class="form-control" name="insureType" id="insureType">
                                <option value="0">{{ __('messages.pleaseSelectInsureType') }}</option>
                                @foreach ($selectOptionList['voluntaryProductTypes'] as $subitem)
                                    <option value="{{ $subitem['id'] }}" {{ ($data['insureType']==$subitem['id']) ? "selected" : "" }}>{{ $subitem['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-12">
                            <label for="partnerType">{{ __('messages.partnerType') }}</label>
                            <select class="form-control" id="partnerType" name="partnerType">
                                <option value="0">{{ __('messages.pleaseSelectPartnerType') }}</option>
                                @foreach ($selectOptionList['partners'] as $subitem)
                                    <option value="{{ $subitem['id'] }}" {{ ($data['partnerType']==$subitem['id']) ? "selected" : "" }}>{{ $subitem['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-12">
                            <label for="insureRate">{{ __('messages.insureRate') }}</label>
                            <input type="text" class="form-control" id="insureRate" name="insureRate" value="{{ $data['insureRate'] }}">
                        </div>
                        <div class="form-group col-12">
                            <label for="repairType">{{ __('messages.repairType') }}</label><br/>
                            @foreach ($selectOptionList['repairTypes'] as $subitem)
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="repairType" id="repairType_{{$subitem['id']}}" value="{{$subitem['id']}}" {{ ($data['repairType']==$subitem['id']) ? "checked" : "" }}>
                                <label class="form-check-label" for="repairType_{{$subitem['id']}}">{{$subitem['name']}}</label>
                            </div>
                            @endforeach
                        </div>
                        <div class="form-group col-12">
                            <label for="receiveDate">{{ __('messages.receiveDate2') }}</label><br>
                            <div class="form-inline">
                                <input data-provide="datepicker" name="receiveDate" type="text" class="form-control col-5" id="receiveDate" value="{{$data['receiveDate']}}">
                                <span class="col-2 text-center"> - </span>
                                <input data-provide="datepicker" name="receiveExpireDate" type="text" class="form-control col-5" id="receiveExpireDate"  value="{{$data['receiveExpireDate']}}">
                            </div>
                        </div>
                        <div class="form-group col-12">
                            <label for="contractDate">{{ __('messages.contractDate') }}</label>
                            <input data-provide="datepicker" name="contractDate" type="text" class="form-control" id="contractDate" value="{{$data['contractDate']}}">
                        </div>
                        <div class="col-12"><button id="unlockRate" type="button" class="btn btn-danger btn-block" data-toggle="modal" data-target="#insuaranceModal">{{ __('messages.unlockRate') }}</button></div>
                        <div class="form-group col-12 mt-3">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="driverRecrive" id="driverRecrive_0" value="0" {{ ($data['driverRecrive']==0) ? "checked" : "" }} >
                                <label class="form-check-label" for="driverRecrive_0">{{ __('messages.driverRecriveNo') }}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="driverRecrive" id="driverRecrive_1" value="1" {{ ($data['driverRecrive']==1) ? "checked" : "" }} >
                                <label class="form-check-label" for="driverRecrive_1">{{ __('messages.addNote') }}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <button id="unlockDriver" type="button" class=" btn btn-danger btn-block" data-toggle="modal" data-target="#driverModal">{{ __('messages.unlockDriver') }}</button>
                            </div>    
                        </div>
                        <div class="form-group col-12">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="beneficiary" id="beneficiary_0" value="0" {{ ($data['beneficiary']==0) ? "checked" : "" }}>
                                <label class="form-check-label" for="beneficiary_1">{{ __('messages.beneficiaryNo') }}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="beneficiary" id="beneficiary_1" value="1" {{ ($data['beneficiary']==1) ? "checked" : "" }}>
                                <label class="form-check-label" for="beneficiary_2">{{ __('messages.has') }}</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input name="beneficiary_id" type="hidden" id="beneficiary_id" value="{{$data['beneficiary_id']}}">
                                <input name="beneficiary_name" style="width: 12rem;" type="text" class="form-check form-check-inline form-control" id="beneficiary_name" value="{{$data['beneficiary_name']}}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card" style="padding: 0.5rem;">
                    <div class="row">
                        <div class="form-group col-12">
                            <label for="actPostcode">{{ __('messages.postNo') }}</label>
                            <input name="actPostcode" type="text" class="form-control" id="actPostcode" value="{{ $data['actPostcode'] }}">
                        </div>
                        <div class="form-group col-12">
                            <label for="actType">{{ __('messages.act') }}</label>
                            <select class="form-control" name="actType" id="actType">
                                <option value="0">{{ __('messages.noAct') }}</option>
                                @foreach ($selectOptionList['compulsoryProductTypes'] as $subitem)
                                    <option value="{{ $subitem['id'] }}" {{ ($data['actType']==$subitem['id']) ? "selected" : "" }}>{{ $subitem['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-12">
                            <label for="actPartnerType">{{ __('messages.partnerType') }}</label>
                            <select class="form-control" id="actPartnerType" name="actPartnerType">
                                <option value="0">{{ __('messages.pleaseSelectPartnerType') }}</option>
                                @foreach ($selectOptionList['partners'] as $subitem)
                                    <option value="{{ $subitem['id'] }}" {{ ($data['actPartnerType']==$subitem['id']) ? "selected" : "" }}>{{ $subitem['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-12">
                            <label for="actReceiveDate">{{ __('messages.actReceiveDate') }}</label><br>
                            <div class="form-inline">
                                <input data-provide="datepicker" type="text" class="form-control col-5" id="actReceiveDate" name="actReceiveDate" value={{$data['actReceiveDate']}}>
                                <span class="col-2 text-center"> - </span>
                                <input data-provide="datepicker" type="text" class="form-control col-5" id="actReceiveExpireDate" name="actReceiveExpireDate" value={{$data['actReceiveExpireDate']}}>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card"><div class="row">
                <div class="mt-3" style="padding: 0.5rem;width: 40%;">
                    <div class="form-group col-12">
                        <label for="orderStatus">{{ __('messages.orderStatus') }}</label>
                        <select class="form-control" id="orderStatus" name="orderStatus">
                            @foreach ($selectOptionList['orderStatus'] as $subitem)
                                <option value="{{ $subitem['id'] }}" {{ ($data['orderStatus']==$subitem['id']) ? "selected" : "" }}>{{ $subitem['name'] }}</option>
                            @endforeach    
                        </select>
                    </div>
                    <div class="table-responsive col-12">
                        <table class="table table-borderless table-money" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th scope="col"></th>
                                    <th scope="col">ประกันภัย</th>
                                    <th scope="col">พรบ.</th>
                                </tr>
                            </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row" class="text-right">เบี้ยสุทธิ</th>
                                        <td><input name="changeOfInsure" type="text" class="form-control text-right-input" id="changeOfInsure" value="{{$data['changeOfInsure']}}" readonly></td>
                                        <td><input name="changeOfCaract" type="text" class="form-control text-right-input" id="changeOfCaract" value="{{$data['changeOfCaract']}}" readonly></td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-right">อากร</th>
                                        <td><input name="dutyInsure" type="text" class="form-control text-right-input" id="dutyInsure" value="{{$data['dutyInsure']}}" readonly></td>
                                        <td><input name="dutyCaract" type="text" class="form-control text-right-input" id="dutyCaract" value="{{$data['dutyCaract']}}" readonly></td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-right">ภาษี</th>
                                        <td><input name="taxInsure" type="text" class="form-control text-right-input" id="taxInsure" value="{{$data['taxInsure']}}" readonly></td>
                                        <td><input name="taxCaract" type="text" class="form-control text-right-input" id="taxCaract" value="{{$data['taxCaract']}}" readonly></td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-right">เบี้ยรวม</th>
                                        <td><input name="totalInsure" type="text" class="form-control text-right-input" id="totalInsure" value="{{$data['totalInsure']}}"></td>
                                        <td><input name="totalCaract" type="text" class="form-control text-right-input" id="totalCaract" value="{{$data['totalCaract']}}"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-right">ส่วนลดเงินสด</th>
                                        <td><input name="agentDiscount" type="text" class="form-control text-right-input" id="agentDiscount" value="{{$data['agentDiscount']}}"></td>
                                        <td>
                                            <div class="form-check">
                                                <input name="freeCarAct" class="form-check-input" type="checkbox" value="1" id="freeCarAct" {{ ($data['freeCarAct']==1) ? "checked" : "" }}>
                                                <label class="form-check-label" for="freeCarAct">{{ __('messages.freeCarAct') }}</label>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-right">รวมชำระ</th>
                                        <td colspan="2"><input name="totalPaymentInsure" type="text" class="form-control text-right-input" id="totalPaymentInsure" value="{{$data['totalPaymentInsure']}}" readonly></td>
                                    </tr>
                                    <tr>
                                        <th scope="row" class="text-right">แคมเปญพิเศษ</th>
                                        <td colspan="2"><textarea name="campaignSpecialInsure" class="form-control" id="campaignSpecialInsure" rows="5">{{$data['campaignSpecialInsure']}}</textarea></td>
                                    </tr>
                                    <tr>
                                        <th colspan="2" scope="row" class="text-right">มูลค่าของแถมรวม</th>
                                        <td><input name="totalGiftFree" type="text" class="form-control text-right-input" id="totalGiftFree" value="{{$data['totalGiftFree']}}"></td>
                                    </tr>
                                </tbody>
                        </table>
                    </div>
                </div>
                <div class=" mt-3" style="padding: 0.2rem;width: 60%;">
                    <div class="form-group col-12">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payType" id="payType_1" value="1" {{ ($data['payType']==1) ? "checked" : "" }}>
                            <label class="form-check-label" for="payType_1">{{ __('messages.fullBuy') }}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="payType" id="payType_2" value="2" {{ ($data['payType']==2) ? "checked" : "" }}>
                            <label class="form-check-label" for="payType_2">{{ __('messages.minBuy') }}</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <select class="form-control" id="selectPaydown" name="selectPaydown" style="{{ ($data['payType']==1) ? "display:none;" : "" }}">
                            @for ($i = 2; $i < 7; $i++)
                                <option value="{{ $i }}" {{ ($data['select_paydown']==$i) ? "selected" : "" }}>{{ __('messages.round') }} {{ $i }}</option>
                            @endfor    
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-12">
                        @foreach ($data['paymentList'] as $key => $item)
                        <?php
                        $display = 'flex';
                        $readOnly = '';
                        $disabled = '';
                        ?>
                        @if (($key>1) && ($data['payType']==1))
                            <?php $display = 'none'; ?>
                        @elseif ($key > $data['select_paydown'])
                            <?php $display = 'none'; ?>
                        @endif
                        
                        @if ($item['status']==1)
                        <?php
                            $readOnly = 'readonly';
                            $disabled = 'disabled';
                        ?>
                        @elseif (($key==1) && ($data['payType']==1))
                            <?php $readOnly = 'readonly';  ?>
                        @elseif ($key == $data['select_paydown'])
                            <?php $readOnly = 'readonly';  ?>
                        @endif
                         
                        <div class="row" id="paymentList_{{$key}}" style="margin-right: 5px; padding: 0.5rem 0.0rem; display: <?php echo $display; ?>;">
                            <input type="hidden" name="paymentList[{{$key}}][payId]" value="{{$item['payId']}}">
                            <span style="font-weight: bold;padding: 0.3rem;">{{ __('messages.round') }} {{$key}}</span>
                            <div style="width: 18%;"><input type="text" class="form-control text-right-input" name="paymentList[{{$key}}][amount]" id="paymentList_amount_{{$key}}" value="{{ $item['amount'] }}" {{ $readOnly }} ></div>
                            <span style="padding: 0.3rem;">นัดชำระ</span>
                            <div style="width: 21%; padding-right: 0.3rem;"><input data-provide="datepicker" type="text" class="form-control" name="paymentList[{{$key}}][dueDate]" id="paymentList_dueDate_{{$key}}" value="{{ $item['dueDate'] }}" {{ $disabled }} ></div>
                            <div style="padding-right: 0.3rem;"><select class="paymentStatus form-control" id="paymentStatus_{{$key}}" name="paymentList[{{$key}}][status]">
                                <option value="0" {{ $item['status']==0 ? "selected" : "" }}>{{ __('messages.unpay') }}</option> 
                                <option value="1" {{ $item['status']==1 ? "selected" : "" }}>{{ __('messages.paid') }}</option> 
                            </select></div>
                            <button id="payButton_{{$key}}" type="button" class="paymentPrint btn btn-primary btn-sm" style="padding: 0.0rem; width:70px;">ออกบิล</button>
                        </div>
                        @endforeach
                    </div>
                    <div class="form-group col-12 row">
                        <div class="col-4">
                            <span>{{ __('messages.payChannelId') }}</span>
                        </div>
                        <div class="col-8">
                            @foreach ($selectOptionList['paymentChannels'] as $item)
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="payChannelId" id="payChannelId_{{$item['id']}}" value="{{$item['id']}}" <?php echo $item['id']==$data['paychannel'] ? "checked" : ""; ?>>
                                <label class="form-check-label" for="payChannelId">{{$item['name']}}</label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>        
            </div></div>
        </div>
        @if ( $data['vehId'] > 0 )
        <?php $index_i = 0; $index_j = 0; ?>
        <div class="col-lg-6 col-md-12 messengerObj" >
            <ul class="list-group">
                <li class="list-group-item list-group-item-info"><h5>{{ __('messages.messenger') }}</h5></li>
                @foreach ($data['messengerObj'] as $itemObj)
                <?php
                    $isFinish = ( !empty($itemObj['messengerStatusFinish']) || $data['status']==701 ) ? true : false;
                ?>
                <li class="list-group-item" style="padding: 0.5rem;"><div class="row">
                    <input type="hidden" name="messengerObj[{{$loop->index}}][messengerId]" value="{{ $itemObj['messengerId'] }}">
                    <div class="form-row col-lg-6 col-md-12">
                        <div class="form-group col-12">
                            <textarea class="form-control <?php echo (!$isFinish) ? 'cloneDescription' : ''; ?>" id="messengerObj_{{$loop->index}}_description" name="messengerObj[{{$loop->index}}][description]" rows="6" placeholder="{{ __('messages.description') }}" <?php echo ($isFinish) ? 'disabled' : ''; ?> >{{$itemObj['description']}}</textarea>    
                        </div>
                        <div class="form-group col-12">
                            <label>{{ __('messages.notificationDate') }}</label>
                            <input type="text" value="{{$itemObj['notificationDate']}}" name="messengerObj[{{$loop->index}}][notificationDate]" class="form-control" readonly>
                        </div>
                        <div class="form-group col-12">
                            <label class="form-check-label" for="messengerTypeId">{{ __('messages.messengerTypeId') }}</label>
                            <select name="messengerObj[{{$loop->index}}][messengerTypeId]" class="custom-select" <?php echo ($isFinish) ? 'disabled' : ''; ?>>
                                <option value="0">เลือกวิธีการส่ง</option>
                                @foreach ($selectOptionList['listMessengerType'] as $item)
                                <option value="{{ $item['messengerTypeId'] }}" {{ ($itemObj['messengerTypeId']==$item['messengerTypeId']) ? "selected" : "" }}>{{ $item['messengerTypeName'] }}</option>
                                @endforeach
                            </select>
                        </div>    
                    </div>
                    <div class="form-row col-lg-6 col-md-12" style="padding-bottom: 0.1rem;">
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <input name="messengerObj[{{$loop->index}}][sentReqEms]" value="{{$itemObj['sentReqEms']}}" type="text" class="form-control" placeholder="{{ __('messages.inputEMS') }}" <?php echo ($isFinish) ? 'disabled' : ''; ?>>
                        </div>
                        <div class="form-group" style="width: 40%;margin-bottom: 3px;">
                            <input data-provide="datepicker" value="{{$itemObj['sentReqDate']}}" name="messengerObj[{{$loop->index}}][sentReqDate]" type="text" class="form-control" placeholder="{{ __('messages.inputDateEMS') }}" <?php echo ($isFinish) ? 'disabled' : ''; ?>>
                        </div>
                        
                        <div class="form-group" style="width: 50%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <input class="form-check-input" name="messengerObj[{{$loop->index}}][sentReceiptTag]" value="1" type="checkbox" <?php echo !empty($itemObj['sentReceiptTag']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?>>
                                <label class="form-check-label" for="sentReceiptTag">{{ __('messages.sentReceipt') }}</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="messengerObj[{{$loop->index}}][sentReqTag]" value="1" type="checkbox" <?php echo !empty($itemObj['sentReqTag']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?>>
                                <label class="form-check-label" for="sentReqTag">{{ __('messages.sentReq') }}</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="messengerObj[{{$loop->index}}][sentActTag]" value="1" type="checkbox" <?php echo !empty($itemObj['sentActTag']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?>>
                                <label class="form-check-label" for="sentActTag">{{ __('messages.sentAct') }}</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="messengerObj[{{$loop->index}}][sentFormCutCardTag]" type="checkbox" <?php echo !empty($itemObj['sentFormCutCardTag']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?>>
                                <label class="form-check-label" for="sentFormCutCardTag">{{ __('messages.sentFormCutCard') }}</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="messengerObj[{{$loop->index}}][campaign]" value="1" type="checkbox" <?php echo !empty($itemObj['campaign']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?>>
                                <label class="form-check-label" for="campaign">{{ __('messages.campaignSpecialInsure') }}</label>
                                <textarea class="form-control <?php echo (!$isFinish) ? 'cloneCampaign' : ''; ?>" id="campaignDetail" name="messengerObj[{{$loop->index}}][campaignDetail]" rows="6" placeholder="" readonly>{{$itemObj['campaignDetail']}}</textarea>
                            </div>
                        </div>
                        <div class="form-group" style="width: 50%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <input class="form-check-input" name="messengerObj[{{$loop->index}}][sentDepartmentTag]" value="1" type="checkbox"<?php echo !empty($itemObj['sentDepartmentTag']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?>>
                                <label class="form-check-label" for="sentDepartmentTag">{{ __('messages.sentDepartment') }}</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="messengerObj[{{$loop->index}}][sentEndorseTag]" value="1" type="checkbox" <?php echo !empty($itemObj['sentEndorseTag']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?>>
                                <label class="form-check-label" for="sentEndorseTag">{{ __('messages.sentEndorse') }}</label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" name="messengerObj[{{$loop->index}}][sentRenewTag]" value="1" type="checkbox" <?php echo !empty($itemObj['sentRenewTag']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?>>
                                <label class="form-check-label" for="sentRenewTag">{{ __('messages.sentRenew') }}</label>
                            </div>
                            <div class="form-check">
                                <input name="messengerObj[{{$loop->index}}][sentOtherItemList][0][sentOtherId]" type="hidden" value="{{$itemObj['sentOtherItemList'][0]['sentOtherItemId']}}">
                                <input class="form-check-input" name="messengerObj[{{$loop->index}}][sentOtherItemList][0][sentOtherTag]" type="checkbox" <?php echo !empty($itemObj['sentOtherItemList'][0]['sentOtherTag']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?>>
                                <label class="form-check-label" for="sentOtherTag">{{ __('messages.sentOther') }}</label>
                                <textarea class="form-control" id="sentOtherDetail" name="messengerObj[{{$loop->index}}][sentOtherItemList][0][sentOtherDetail]" rows="6" placeholder="{{ __('messages.description') }}" <?php echo ($isFinish) ? 'disabled' : ''; ?>>{{$itemObj['sentOtherItemList'][0]['sentOtherDetail']}}</textarea>
                            </div>
                        </div>
                    </div>
                </div></li>
                @endforeach
            </ul>
        </div>
        <div class="col-lg-6 col-md-12 sentMessenger" >
            <ul class="list-group">
                <li class="list-group-item list-group-item-info"><h5>{{ __('messages.sentMessenger') }}</h5></li>
                @foreach ( $data['sentMessengerObj'] as $itemObj )
                <?php
                    $isFinish = ( !empty($itemObj['completeSentMessengerFlag']) || $data['status']==105 ) ? true : false;
                ?>
                <li class="list-group-item" style="padding: 0.5rem;"><div class="row">
                    <input type="hidden" name="sentMessengerObj[{{$loop->index}}][sentMessengerId]" value="{{ $itemObj['sentMessengerId'] }}">
                    <div class="form-row col-lg-6 col-md-12">
                        <div class="form-group col-12">
                            <textarea class="form-control <?php echo (!$isFinish) ? 'cloneDescription' : ''; ?>" name="sentMessengerObj[{{$loop->index}}][detail]" rows="6" placeholder="{{ __('messages.description') }}" <?php echo ($isFinish) ? 'disabled' : ''; ?> >{{ $itemObj['detail'] }}</textarea>    
                        </div>
                        <div class="form-group col-12">
                            <label>{{ __('messages.notificationDate') }}</label>
                            <input type="text" value="{{ $itemObj['notificationDate'] }}" name="sentMessengerObj[{{$loop->index}}][notificationDate]" class="form-control"  <?php echo ($isFinish) ? 'disabled' : 'readonly'; ?> >
                        </div>
                        <div class="form-group col-6">
                            <label>{{ __('messages.dueDateShort') }}</label>
                            <input type="text" name="sentMessengerObj[{{$loop->index}}][dueDateSM]" value="{{ $itemObj['dueDateSM'] }}" class="form-control" data-provide="datepicker" <?php echo ($isFinish) ? 'disabled' : ''; ?> >
                        </div>
                        <div class="form-group col-6">
                            <label>{{ __('messages.time') }}</label>
                            <input type="text" name="sentMessengerObj[{{$loop->index}}][dueTimeSM]" value="{{ $itemObj['dueTimeSM'] }}" class="form-control dueTimeSM" <?php echo ($isFinish) ? 'disabled' : ''; ?> >
                        </div>
                        <div class="form-group col-4">
                            <label class="form-check-label">{{ __('messages.messengerNote') }}</label>
                        </div>
                        <div class="form-group col-8">
                            <textarea class="form-control" name="sentMessengerObj[{{$loop->index}}][note]" rows="4" placeholder="{{ __('messages.note') }}" <?php echo ($isFinish) ? 'disabled' : ''; ?> >{{ $itemObj['note'] }}</textarea>    
                        </div>
                    </div>
                    <div class="form-row col-lg-6 col-md-12" style="padding-bottom: 0.1rem;height: 300px;">
                        <div class="form-group" style="width: 40%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <input class="form-check-input" name="sentMessengerObj[{{$loop->index}}][receiveDocTag]" type="checkbox" <?php echo !empty($itemObj['receiveDocTag']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?> >
                                <label class="form-check-label" for="receiveDocTag">{{ __('messages.receiveDoc') }}</label>
                            </div>
                        </div>
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <input name="receiveDocDetail" value="{{ $itemObj['receiveDocDetail'] }}" type="text" class="form-control" <?php echo ($isFinish) ? 'disabled' : ''; ?> >
                        </div>
                        
                        <div class="form-group" style="width: 40%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <input class="form-check-input" name="sentMessengerObj[{{$loop->index}}][sentBill]" type="checkbox" <?php echo !empty($itemObj['sentBill']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?> >
                                <label class="form-check-label" for="sentBill">{{ __('messages.sentReceipt') }}</label>
                            </div>
                        </div>
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;"></div>
                        
                        <div class="form-group" style="width: 40%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <input class="form-check-input" name="sentMessengerObj[{{$loop->index}}][sentReq]" type="checkbox" <?php echo !empty($itemObj['sentReq']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?> >
                                <label class="form-check-label" for="sentReq">{{ __('messages.sentReq') }}</label>
                            </div>
                        </div>
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;"></div>
                        
                        <div class="form-group" style="width: 40%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <input class="form-check-input" name="sentMessengerObj[{{$loop->index}}][sentAct]" type="checkbox" <?php echo !empty($itemObj['sentAct']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?> >
                                <label class="form-check-label" for="sentAct">{{ __('messages.sentAct') }}</label>
                            </div>
                        </div>
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;"></div>
                        
                        <div class="form-group" style="width: 40%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <input class="form-check-input" name="sentMessengerObj[{{$loop->index}}][sentDepartment]" type="checkbox" <?php echo !empty($itemObj['sentDepartment']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?> >
                                <label class="form-check-label" for="sentDepartment">{{ __('messages.sentDepartment') }}</label>
                            </div>
                        </div>
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;"></div>
                        
                        <div class="form-group" style="width: 40%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <input class="form-check-input" name="sentMessengerObj[{{$loop->index}}][receiveFirstInstallmentTag]" type="checkbox" <?php echo !empty($itemObj['receiveFirstInstallmentTag']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?> >
                                <label class="form-check-label" for="receiveFirstInstallmentTag">{{ __('messages.receiveFirstInstallment') }}</label>
                            </div>
                        </div>
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <input name="sentMessengerObj[{{$loop->index}}][receiveFirstInstallmentDetail]" type="text" class="form-control" value="{{ $itemObj['receiveFirstInstallmentDetail'] }}" <?php echo ($isFinish) ? 'disabled' : ''; ?> >
                        </div>
                        
                        <div class="form-group" style="width: 40%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <input class="form-check-input" name="sentMessengerObj[{{$loop->index}}][photographTag]" type="checkbox" <?php echo !empty($itemObj['photographTag']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?> >
                                <label class="form-check-label" for="photographTag">{{ __('messages.photograph') }}</label>
                            </div>
                        </div>
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <input name="sentMessengerObj[{{$loop->index}}][photographDetail]" type="text" class="form-control" value="{{ $itemObj['photographDetail'] }}" <?php echo ($isFinish) ? 'disabled' : ''; ?> >
                        </div>
                        
                        <div class="form-group" style="width: 40%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <div class="form-check">
                                <input class="form-check-input" name="sentMessengerObj[{{$loop->index}}][receiveLastInstallmentTag]" type="checkbox" <?php echo !empty($itemObj['receiveLastInstallmentTag']) ? 'checked' : ''; ?> <?php echo ($isFinish) ? 'disabled' : ''; ?> >
                                <label class="form-check-label" for="receiveLastInstallmentTag">{{ __('messages.receiveLastInstallment') }}</label>
                            </div>
                        </div>
                        <div class="form-group" style="width: 60%;padding-right: 0.3rem;margin-bottom: 3px;">
                            <input name="sentMessengerObj[{{$loop->index}}][receiveLastInstallmentDetail]" type="text" class="form-control" value="{{ $itemObj['receiveLastInstallmentDetail'] }}" <?php echo ($isFinish) ? 'disabled' : ''; ?> >
                        </div>
                    </div>
                </div></li>
                @endforeach
            </ul>
        </div>
        @endif
        <div class="col-lg-12 col-md-12 text-center" style="">
            <button type="submit" class="btn btn-success btn-lg mr-2 my-3 col-2 saveButton"><?php echo ($data['vehId'] > 0) ? __('messages.save') : __('messages.create'); ?></button>
            <a href="{{url('customer')}}"><button type="button" class="btn btn-secondary btn-lg mr-2 my-3 col-2 cancelButton">{{ __('messages.cancel') }}</button></a>
        </div>
    </div>
    <div class="modal fade" id="insuaranceModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 1700px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('messages.unlockRate') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body"><div class="row">
                    <input type="hidden" value="{{$data['itemProtectId']}}" name="itemProtectId">
                    <div class="col-lg-4 col-md-12 mb-5" >
                        <h5 class="text-center">{{ __('messages.damagePerPerson') }}</h5>
                        <p class="text-left">{{ __('messages.damagePerPerson1') }}</p>
                        <div class="input-group">
                            <input name="extLifeDamagePerPerson" id="extLifeDamagePerPerson" type="text" class="form-control" value="{{$data['extLifeDamagePerPerson']}}" style="padding: 1.11rem;">
                            <div class="input-group-append">
                                <span class="input-group-text" >{{ __('messages.bahtPerMan') }}</span>
                            </div>
                        </div>
                        <div class="input-group mt-3">
                            <input name="extLifeDamagePerTime" id="extLifeDamagePerTime" type="text" class="form-control" value="{{$data['extLifeDamagePerTime']}}" style="padding: 1.11rem;">
                            <div class="input-group-append">
                                <span class="input-group-text" >{{ __('messages.bahtPerHit') }}</span>
                            </div>
                        </div>
                        <p class="text-left mt-3">{{ __('messages.damagePerPerson2') }}</p>
                        <div class="input-group">
                            <input name="extAssetDamagePerTime" id="extAssetDamagePerTime" type="text" class="form-control" value="{{$data['extAssetDamagePerTime']}}" style="padding: 1.11rem;">
                            <div class="input-group-append">
                                <span class="input-group-text" >{{ __('messages.bahtPerHit') }}</span>
                            </div>
                        </div>
                        <p class="text-left mt-3">{{ __('messages.damagePerPerson21') }}</p>
                        <div class="input-group">
                            <input name="extExcessPerTime" id="extExcessPerTime" type="text" class="form-control" value="{{$data['extExcessPerTime']}}" style="padding: 1.11rem;">
                            <div class="input-group-append">
                                <span class="input-group-text" >{{ __('messages.bahtPerHit') }}</span>
                            </div>
                        </div>
                    </div>        
                    <div class="col-lg-4 col-md-12 mb-5" >
                        <h5 class="text-center">{{ __('messages.damageForCar') }}</h5>
                        <p class="text-left">{{ __('messages.damageForCar1') }}</p>
                        <div class="input-group">
                            <input name="vehDamagePerTime" id="vehDamagePerTime" type="text" class="form-control" value="{{$data['vehDamagePerTime']}}" style="padding: 1.11rem;">
                            <div class="input-group-append">
                                <span class="input-group-text">{{ __('messages.bahtPerHit') }}</span>
                            </div>
                        </div>
                        <p class="text-left mt-3">{{ __('messages.damageForCar11') }}</p>
                        <div class="input-group">
                            <input name="vehExcessPerTime" id="vehExcessPerTime" type="text" class="form-control" value="{{$data['vehExcessPerTime']}}" style="padding: 1.11rem;">
                            <div class="input-group-append">
                                <span class="input-group-text" >{{ __('messages.bahtPerHit') }}</span>
                            </div>
                        </div>
                        <p class="text-left mt-3">{{ __('messages.damageForCar2') }}</p>
                        <div class="input-group">
                            <input name="vehLostPerTime" id="vehLostPerTime" type="text" class="form-control" value="{{$data['vehLostPerTime']}}" style="padding: 1.11rem;">
                            <div class="input-group-append">
                                <span class="input-group-text">{{ __('messages.baht') }}</span>
                            </div>
                        </div>
                    </div>        
                    <div class="col-lg-4 col-md-12" >
                        <h5 class="text-center">{{ __('messages.lastNote') }}</h5>
                        <p class="text-left">{{ __('messages.lastNote1') }}</p>
                        <p class="text-left">{{ __('messages.lastNote11') }}</p>
                        <div class="form-inline">
                            <p class="text-left" style="margin: 5px;">{{ __('messages.lastNote11A') }}</p>
                            <div class="input-group">
                                <input name="permDisabilityDriver" id="permDisabilityDriver" type="text" class="form-control" value="{{$data['permDisabilityDriver']}}" style="padding: 1.11rem;">
                                <div class="input-group-append">
                                    <span class="input-group-text">{{ __('messages.bahtPerHit') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-inline mt-3">
                            <p class="text-left" style="margin: 5px;">{{ __('messages.lastNote11B') }}</p>
                            <div class="input-group" style="width:83%">
                                <input name="permDisabilityPassengerPerson" id="permDisabilityPassengerPerson" type="text" class="form-control" value="{{$data['permDisabilityPassengerPerson']}}" style="padding: 1.11rem;">
                                <div class="input-group-append">
                                    <span class="input-group-text">{{ __('messages.person') }}</span>
                                </div>
                                <input name="permDisabilityPassenger" id="permDisabilityPassenger" type="text" class="form-control" value="{{$data['permDisabilityPassenger']}}" style="padding: 1.11rem;">
                                <div class="input-group-append">
                                    <span class="input-group-text">{{ __('messages.bahtPerHit') }}</span>
                                </div>
                            </div>
                        </div>
                        <p class="text-left mt-3">{{ __('messages.lastNote12') }}</p>
                        <div class="form-inline">
                            <p class="text-left" style="margin: 5px;">{{ __('messages.lastNote11A') }}</p>
                            <div class="input-group">
                                <input name="tempDisabilityDriver" id="tempDisabilityDriver" type="text" class="form-control" value="{{$data['tempDisabilityDriver']}}" style="padding: 1.11rem;">
                                <div class="input-group-append">
                                    <span class="input-group-text">{{ __('messages.bahtPerHit') }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="form-inline mt-3">
                            <p class="text-left" style="margin: 5px;">{{ __('messages.lastNote11B') }}</p>
                            <div class="input-group" style="width:83%">
                                <input name="tempDisabilityPassengerPerson" id="tempDisabilityPassengerPerson" type="text" class="form-control" value="{{$data['tempDisabilityPassengerPerson']}}" style="padding: 1.11rem;">
                                <div class="input-group-append">
                                    <span class="input-group-text">{{ __('messages.person') }}</span>
                                </div>
                                <input name="tempDisabilityPassenger" id="tempDisabilityPassenger" type="text" class="form-control" value="{{$data['tempDisabilityPassenger']}}" style="padding: 1.11rem;">
                                <div class="input-group-append">
                                    <span class="input-group-text">{{ __('messages.bahtPerHit') }}</span>
                                </div>
                            </div>
                        </div>
                        <p class="text-left mt-3" style="margin: 5px;">{{ __('messages.lastNote2') }}</p>
                        <div class="input-group">
                            <input name="treatmentDriver" id="treatmentDriver" type="text" class="form-control" value="{{$data['treatmentDriver']}}" style="padding: 1.11rem;">
                            <div class="input-group-append">
                                <span class="input-group-text">{{ __('messages.bahtPerHit') }}</span>
                            </div>
                        </div>
                        <p class="text-left mt-3" style="margin: 5px;">{{ __('messages.lastNote3') }}</p>
                        <div class="input-group">
                            <input name="bailDriver" id="bailDriver" type="text" class="form-control" value="{{$data['bailDriver']}}" style="padding: 1.11rem;">
                            <div class="input-group-append">
                                <span class="input-group-text">{{ __('messages.bahtPerHit') }}</span>
                            </div>
                        </div>
                    </div>        
                </div></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="driverModal"role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document" style="max-width: 1200px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ __('messages.unlockDriver') }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @foreach ($data['driverList'] as $key => $item)
                    <div class="form-row">
                        <input type="hidden" name="driverList[{{$key}}][driverId]" id="driverList_{{$key}}_id" value="{{$item['driverId']}}">
                        <div class="form-group col-lg-6 col-md-8">
                            <label for="driverList_{{$key}}_Name">{{ __('messages.name') }}</label>
                            <input type="text" name="driverList[{{$key}}][driverName]" class="form-control" id="driverList_{{$key}}_Name" value="{{$item['driverName']}}">
                        </div>
                        <div class="form-group col-lg-3 col-md-4">
                            <label for="driverList_{{$key}}_birthDate">{{ __('messages.birthDate') }}</label>
                            <input data-provide="datepicker" type="text" name="driverList[{{$key}}][birthDate]" class="form-control" id="driverList_{{$key}}_birthDate" value="{{$item['birthDate']}}">
                        </div>
                        <div class="form-group col-lg-3 col-md-6">
                            <label for="driverList_{{$key}}_occupation">{{ __('messages.occupation') }}</label>
                            <input type="text" name="driverList[{{$key}}][occupation]" class="form-control" id="driverList_{{$key}}_occupation" value="{{$item['occupation']}}">
                        </div>
                        <div class="form-group col-lg-4 col-md-6">
                            <label for="driverList_{{$key}}_driveLicenseNo">{{ __('messages.driveLicenseNo') }}</label>
                            <input type="text" name="driverList[{{$key}}][driveLicenseNo]" class="form-control" id="driverList_{{$key}}_driveLicenseNo" value="{{$item['driveLicenseNo']}}">
                        </div>
                        <div class="form-group col-lg-4 col-md-6">
                            <label for="driverList_{{$key}}_driveExpiryDate">{{ __('messages.expiryDate') }}</label>
                            <input data-provide="datepicker" type="text" name="driverList[{{$key}}][driveExpiryDate]" class="form-control" id="driverList_{{$key}}_driveExpiryDate" value="{{$item['driveExpiryDate']}}">
                        </div>
                        <div class="form-group col-lg-4 col-md-6">
                            <label for="driverList_{{$key}}_driveProvId">{{ __('messages.driveProv') }}</label>
                            <select class="form-control selectpicker" id="driverList_{{$key}}_driveProvId" name="driverList[{{$key}}][driveProvId]" data-live-search="true">
                                @foreach ($selectOptionList['provinces'] as $subitem)
                                <option value="{{ $subitem['id'] }}" {{ ($item['driveProvId']==$subitem['id']) ? "selected" : "" }}>{{ $subitem['name'] }}</option>
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" name="driverList[{{$key}}][driverId]" value="{{$item['driverId']}}">
                        <input type="hidden" id="driverList_{{$key}}_driveProvName" name="driverList[{{$key}}][driveProvName]" value="">           
                    </div>
                    @endforeach
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- hidden Data -->
    <input type="hidden" name="custId" value="{{$data['custId']}}">
    <input type="hidden" name="vehId" value="{{$data['vehId']}}">
    <input type="hidden" name="contactTypeId" value="{{$data['contactTypeId']}}">
    <input type="hidden" name="hasCctv" value="{{$data['hasCctv']}}">
    <input type="hidden" name="hasAccessory" value="1">
    <input type="hidden" name="orderId" value="{{$data['orderId']}}">
    <input type="hidden" name="caractId" value="{{$data['caractId']}}">
    <input type="hidden" name="insureId" value="{{$data['insureId']}}">
    <input type="hidden" id="vehProvName" name="vehProvName">
    <input type="hidden" id="partnerName" name="partnerName">
    <input type="hidden" id="vehTypeName" name="vehTypeName">
    <input type="hidden" id="manuName" name="manuName">
    <input type="hidden" id="modelName" name="modelName">
    <input type="hidden" id="saleAgentName" name="saleAgentName" value="{{$data['saleAgentName']}}">
    <input type="hidden" id="actTypeName" name="actTypeName" >
</form>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('js/jquery.form.js') }}"></script>
<script type="text/javascript">
    autonumber_arr = new Array();
    isChange = false;
    
    $(document).ready(function() {
        $('.dueTimeSM').datetimepicker({
            "showClose": true,
            "showClear": true,
            "showTodayButton": true,
            "format": "HH:mm",
        });
        
        $(".saveButton").click(function(){
            if(!$("#checkChasisNo").hasClass('btn-success')){
                $("#alertModalBody").text("Error : กรุณาตรวจสอบเลขถังให้ถูกต้อง");
                $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                $("#alertModal").modal('show');
                return false;    
            }
            for(i=1;i<6;i++){
                $("#paymentList_dueDate_" + i).prop("disabled", false);
            }
            @if(session('userlevelId') != 1 && session('userlevelId') != 6 )
                $(".paymentStatus").prop("disabled", false);
                $("#saleAgentId").prop("disabled", false);
            @endif
            
            $(".messengerObj :input").prop("disabled", false);
            $(".sentMessenger :input").prop("disabled", false);
            
            $("#formUpdate").ajaxForm({
                url: "<?php echo ( $data['vehId'] > 0 ) ? url('customer/' . $data['vehId']) : url('customer'); ?>",
                type: "{{ ( $data['vehId'] > 0 ) ? 'PUT' : 'POST' }}",
                beforeSubmit: function() {    
                },
                success: function(resp) {
                    if(typeof resp.error !== 'undefined'){
                        $("#alertModalBody").text("Error : " + resp.error.message);    
                        $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                        $("#alertModal").modal('show');
                        calPaymentStatus();
                            @if(session('userlevelId') != 1 && session('userlevelId') != 6 )
                                $(".paymentStatus").prop("disabled", true);
                            @endif    
                    }else{
                        $("#alertModalBody").text("Success : <?php echo ( $data['vehId'] > 0 ) ? __('messages.saved') : __('messages.created'); ?>");    
                        $("#alertModalLabel").text("<?php echo __('messages.success'); ?>");
                        $('#alertModal').on('hidden.bs.modal', function () {
                            isChange = false;
                            @if($data['vehId'] > 0)
                            window.open("<?php echo url('customer'); ?>/<?php echo $data['vehId']; ?>","_self");    
                            @else
                            var vehId = resp.result.vehId;
                            window.open("<?php echo url('customer'); ?>/" + vehId,"_self");    
                            @endif
                        }).modal('show');    
                    }
                },
                error:function(resp) {
                    $("#alertModalBody").text("Error : " + resp.statusText);
                    $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                    $("#alertModal").modal('show');
                    calPaymentStatus();
                    @if(session('userlevelId') != 1 && session('userlevelId') != 6 )
                        $(".paymentStatus").prop("disabled", true);
                    @endif
                },
                    
            });   
        }); 

        var iPhoneList = {{count($data['phoneList'])}};
        var iLineList = {{count($data['lineList'])}};
        var iAddrList = {{count($data['addressList'])}};
        
        autonumber_arr['engineCapacity'] = new AutoNumeric("#engineCapacity",{ 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' , 'decimalPlaces' : 0 });
        autonumber_arr['modelYear'] = new AutoNumeric("#modelYear",[{ 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' , 'decimalPlaces' : 0 }, 'numeric']);
        autonumber_arr['passenger'] = new AutoNumeric("#passenger",{ 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' , 'decimalPlaces' : 0 });
        autonumber_arr['accessoryPrice'] = new AutoNumeric('#accessoryPrice', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'true' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['insureRate'] = new AutoNumeric('#insureRate', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'true' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['extLifeDamagePerPerson'] = new AutoNumeric('#extLifeDamagePerPerson', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['extLifeDamagePerTime'] = new AutoNumeric('#extLifeDamagePerTime', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['extAssetDamagePerTime'] = new AutoNumeric('#extAssetDamagePerTime', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['extExcessPerTime'] = new AutoNumeric('#extExcessPerTime', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['vehDamagePerTime'] = new AutoNumeric('#vehDamagePerTime', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['vehExcessPerTime'] = new AutoNumeric('#vehExcessPerTime', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['vehLostPerTime'] = new AutoNumeric('#vehLostPerTime', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['permDisabilityDriver'] = new AutoNumeric('#permDisabilityDriver', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['permDisabilityPassengerPerson'] = new AutoNumeric('#permDisabilityPassengerPerson', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' , 'decimalPlaces' : 0 });
        autonumber_arr['permDisabilityPassenger'] = new AutoNumeric("#permDisabilityPassenger",{ 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['tempDisabilityDriver'] = new AutoNumeric("#tempDisabilityDriver",{ 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['tempDisabilityPassengerPerson'] = new AutoNumeric('#tempDisabilityPassengerPerson', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' , 'decimalPlaces' : 0 });
        autonumber_arr['tempDisabilityPassenger'] = new AutoNumeric("#tempDisabilityPassenger",{ 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['treatmentDriver'] = new AutoNumeric("#treatmentDriver",{ 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['bailDriver'] = new AutoNumeric("#bailDriver",{ 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'floats' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['changeOfInsure'] = new AutoNumeric('#changeOfInsure', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'true' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['changeOfCaract'] = new AutoNumeric('#changeOfCaract', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'true' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['dutyInsure'] = new AutoNumeric('#dutyInsure', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'true' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['dutyCaract'] = new AutoNumeric('#dutyCaract', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'true' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['taxInsure'] = new AutoNumeric('#taxInsure', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'true' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['taxCaract'] = new AutoNumeric('#taxCaract', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'true' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['totalInsure'] = new AutoNumeric('#totalInsure', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'true' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['totalCaract'] = new AutoNumeric('#totalCaract', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'true' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['agentDiscount'] = new AutoNumeric('#agentDiscount', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'true' , 'emptyInputBehavior' : 'zero'});
        autonumber_arr['totalPaymentInsure'] = new AutoNumeric('#totalPaymentInsure', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'true' , 'emptyInputBehavior' : 'zero' });
        autonumber_arr['totalGiftFree'] = new AutoNumeric('#totalGiftFree', { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'true' , 'emptyInputBehavior' : 'zero' });
        
        autonumber_arr['payPeriod'] = new Array();
        for(i=1;i<=6;i++){
            autonumber_arr['payPeriod'][i] = new AutoNumeric('#paymentList_amount_' + i, { 'valuesToStrings': { 0: "-" }, 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'true' , 'emptyInputBehavior' : 'zero' , 'decimalPlaces' : 0 });
        }
        
        replaceCommand();
        
        function replaceCommand(){
            $(".listClose").click(function() {
                $(this).parents("tr").remove();
            });
            
            $(".cloneDes").on("change keyup paste", function() {
                var nameDes = $("#custName").val();
                var AddrDes = "";
                var phoneDes = "";
                if( typeof $("input[name='phoneListSelect']:checked").val() !== 'undefined'){
                    var index = $("input[name='phoneListSelect']:checked").val();
                    phoneDes = $("#phoneList_" + index + "_phoneNo").val();
                }
                if( typeof $("input[name='addressListSelect']:checked").val() !== 'undefined'){
                    var index = $("input[name='addressListSelect']:checked").val();
                    AddrDes   = $("#addressList_" + index + "_address").val() + " " +
                                $("#addressList_" + index + "_distName").val() + " " +
                                $("#addressList_" + index + "_amphName").val() + " " +
                                "จังหวัด" + $("#addressList_" + index + "_provName").val() + " " +
                                $("#addressList_" + index + "_zipCode").val() + " ";
                }
                $(".cloneDescription").val(nameDes + " " + AddrDes + " " + phoneDes);
            });   
        }
        
        $("#phoneListAdd").click(function() {
            $.get("getNewPhoneList/" + (iPhoneList + 1) , function(response) {
                $("#phoneList").append(response);
                iPhoneList = iPhoneList + 1;
                replaceCommand();
            });
        });
        
        $("#lineListAdd").click(function() {
            $.get("getNewLineList/" + (iLineList + 1) , function(response) {
                $("#lineList").append(response);
                iLineList = iLineList + 1;
                replaceCommand();
            });
        });
        
        $("#addrListAdd").click(function() {
            $.get("getNewAddrList/" + (iAddrList + 1) , function(response) {
                $("#addrList").append(response);
                iAddrList = iAddrList + 1;
                replaceCommand();
            });
        });
        
        $("#manuId").change(function(){
            $("#modelId").empty();
            $("#modelId").append('<option value="">{{__('messages.pleaseSelectModel')}}</option>');
            if( parseInt($(this).val()) > 0  ){                                   
                $.ajax({
                    url: "<?php echo url('model/getAllModel'); ?>?manuId=" + $(this).val(),
                    success: function(resp){
                        jsonObj = resp;
                        if(typeof jsonObj.error !== 'undefined'){
                            $("#alertModalBody").text("Error : " + jsonObj.error.message);    
                            $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                            $("#alertModal").modal('show');    
                        }else if(jsonObj.result != null ){
                            $.each( jsonObj.result.models, function( key, value ) {
                                if( value.status == 1 ){
                                    $("#modelId").append('<option value="' + value.modelId + '" >' + value.modelName + '</option>');    
                                }
                            });
                            $('#modelId').selectpicker('refresh'); 
                        }           
                    },
                    error:function(resp) {
                        $("#alertModalBody").text("Network Error : " + resp.statusText);
                        $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                        $("#alertModal").modal('show');
                    },
                });
            }
            $('#modelId').selectpicker('refresh');
        });
        
        $("input[name='driverRecrive']").change(function(){
            canDriverRecrive();        
        });
        
        $("input[name='beneficiary']").change(function(){
            canBeneficiary();        
        });
        
        $("#insureType").change(function(){
            selectInsureType();
            selectPartnerType();
        });

        $("#actType").change(function(){
            selectActType();
        });
        
        $("#totalInsure").on('input',function(e){
            calculateInsure();
            calculateTotal();
            calculatePayment();          
        });
        
        $("#totalCaract").on('input',function(e){
            calculateCarAct();
            calculateTotal();
            calculatePayment();          
        });
        
        $("#agentDiscount").on('input',function(e){
            calculateTotal();
            calculatePayment();          
        });
        
        $("#freeCarAct").change(function(){
            calculateTotal();
            calculatePayment();
        });
        
        $("#selectPaydown").change(function(){
            calPaymentStatus();
            calculatePayment();
        });
        
        @for($i=1;$i<=6;$i++)
        $("#paymentList_amount_" + {{$i}}).on('input',function(e){
            calculatePayment({{$i}});          
        });
        
        $("#paymentList_dueDate_" + {{$i}}).on('change',function(e){
            var d = new Date($("#paymentList_dueDate_{{$i}}").val());
            for( var i={{$i+1}} ; i<=6 ; i++ ){
                d.setMonth(d.getMonth()+1);
                if((d.getMonth()+1)<10){
                    month = "0" + (d.getMonth()+1);    
                }else{
                    month = (d.getMonth()+1);
                }
                
                if((d.getDate())<10){
                    date = "0" + d.getDate();    
                }else{
                    date = d.getDate();
                }
                if($("#paymentStatus_" + i).val() == 0){
                    $("#paymentList_dueDate_" + i).val(d.getFullYear() + "-" + month + "-" + date);    
                }
            }
        });
        
        $("#payButton_{{$i}}").click(function(e){
            if($("#paymentStatus_{{$i}}").val() == 0){
                pdfform("bill",{{$i}});
            }else{
                pdfform("receipt",{{$i}});
            }    
        }); 
        @endfor
        
        $(".paymentStatus").change(function(){
            calPaymentStatus();
        });
        
        $("#partnerType").change(function(){
            selectPartnerType();
        });
        
        $("#receiveDate,#receiveExpireDate").change(function(){
            var CurrentDate = new Date($("#receiveDate").val());
            GivenDate = new Date($("#receiveExpireDate").val());

            if(GivenDate < CurrentDate){
                $(this).val('');
                $("#alertModalBody").text("Error : This Date can not select less than Expire Date.");    
                $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                $("#alertModal").modal('show');
            }else if($(this).attr('id') == "receiveDate"){
                var res = $(this).val().split("-");
                $('#receiveExpireDate').val((parseInt(res[0])+1)+"-"+res[1]+"-"+res[2]);
            }
        });
        
        $("#actReceiveDate,#actReceiveExpireDate").change(function(){
            var CurrentDate = new Date($("#actReceiveDate").val());
            GivenDate = new Date($("#actReceiveExpireDate").val());

            if(GivenDate < CurrentDate){
                $(this).val('');
                $("#alertModalBody").text("Error : This Date can not select less than Expire Date.");    
                $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                $("#alertModal").modal('show');
            }else if($(this).attr('id') == "actReceiveDate"){
                var res = $(this).val().split("-");
                $('#actReceiveExpireDate').val((parseInt(res[0])+1)+"-"+res[1]+"-"+res[2]);
            }
        });
        
        $("input[name='payType'], #selectPaydown").change(function(){ 
            if ($("input[name='payType']:checked").val() == 1){
                for( var i=2; i<=6 ; i++ ){
                    $("#paymentList_" + i).hide();
                }
                $("#selectPaydown").hide();
            }else{
                var paydown = $("#selectPaydown").val();
                console.log(paydown);
                for( var i=2; i<=paydown ; i++ ){
                    $("#paymentList_" + i).show();
                }
                for( var i=i; i<=6 ; i++ ){
                    $("#paymentList_" + i).hide();
                }
                $("#selectPaydown").show();
            }
            calPaymentStatus();
            calculatePayment();    
        });
        
        // Init First Time
        canDriverRecrive();
        canBeneficiary();
        selectInsureType(false);
        selectActType(false);
        calPaymentStatus();
        selectPartnerType();
                
        @if($data['isReadOnly'])
            $("#formUpdate :input").prop("disabled", true);
            $(".listClose").remove();
            $(".saveButton").remove();
            $(".cancelButton").prop("disabled", false);
            $("#unlockRate").prop("disabled", false);
            $("#unlockDriver").prop("disabled", false);
            $(".modal-footer :button").prop("disabled", false);
            $(".modal-header :button").prop("disabled", false);
            $(".listpdf").empty();
            $("#checkChasisNo").html('<i class="fas fa-check-circle"></i>');
            $("#checkChasisNo").addClass("btn-success").removeClass("btn-primary"); 
        @else
            @if(session('userlevelId') != 1 && session('userlevelId') != 6 )
                $(".paymentPrint").prop("disabled", true).hide();
                $(".paymentStatus").prop("disabled", true);
                $("#saleAgentId").prop("disabled", true);
                $(".listpdf").empty();
            @endif
        @endif
        
        $("#formUpdate").css("opacity",1);
        var oldChasisNo = $("#chasisNo").val();
        
        @if(!empty($data['chasisNo']))
            $("#checkChasisNo").html('<i class="fas fa-check-circle"></i>');
            $("#checkChasisNo").addClass("btn-success").removeClass("btn-primary"); 
        @endif
        
        $("#chasisNo").on('input', function(){
            $("#checkChasisNo").removeClass("btn-success btn-danger");
            $("#checkChasisNo").addClass("btn-primary");
            $("#checkChasisNo").html("ตรวจเลขถัง");       
        });
        
        $("#campaignSpecialInsure").on("change keyup paste", function() {
            $(".cloneCampaign").val( $("#campaignSpecialInsure").val() );      
        });
        
        $("#checkChasisNo").click(function(e){
            $("#checkChasisNo").html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>').prop("disabled", true);
            $.ajax({
                url: "<?php echo url('customer/isDuplicateChasisNo'); ?>",
                type: "POST",
                data:{
                    chasisNo: $("#chasisNo").val(),
                    _token: "{{ csrf_token() }}"
                },
                success: function(resp) {
                    if(resp != 'null') {
                        var data = jQuery.parseJSON(resp);
                        if (typeof data['isDuplicate'] === 'undefined') {
                            data = new Array();
                            data['isDuplicate'] = false;    
                        }
                        if((data['isDuplicate'])&&(oldChasisNo != $("#chasisNo").val())){
                            $("#checkChasisNo").html('<i class="fas fa-times-circle"></i>');
                            $("#checkChasisNo").addClass("btn-danger").removeClass("btn-primary");
                        }else{
                            $("#checkChasisNo").html('<i class="fas fa-check-circle"></i>');
                            $("#checkChasisNo").addClass("btn-success").removeClass("btn-primary");    
                        }
                        $("#checkChasisNo").prop("disabled", false);
                    }
                    else{
                        $(e).append('มีความผิดพลาดทางเทคนิค กรุณาตรวจสอบใหม่อีกครั้ง');
                        $(e).dialog("open");    
                    }
                },
                error:function(resp) {
                    $("#alertModalBody").text("Error : " + resp.statusText);
                    $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                    $("#alertModal").modal('show');    
                },
            });
        });
        
        $("#formUpdate :input").on("change keyup paste", function() {
            isChange = true;
        });
        
    });
    
    $('html').bind('keypress', function(e)
    {
        try {
            var element = e.target.nodeName.toLowerCase();
            if ( ( element != 'textarea' ) && (e.keyCode == 13) ){
                return false;
            }
        }
        catch(err) {
            console.log(err);
        }
        
    });
    
    $('#copyButton').on('click', function(event) {
        $(".copyToClipboard").select();
        document.execCommand('copy');
    });
    
    function selectInsureType(isCalculate = true){
        if ($("#insureType").val() == 0){
            // hide
            $("#unlockRate").hide();
            $("#unlockDriver").hide();
            
            // readonly
            $("#insureNo").attr("readonly",true);
            $("#insureNo").val("");
            $("#postNo").attr("readonly",true);
            $("#postNo").val("");
            $("#insureRate").attr("readonly",true);
            autonumber_arr['insureRate'].set(0);
            $("#totalInsure").attr("readonly",true);
            
            // disabled
            $("#partnerType").attr("disabled",true);
            $("#partnerType").val("0");
            $("input[name='repairType']").attr("disabled",true);
            $("#receiveDate").attr("disabled",true);
            $("#receiveDate").val("");
            $("#receiveExpireDate").attr("disabled",true);
            $("#receiveExpireDate").val("");
            $("#contractDate").attr("disabled",true);
            $("#contractDate").val("");
            $("input[name='driverRecrive']").prop('checked', false).attr("disabled",true);
            $("#driverRecrive_0").prop('checked', true);
            $("input[name='beneficiary']").prop('checked', false).attr("disabled",true);
            $("#beneficiary_0").prop('checked', true);
            $("#beneficiary_name").attr("disabled",false);
            
            if(isCalculate){
                autonumber_arr['totalInsure'].set(0);
                calculateInsure();
                calculateTotal();
            }
                
        }else{ 
            // hide
            $("#unlockRate").show();
            
            // readonly
            $("#insureNo").attr("readonly",false);
            $("#postNo").attr("readonly",false);
            $("#insureRate").attr("readonly",false);
            $("#totalInsure").attr("readonly",false);
            
            // disabled
            $("#partnerType").attr("disabled",false);
            $("input[name='repairType']").attr("disabled",false);
            $("#receiveDate").attr("disabled",false);
            $("#receiveExpireDate").attr("disabled",false);
            $("#contractDate").attr("disabled",false);
            $("input[name='driverRecrive']").attr("disabled",false);
            $("input[name='beneficiary']").attr("disabled",false);
        }
    }
    
    function selectPartnerType(isCalculate = true){
        if($("#partnerType").val() == 2){
            $("#viriya").attr("disabled",false);    
        }else{
            $("#viriya").attr("disabled",true);
        }
    }
    
    function selectActType(isCalculate = true){
        if ($("#actType").val() == 0){
            // readonly
            $("#actPostcode").attr("readonly",true);
            // disabled
            $("#actPartnerType").attr("disabled",true);
            $("#actPartnerType").val("0");
            $("#actReceiveDate").attr("disabled",true);
            $("#actReceiveDate").val("");
            $("#actReceiveExpireDate").attr("disabled",true);
            $("#actReceiveExpireDate").val("");
            
            if(isCalculate){
                autonumber_arr['totalCaract'].set(0);
            }
                
        }else{
            // readonly
            $("#actPostcode").attr("readonly",false);
            
            // disabled
            $("#actPartnerType").attr("disabled",false);
            $("#actReceiveDate").attr("disabled",false);
            $("#actReceiveExpireDate").attr("disabled",false);
            
            var selectedCarAct = $("#actType").val();
            var priceCarAct = Array();
            @foreach ($selectOptionList['compulsoryProductTypes'] as $subitem)
                priceCarAct[{{ $subitem['id'] }}] = {{ $subitem['price'] }};
            @endforeach
            var price = priceCarAct[selectedCarAct];
            autonumber_arr['totalCaract'].set(price);
        }
        
        if(isCalculate){
            calculateCarAct();
            calculateTotal();
        }
    }
    
    function canDriverRecrive(){
        if ($("input[name='driverRecrive']:checked").val() == 1){
            $("#unlockDriver").show();
        }else{
            $("#unlockDriver").hide();
            //console.log("hide");
        }
    }
    
    function canBeneficiary(){
        if ($("input[name='beneficiary']:checked").val() == 1){
            $("#beneficiary_name").attr("disabled",false);
        }else{
            $("#beneficiary_name").attr("disabled",true);
            $("#beneficiary_name").val("");
        }
    }
    
    function calculateInsure(){
        var changeOfInsure = Math.floor(autonumber_arr['totalInsure'].get()/1.07428);
        var duty = Math.ceil(changeOfInsure*0.00428);
        var tax = (autonumber_arr['totalInsure'].get()-(changeOfInsure+duty)).toFixed(2);
        
        autonumber_arr['dutyInsure'].set(duty);
        autonumber_arr['taxInsure'].set(tax);
        autonumber_arr['changeOfInsure'].set(changeOfInsure);
        
    }
    
    function calculateCarAct(){ 
        var changeOfCarAct = Math.floor(autonumber_arr['totalCaract'].get()/1.07428);
        var duty = Math.ceil(changeOfCarAct*0.00428);
        var tax = (autonumber_arr['totalCaract'].get()-(changeOfCarAct+duty)).toFixed(2);

        autonumber_arr['dutyCaract'].set(duty);
        autonumber_arr['taxCaract'].set(tax);
        autonumber_arr['changeOfCaract'].set(changeOfCarAct);
    }
    
    function calculateTotal(){
        var totalPaymentInsure = parseFloat(autonumber_arr['totalInsure'].getNumericString());
        if($("#freeCarAct").prop("checked") == false){
            totalPaymentInsure = totalPaymentInsure + parseFloat(autonumber_arr['totalCaract'].getNumericString());        
        }
        autonumber_arr['totalPaymentInsure'].set(totalPaymentInsure - parseFloat(autonumber_arr['agentDiscount'].getNumericString()));
    }
    
    function calculatePayment(start = 0){
        var totalPrice = parseFloat(autonumber_arr['totalPaymentInsure'].getNumericString());
        var arrListUpdate = Array();
        if ($("input[name='payType']:checked").val() == 1){
            paydown = 1; 
        }else{
            var paydown = $("#selectPaydown").val();    
        }
        var division = paydown;
        for( var i=1; i<=paydown ; i++ ){
            if( ( $("#paymentStatus_" + i).val() == 1) || ( i <= start) ){
                division = division - 1;
                totalPrice = totalPrice - parseFloat(autonumber_arr['payPeriod'][i].getNumericString());    
            }else{
                arrListUpdate.push(i);    
            }
        }
        var priceByRound = parseInt(totalPrice / division);
        var additionPrice = totalPrice % division;
        
        while (arrListUpdate.length) {
            var id = arrListUpdate.pop();
            autonumber_arr['payPeriod'][id].set(priceByRound+additionPrice);
            additionPrice = 0;
        } 
    }
    
    function calPaymentStatus(){
        for(var i=1; i<=6 ; i++ ){
            if( $( "#paymentStatus_" + i ).val() == 1 ){
                $("#paymentList_amount_" + i).prop("readonly", true); 
                $("#paymentList_dueDate_" + i).prop("disabled", true);    
                $("#payButton_" + i).text("ออกใบเสร็จ");
            }else if ($("input[name='payType']:checked").val() == 1) {
                if (i == 1){
                    $("#paymentList_amount_" + i).prop("readonly", true); 
                    $("#paymentList_dueDate_" + i).prop("disabled", false);    
                }
                $("#payButton_" + i).text("ออกบิล");
            }else{
                if ( i == $("#selectPaydown").val() ){
                    $("#paymentList_amount_" + i).prop("readonly", true); 
                    $("#paymentList_dueDate_" + i).prop("disabled", false);
                }else{
                    $("#paymentList_amount_" + i).prop("readonly", false); 
                    $("#paymentList_dueDate_" + i).prop("disabled", false);
                }
                $("#payButton_" + i).text("ออกบิล");
            }
        }
    }
    
    function pdfform(method, index = 0){
        var disableList = Array();
        var vehProvIdText = $("#vehProvId option:selected").html();
        if( $("#vehProvId").val() == 0 ){
            var vehProvIdText = "";    
        }
        $("#vehProvName").val(vehProvIdText);
        
        var partnerNameText = $("#partnerType option:selected").html();
        if( $("#partnerType").val() == 0 ){
            var partnerNameText = "";    
        }
        $("#partnerName").val(partnerNameText);
        
        var vehTypeName = $("#vehTypeId option:selected").html();
        if( $("#vehTypeId").val() > 0 ){
            var arrVehType = vehTypeName.split(":");
            vehTypeName = arrVehType[1];    
        }else{
            vehTypeName = "";
        }
        $("#vehTypeName").val(vehTypeName);
        
        var manuName = $("#manuId option:selected").html();
        if( $("#manuId").val() == 0 ){
            manuName = "";
        }
        $("#manuName").val(manuName);
        
        var modelName = $("#modelId option:selected").html();
        if( $("#modelId").val() == 0 ){
            modelName = "";
        }
        $("#modelName").val(modelName);
        
        var actTypeName = $("#actType option:selected").html();
        if( $("#actType").val() > 0 ){
            var arrActType = actTypeName.split(" — ");
            actTypeName = arrActType[0];    
        }else{
            actTypeName = "";
        }
        $("#actTypeName").val(actTypeName);
        
        autonumber_arr['engineCapacity'].unformat();
        autonumber_arr['accessoryPrice'].unformat();
        autonumber_arr['extLifeDamagePerPerson'].unformat();
        autonumber_arr['extLifeDamagePerTime'].unformat();
        autonumber_arr['extAssetDamagePerTime'].unformat();
        autonumber_arr['extExcessPerTime'].unformat();
        autonumber_arr['vehDamagePerTime'].unformat();
        autonumber_arr['vehExcessPerTime'].unformat();
        autonumber_arr['vehLostPerTime'].unformat();
        autonumber_arr['permDisabilityDriver'].unformat();
        autonumber_arr['permDisabilityPassengerPerson'].unformat();
        autonumber_arr['permDisabilityPassenger'].unformat();
        autonumber_arr['tempDisabilityDriver'].unformat();
        autonumber_arr['tempDisabilityPassengerPerson'].unformat();
        autonumber_arr['tempDisabilityPassenger'].unformat();
        autonumber_arr['treatmentDriver'].unformat();
        autonumber_arr['bailDriver'].unformat();
        
        autonumber_arr['changeOfInsure'].unformat();
        autonumber_arr['dutyInsure'].unformat();
        autonumber_arr['taxInsure'].unformat();
        autonumber_arr['totalInsure'].unformat();
        
        autonumber_arr['changeOfCaract'].unformat();
        autonumber_arr['dutyCaract'].unformat();
        autonumber_arr['taxCaract'].unformat();
        autonumber_arr['totalCaract'].unformat();
        
        autonumber_arr['agentDiscount'].unformat();
        autonumber_arr['totalPaymentInsure'].unformat();
        
        for(var i=1; i<=6 ; i++ ){
            autonumber_arr['payPeriod'][i].unformat();    
        }
        
        /*if(!$("input[name='phoneListSelect']").attr("disabled")){
            $("input[name='phoneListSelect']").attr("disabled", false);
            disableList.push("input[name='phoneListSelect']");
        }
        
        if(!$("input[name='addressList']").attr("disabled")){
            $("input[name='addressList']").attr("disabled", false);
            disableList.push("input[name='addressList']");
        } */
        
        var form = document.getElementById('formUpdate');
        if(index == 0)
            form.action = "{{ url('pdf') }}/" + method ;
        else{
            form.action = "{{ url('pdf') }}/" + method + "/" + index ;
        }
        form.method = "POST";
        form.target = "_blank";
        
        form.submit();
        
        $.each(disableList, function( index, value ) {
            $(value).attr("disabled", false);
        });
        
        autonumber_arr['engineCapacity'].reformat();
        autonumber_arr['accessoryPrice'].reformat();
        autonumber_arr['extLifeDamagePerPerson'].reformat();
        autonumber_arr['extLifeDamagePerTime'].reformat();
        autonumber_arr['extAssetDamagePerTime'].reformat();
        autonumber_arr['extExcessPerTime'].reformat();
        autonumber_arr['vehDamagePerTime'].reformat();
        autonumber_arr['vehExcessPerTime'].reformat();
        autonumber_arr['vehLostPerTime'].reformat();
        autonumber_arr['permDisabilityDriver'].reformat();
        autonumber_arr['permDisabilityPassengerPerson'].reformat();
        autonumber_arr['permDisabilityPassenger'].reformat();
        autonumber_arr['tempDisabilityDriver'].reformat();
        autonumber_arr['tempDisabilityPassengerPerson'].reformat();
        autonumber_arr['tempDisabilityPassenger'].reformat();
        autonumber_arr['treatmentDriver'].reformat();
        autonumber_arr['bailDriver'].reformat();
        
        autonumber_arr['changeOfInsure'].reformat();
        autonumber_arr['dutyInsure'].reformat();
        autonumber_arr['taxInsure'].reformat();
        autonumber_arr['totalInsure'].reformat();
        
        autonumber_arr['changeOfCaract'].reformat();
        autonumber_arr['dutyCaract'].reformat();
        autonumber_arr['taxCaract'].reformat();
        autonumber_arr['totalCaract'].reformat();
        
        autonumber_arr['agentDiscount'].reformat();
        autonumber_arr['totalPaymentInsure'].reformat();
        
        for(var i=1; i<=6 ; i++ ){
            autonumber_arr['payPeriod'][i].reformat();    
        }
        
        form.target = "_self";    
        form.action = "";
        form.method = "";
    }
    
    function quotation(){
        var form = document.getElementById('formUpdate');
        
        var vehProvIdText = $("#vehProvId option:selected").html();
        if( $("#vehProvId").val() == 0 ){
            var vehProvIdText = "";    
        }
        $("#vehProvName").val(vehProvIdText);
        
        var manuName = $("#manuId option:selected").html();
        if( $("#manuId").val() == 0 ){
            manuName = "";
        }
        $("#manuName").val(manuName);
        
        var modelName = $("#modelId option:selected").html();
        if( $("#modelId").val() == 0 ){
            modelName = "";
        }
        $("#modelName").val(modelName);
        
        form.method = "POST";
        form.target = "_blank";
        form.action = "{{ url('quotation') }}/{{$data['vehId']}}";
        form.submit();    
    }
    
    function viriyaform(){
        var form = document.getElementById('formUpdate');
        
        var vehProvIdText = $("#vehProvId option:selected").html();
        if( $("#vehProvId").val() == 0 ){
            var vehProvIdText = "";    
        }
        $("#vehProvName").val(vehProvIdText);
        
        var manuName = $("#manuId option:selected").html();
        if( $("#manuId").val() == 0 ){
            manuName = "";
        }
        $("#manuName").val(manuName);
        
        var modelName = $("#modelId option:selected").html();
        if( $("#modelId").val() == 0 ){
            modelName = "";
        }
        $("#modelName").val(modelName);
        
        var driveProvName = $("#driverList_0_driveProvId option:selected").html();
        if( $("#driverList_0_driveProvId").val() == 0 ){
            driveProvName = "";
        }
        $("#driverList_0_driveProvName").val(driveProvName);
        
        var driveProvName = $("#driverList_1_driveProvId option:selected").html();
        if( $("#driverList_1_driveProvId").val() == 0 ){
            driveProvName = "";
        }
        $("#driverList_1_driveProvName").val(driveProvName);
        
        form.method = "POST";
        form.target = "_blank";
        form.action = "{{ url('viriya') }}";
        form.submit();    
    }
    
    window.onbeforeunload = function (e) {
        if(isChange){
            e = e || window.event;

            // For IE and Firefox prior to version 4
            if (e) {
                e.returnValue = 'Sure?';
            }

            // For Safari
            return 'Sure?';
        }
    };
    
</script>
@endsection