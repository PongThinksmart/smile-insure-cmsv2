<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use App\Model\Api;
use App\Helper\PdfExport;
use App\Helper\StringHelper;
use Endroid\QrCode\QrCode;
use App\Http\Controllers\QuotationController;

class PdfController extends Controller
{
    
    private $pdf;
    
    function __construct() {
        //parent::__construct();
        $this->pdf = new PdfExport();
    }
    
    function printText($text=null, $x=0, $y=0, $align = "L", $v_align = "T", $widthArea = 0, $heightArea = 100, $isBorder = false){
        if( (strtoupper($align) == "L") && (strtoupper($v_align) == "T" ) && ($widthArea <= 0) ){
            if(!empty($text)){
                $this->pdf->Text($x, $y, iconv('UTF-8', 'TIS-620', $text));
            }else{                                                       
                $this->pdf->Image(url("img/none.png"), $x, $y-1.5, 1.5, 1.5);
            }    
        }else{
            if(!empty($text)){
                $this->pdf->SetXY($x,$y-5);
                $this->pdf->drawTextBox(
                    iconv('UTF-8', 'TIS-620',$text),
                    $widthArea,
                    $heightArea,
                    $align,
                    $v_align,
                    $isBorder
                );
            }else{
                $this->pdf->SetXY($x,$y-5);
                $this->pdf->drawTextBox(
                    "-",
                    $widthArea,
                    $heightArea,
                    $align,
                    $v_align,
                    $isBorder
                );
            }    
        }
            
    }
    
    private function normalizeData($request){
        $arrData = [];
        $arrData['workType'] = $request->has('workType') ? intval($request->input('workType')) : null;
        $arrData['insureType'] = $request->has('insureType') ? intval($request->input('insureType')) : null;
        $arrData['repairType'] = $request->has('repairType') ? intval($request->input('repairType')) : null;
        $arrData['partnerName'] = $request->has('partnerName') ? $request->input('partnerName') : null;
        $arrData['insureNo'] = $request->has('insureNo') ? $request->input('insureNo') : null;
        $arrData['custName'] = $request->has('custName') ? $request->input('custName') : null;
        //$arrData['custId'] = $request->has('custId') ? sprintf ("%010d", $request->input('custId')) : null;
        $arrData['custId'] = $request->has('vehId') ? sprintf ("%010d", $request->input('vehId')) : null;
        $arrData['vehId'] = $request->has('vehId') ?$request->input('vehId') : null;
        $arrData['idCardNumber'] = $request->has('idCardNumber') ? $request->input('idCardNumber') : null;
        $arrData['vehTypeId'] = $request->has('vehTypeId') ? intval($request->input('vehTypeId')) : null;
        $arrData['vehTypeName'] = $request->has('vehTypeName') ? $request->input('vehTypeName') : null;
        if(!empty($arrData['vehTypeId'])){
            switch($arrData['vehTypeId']){
                case 54 : $arrData['vehTypeCode'] = 110; break;
                case 55 : $arrData['vehTypeCode'] = 120; break;
                case 56 : $arrData['vehTypeCode'] = 210; break;
                case 57 : $arrData['vehTypeCode'] = 220; break;
                case 58 : $arrData['vehTypeCode'] = 230; break;
                case 59 : $arrData['vehTypeCode'] = 320; break;
                case 61 : $arrData['vehTypeCode'] = 420; break;
                case 62 : $arrData['vehTypeCode'] = 520; break;
                case 63 : $arrData['vehTypeCode'] = 540; break;
                case 64 : $arrData['vehTypeCode'] = 610; break;
                case 65 : $arrData['vehTypeCode'] = 620; break;
                case 66 : $arrData['vehTypeCode'] = 630; break;
                case 67 : $arrData['vehTypeCode'] = 730; break;
            }
        }else{
            $arrData['vehTypeCode'] = null;    
        }
            
        $arrData['receiveDate'] = $request->has('receiveDate') ? $request->input('receiveDate') : null;
        if(!empty($arrData['receiveDate'])){
            $arrData['receiveDateY'] = date( "Y", strtotime($arrData['receiveDate']) );
            $arrData['receiveDateM'] = date( "m", strtotime($arrData['receiveDate']) );
            $arrData['receiveDateD'] = date( "d", strtotime($arrData['receiveDate']) );
        }else{
            $arrData['receiveDateY'] = null;
            $arrData['receiveDateM'] = null;
            $arrData['receiveDateD'] = null;
        }
        $arrData['receiveExpireDate'] = $request->has('receiveExpireDate') ? $request->input('receiveExpireDate') : null;
        if(!empty($arrData['receiveExpireDate'])){
            $arrData['receiveExpireDateY'] = date( "Y", strtotime($arrData['receiveExpireDate']) );
            $arrData['receiveExpireDateM'] = date( "m", strtotime($arrData['receiveExpireDate']) );
            $arrData['receiveExpireDateD'] = date( "d", strtotime($arrData['receiveExpireDate']) );
        }else{
            $arrData['receiveExpireDateY'] = null;
            $arrData['receiveExpireDateM'] = null;
            $arrData['receiveExpireDateD'] = null;
        }
        $arrData['beneficiary'] = $request->has('beneficiary') ? $request->input('beneficiary') : null;
        $arrData['beneficiary_name'] = $request->has('beneficiary_name') ? $request->input('beneficiary_name') : null;
        $arrData['phoneNo'] = $request->has('phoneNo') ? $request->input('phoneNo') : null;
        $arrData['fullAddr'] = null;
        
        if( $request->has('phoneListSelect') && $request->has('phoneList') ){
            $select = intval($request->input('phoneListSelect'));
            foreach( $request->input('phoneList') as $key => $item){
                if( $key == $select ){
                    $arrData['phoneNo'] = $item['phoneNo'];    
                }
            }
        }
        
        if( $request->has('addressListSelect') && $request->has('addressList') ){
            $select = intval($request->input('addressListSelect'));
            foreach( $request->input('addressList') as $key => $item){
                if( $key == $select ){
                    $arrData['fullAddr'] = $item['address'] . " " . $item['distName'] . " " . $item['amphName'] . " " . $item['provName'] . " " .$item['zipCode'];    
                }
            }
        }
        
        $arrData['driverList'] = [];
        if( $request->has('driverRecrive') && $request->has('driverList') ){
            if($request->input('driverRecrive')){
                foreach( $request->input('driverList') as  $item){
                    if(!empty($item['birthDate'])){
                        $item['birthDateY'] = date( "Y", strtotime($item['birthDate']) );
                        $item['birthDateM'] = date( "m", strtotime($item['birthDate']) );
                        $item['birthDateD'] = date( "d", strtotime($item['birthDate']) );
                    }else{
                        $item['birthDateY'] = null;
                        $item['birthDateM'] = null;
                        $item['birthDateD'] = null;
                    }
                    $arrData['driverList'][] = $item;
                }    
            }
        }
        $arrData['manuName'] = $request->has('manuName') ? $request->input('manuName') : null;
        $arrData['modelName'] = $request->has('modelName') ? $request->input('modelName') : null;
        $arrData['vehLicenseNo'] = $request->has('vehLicenseNo') ? $request->input('vehLicenseNo') : null;
        $arrData['vehProvName'] = $request->has('vehProvName') ? $request->input('vehProvName') : null;
        $arrData['chasisNo'] = $request->has('chasisNo') ? $request->input('chasisNo') : null;
        $arrData['engineNo'] = $request->has('engineNo') ? $request->input('engineNo') : null;
        $arrData['modelYear'] = $request->has('modelYear') ? $request->input('modelYear') : null;
        $arrData['bodyType'] = $request->has('bodyType') ? $request->input('bodyType') : null;
        $arrData['passenger'] = $request->has('passenger') ? intval($request->input('passenger')) : null;
        $arrData['passenger'] = $request->has('passenger') ? intval($request->input('passenger')) : null;
        $arrData['engineCapacity'] = $request->has('engineCapacity') ? intval($request->input('engineCapacity')) : null;
        $arrData['accessory'] = $request->has('accessory') ? $request->input('accessory') : null;
        $arrData['accessoryPrice'] = $request->has('accessoryPrice') ? floatval($request->input('accessoryPrice')) : null;
        $arrData['extLifeDamagePerPerson'] = $request->has('extLifeDamagePerPerson') ? floatval($request->input('extLifeDamagePerPerson')) : null;
        $arrData['extLifeDamagePerTime'] = $request->has('extLifeDamagePerTime') ? floatval($request->input('extLifeDamagePerTime')) : null;
        $arrData['extAssetDamagePerTime'] = $request->has('extAssetDamagePerTime') ? floatval($request->input('extAssetDamagePerTime')) : null;
        $arrData['extExcessPerTime'] = $request->has('extExcessPerTime') ? floatval($request->input('extExcessPerTime')) : null;
        $arrData['vehDamagePerTime'] = $request->has('vehDamagePerTime') ? floatval($request->input('vehDamagePerTime')) : null;
        $arrData['vehExcessPerTime'] = $request->has('vehExcessPerTime') ? floatval($request->input('vehExcessPerTime')) : null;
        $arrData['vehLostPerTime'] = $request->has('vehLostPerTime') ? floatval($request->input('vehLostPerTime')) : null;
        $arrData['permDisabilityDriver'] = $request->has('permDisabilityDriver') ? floatval($request->input('permDisabilityDriver')) : null;
        $arrData['permDisabilityPassengerPerson'] = $request->has('permDisabilityPassengerPerson') ? intval($request->input('permDisabilityPassengerPerson')) : null;
        $arrData['permDisabilityPassenger'] = $request->has('permDisabilityPassenger') ? floatval($request->input('permDisabilityPassenger')) : null;
        $arrData['tempDisabilityDriver'] = $request->has('tempDisabilityDriver') ? floatval($request->input('tempDisabilityDriver')) : null;
        $arrData['tempDisabilityPassengerPerson'] = $request->has('tempDisabilityPassengerPerson') ? intval($request->input('tempDisabilityPassengerPerson')) : null;
        $arrData['tempDisabilityPassenger'] = $request->has('tempDisabilityPassenger') ? floatval($request->input('tempDisabilityPassenger')) : null;
        $arrData['treatmentDriver'] = $request->has('treatmentDriver') ? floatval($request->input('treatmentDriver')) : null;
        $arrData['bailDriver'] = $request->has('bailDriver') ? floatval($request->input('bailDriver')) : null;
        $arrData['changeOfInsure'] = $request->has('changeOfInsure') ? floatval($request->input('changeOfInsure')) : null;
        $arrData['dutyInsure'] = $request->has('dutyInsure') ? floatval($request->input('dutyInsure')) : null;
        $arrData['taxInsure'] = $request->has('taxInsure') ? floatval($request->input('taxInsure')) : null;
        $arrData['totalInsure'] = $request->has('totalInsure') ? floatval($request->input('totalInsure')) : null;
        $arrData['contractDate'] = $request->has('contractDate') ? $request->input('contractDate') : null;
        if(!empty($arrData['contractDate'])){
            $arrData['contractDateY'] = date( "Y", strtotime($arrData['contractDate']) );
            $arrData['contractDateM'] = date( "m", strtotime($arrData['contractDate']) );
            $arrData['contractDateD'] = date( "d", strtotime($arrData['contractDate']) );
        }else{
            $arrData['contractDateY'] = null;
            $arrData['contractDateM'] = null;
            $arrData['contractDateD'] = null;
        }
        $arrData['saleAgentName'] = $request->has('saleAgentName') ? $request->input('saleAgentName') : null;
        
        $arrData['actType'] = $request->has('actType') ? intval($request->input('actType')) : null;
        $arrData['actTypeName'] = $request->has('actTypeName') ? $request->input('actTypeName') : null;
        $arrData['changeOfCaract'] = $request->has('changeOfCaract') ? floatval($request->input('changeOfCaract')) : null;
        $arrData['dutyCaract'] = $request->has('dutyCaract') ? floatval($request->input('dutyCaract')) : null;
        $arrData['taxCaract'] = $request->has('taxCaract') ? floatval($request->input('taxCaract')) : null;
        $arrData['totalCaract'] = $request->has('totalCaract') ? floatval($request->input('totalCaract')) : null;
        
        $arrData['agentDiscount'] = $request->has('agentDiscount') ? floatval($request->input('agentDiscount')) : null;
        $arrData['totalPaymentInsure'] = $request->has('totalPaymentInsure') ? floatval($request->input('totalPaymentInsure')) : null;
        $arrData['freeCarAct'] = $request->has('freeCarAct') ? $request->input('freeCarAct') : null;
        $arrData['payType'] = $request->has('payType') ? $request->input('payType') : null;
        $arrData['selectPaydown'] = $request->has('selectPaydown') ? $request->input('selectPaydown') : null;
        $arrData['orderId'] = $request->has('orderId') ? intval($request->input('orderId')) : null;
        $arrData['dueDate'] = array();
        $arrData['amount'] = array();
        $arrData['paymentStatus'] = array();
        if($request->has('paymentList')){
            $paymentList = $request->input('paymentList');
            for($i=1;$i<=6;$i++){
                $arrData['dueDate'][$i] = !empty($paymentList[$i]['dueDate']) ? $paymentList[$i]['dueDate'] : null;    
                $arrData['paymentStatus'][$i] = !empty($paymentList[$i]['status']) ? intval($paymentList[$i]['status']) : null;    
                $arrData['amount'][$i] = !empty($paymentList[$i]['amount']) ? floatval($paymentList[$i]['amount']) : null;    
            }    
        }else{
            for($i=1;$i<=6;$i++){
                $arrData['dueDate'][$i] = null;    
                $arrData['amount'][$i] = null;    
            }
        }

        $arrData['refNo'] = $request->has('refNo') ? $request->input('refNo') : null;
        $arrData['carInfo'] = $request->has('carInfo') ? $request->input('carInfo') : null;
        $arrData['isSpecifyDriver'] = $request->has('isSpecifyDriver') ? intval($request->input('isSpecifyDriver')) : 0;
        $arrData['driverList'] = array();
        if( $request->has('isSpecifyDriver') && $request->has('driverList') ){
            foreach( $request->input('driverList') as  $item){
                $arrData['driverList'][] = $item;
            }
        }
        
        $arrData['quotationList'] = array();
        if( $request->has('itemList') ){
            foreach( $request->input('itemList') as  $item){
                $arrData['quotationList'][] = $item;
            }
        }
        $arrData['promotionStaticRow1'] = $request->has('promotionStaticRow1') ? $request->input('promotionStaticRow1') : null;
        $arrData['promotionStaticRow2'] = $request->has('promotionStaticRow2') ? $request->input('promotionStaticRow2') : null;
        $arrData['promotionStaticRow3'] = $request->has('promotionStaticRow3') ? $request->input('promotionStaticRow3') : null;
        
        $arrData['proposedDate'] = $request->has('proposedDate') ? $request->input('proposedDate') : null;

        return $arrData;           
    }
    
    private function normalizeViriyaData($request){
        $arrData = $request->all();
        
        return $arrData;           
    }
    
    public function sqa(Request $request){
        
        $this->pdf->AddPage();
        $this->pdf->Image(url("img/appsm_bg.jpg"), 4, 5, 201, 289);
        $this->pdf->AddFont('cordia','','cordia.php');
        $this->pdf->SetFont('cordia','',12);
        $this->pdf->SetTextColor(10,10,255);
        
        $arrData = $this->normalizeData($request);

        if(!empty($arrData['workType'])){
            switch($arrData['workType']){
                case 1 :
                case 3 :$this->pdf->Image(url("img/checkbox.png"), 33.3, 43.5, 3, 3);
                        break;
                case 2 :$this->pdf->Image(url("img/checkbox.png"), 54, 43.5, 3, 3);
                        break;
            }
        }
        
        if(!empty($arrData['insureType'])){
            $pos_x = 70;
            switch($arrData['insureType']){
                case 7 :$pos_x = $pos_x + 20;
                case 5 :$pos_x = $pos_x + 18;
                case 8 :$pos_x = $pos_x + 20;
                case 6 :$pos_x = $pos_x + 20;
                case 4 :$this->pdf->Image(url("img/checkbox.png"), $pos_x, 43.5, 3, 3);
                        break;
            }
        }
        
        if(!empty($arrData['repairType'])){
            $pos_x = 178;
            switch($arrData['repairType']){
                case 2 :$pos_x = $pos_x + 10;
                case 1 :$this->pdf->Image(url("img/checkbox.png"), $pos_x, 43.5, 3, 3);
                        break;
            }
        }
        
        $this->printText($arrData['partnerName'], 37, 53);
        $this->printText($arrData['insureNo'], 145, 53);
        $this->printText($arrData['custName'], 40, 58);
        $this->printText($arrData['custId'], 143, 58);
        $this->printText($arrData['idCardNumber'], 65, 63);
        $this->printText($arrData['phoneNo'], 141, 63);
        $this->printText($arrData['fullAddr'], 25, 68);

        for($i=0;$i<2;$i++){
            $item = !empty($arrData['driverList'][$i]) ? $arrData['driverList'][$i] : null;
            
            $this->printText($item['driverName'], 26, 75.5+($i*5));
            $this->printText($item['birthDateD'], 109, 75.5+($i*5));
            $this->printText($item['birthDateM'], 115, 75.5+($i*5));
            $this->printText($item['birthDateY'], 121, 75.5+($i*5));
            $this->printText($item['occupation'], 135, 75.5+($i*5));
        }
        
        if(!empty($arrData['vehTypeId'])){
            $pos_x = 39;
            switch($arrData['vehTypeId']){
                case 67 : 
                case 58 : $pos_x = $pos_x + 34;
                case 63 : $pos_x = $pos_x + 27;
                case 65 :
                case 62 : 
                case 61 : 
                case 59 : 
                case 57 : $pos_x = $pos_x + 20;
                case 64 : 
                case 56 : 
                case 54 : $this->pdf->Image(url("img/checkbox.png"), $pos_x, 85, 3, 3);
                          break;
                default : $this->pdf->Image(url("img/checkbox.png"), 147, 85, 3, 3);
                          $this->printText($arrData['vehTypeName'], 162, 88);
                          break;
            }
        }
        
        $this->printText($arrData['receiveDate'], 55, 94);
        $this->printText($arrData['receiveExpireDate'], 135, 94);
        if($arrData['beneficiary']){
            $this->pdf->Image(url("img/checkbox.png"), 57, 97.5, 3, 3);
            $this->printText($arrData['beneficiary_name'], 70, 100);    
        }else{
            $this->pdf->Image(url("img/checkbox.png"), 39, 97.5, 3, 3);
            $this->printText('', 70, 100);
        }
        
        $this->printText("1", 16, 125);
        $this->printText($arrData['vehTypeCode'], 26, 125);
        $this->printText($arrData['manuName'], 35, 120, "C", "M", 27, 17);
        $this->printText($arrData['modelName'], 62, 120, "C", "M", 25, 17);
        $this->printText($arrData['vehLicenseNo'], 87.4, 120, "C", "M", 21.5, 8);
        $this->printText($arrData['vehProvName'], 87.4, 128, "C", "M", 21.5, 8);
        $this->printText($arrData['chasisNo'], 109, 120, "C", "M", 29.3, 6);
        $this->printText($arrData['engineNo'], 109, 131, "C", "M", 29.3, 6);
        $this->printText($arrData['modelYear'], 138, 120, "C", "M", 11, 17);
        $this->printText($arrData['bodyType'], 149, 120, "C", "M", 18.5, 17);
        $this->printText(
            $arrData['passenger'] . "/" . (empty($arrData['engineCapacity']) ? "-" : number_format($arrData['engineCapacity'],0)) . "/-",
            167.5,
            120,
            "C",
            "M",
            30,
            17
        );
        
        if(!empty($arrData['accessory'])){
            $this->pdf->Image(url("img/checkbox.png"), 79, 134, 3, 3);
        }else{
            $this->pdf->Image(url("img/checkbox.png"), 65, 134, 3, 3);                                                            
        }
        $this->printText($arrData['accessory'], 95, 136);
        $this->printText(number_format($arrData['accessoryPrice'],2), 160, 136);
        $this->printText(number_format($arrData['extLifeDamagePerPerson']), 32, 165);
        $this->printText(number_format($arrData['extLifeDamagePerTime']), 32, 171);
        $this->printText(number_format($arrData['extAssetDamagePerTime']), 32, 182);
        $this->printText(number_format($arrData['extExcessPerTime']), 32, 193);
        $this->printText(number_format($arrData['vehDamagePerTime']), 93, 160);
        $this->printText(number_format($arrData['vehExcessPerTime']), 93, 171);
        $this->printText(number_format($arrData['vehLostPerTime']), 93, 182);
        $this->printText(number_format($arrData['permDisabilityDriver']), 165, 165.5);
        $this->printText(number_format($arrData['permDisabilityPassengerPerson']), 154, 171);
        $this->printText(number_format($arrData['permDisabilityPassenger']), 167, 171);
        $this->printText(number_format($arrData['tempDisabilityDriver']), 165, 183);
        $this->printText(number_format($arrData['tempDisabilityPassengerPerson']), 154, 188.5);
        $this->printText(number_format($arrData['tempDisabilityPassenger']), 167, 188.5);
        $this->printText(number_format($arrData['treatmentDriver']), 163, 194.5);
        $this->printText(number_format($arrData['bailDriver']), 165, 201);
                                                             
        $this->pdf->Image(url("img/none.png"), 70, 209.5-2, 1.5, 1.5);
        $this->pdf->Image(url("img/none.png"), 165, 209.5-2, 1.5, 1.5);
        $this->pdf->Image(url("img/none.png"), 80, 215.5-2, 1.5, 1.5);
        
        $this->printText(number_format($arrData['changeOfInsure'],2), 43, 223.5);
        $this->printText(number_format($arrData['dutyInsure'],2), 83, 223.5);
        $this->printText(number_format($arrData['taxInsure'],2), 115, 223.5);
        $this->printText(number_format($arrData['totalInsure'],2), 152, 223.5);
        
        $this->printText($arrData['receiveDateD'], 87, 243.5);
        $this->printText($arrData['receiveDateM'], 93, 243.5);
        $this->printText($arrData['receiveDateY'], 99, 243.5);
        $this->printText($arrData['receiveExpireDateD'], 120, 243.5);
        $this->printText($arrData['receiveExpireDateM'], 126, 243.5);
        $this->printText($arrData['receiveExpireDateY'], 132, 243.5);
        $this->printText($arrData['contractDateD'], 180, 248.5);
        $this->printText($arrData['contractDateM'], 186, 248.5);
        $this->printText($arrData['contractDateY'], 192, 248.5);
        $this->printText($arrData['saleAgentName'], 134, 248.5);

        $this->pdf->Text(87, 230.5, iconv('UTF-8', 'TIS-620',"บริษัท สมายล์ อินชัวร์ โบรกเกอร์ จำกัด"));
        $this->pdf->Text(162, 230.5, iconv('UTF-8', 'TIS-620',"ว00016/2561"));
        $this->pdf->Image(url("img/checkbox.png"), 53, 228, 3, 3);
        $this->pdf->Output();
    }
    
    public function invoice(Request $request){
        $this->pdf->AddPage();
        $this->pdf->Image(url("img/smileinsure_invoice_new.jpg"), 4, 5, 201, 289);
        $this->pdf->AddFont('cordia','','cordia.php');
        $this->pdf->SetFont('cordia','',12);
        $this->pdf->SetTextColor(10,10,255);
        
        $arrData = $this->normalizeData($request);
        
        $this->printText($arrData['partnerName'], 36, 46);
        $this->printText(date("d"), 175, 46);
        $this->printText(date("m"), 181, 46);
        $this->printText(date("Y"), 187, 46);
        $this->printText($arrData['custName'], 38, 51);
        $this->printText($arrData['custId'], 140, 51);
        $this->printText($arrData['fullAddr'], 30, 56.5);
        
        $arrData['manuName'] = !empty($arrData['manuName']) ? $arrData['manuName'] : "-";
        $arrData['modelName'] = !empty($arrData['modelName']) ? $arrData['modelName'] : "-";
        $arrData['engineCapacity'] = !empty($arrData['engineCapacity']) ? number_format($arrData['engineCapacity'],0) : "-";
        $this->printText($arrData['manuName'] . "/" . $arrData['modelName'] . "/" . $arrData['engineCapacity'], 32, 61.5);
        
        $this->printText($arrData['vehLicenseNo'] . " " . $arrData['vehProvName'], 130, 61.5);
        $this->printText(str_replace("-","/",$arrData['receiveDate']) . " - " . str_replace("-","/",$arrData['receiveExpireDate']), 32, 67);
        $this->printText($arrData['saleAgentName'], 135, 67);
        
        $y_list = 80;
        $order = 1;
        if(!empty($arrData['insureType'])){
            $this->printText($order, 21, $y_list);
            switch($arrData['insureType']){
                case 4: $levelInsure = "1"; break;
                case 5: $levelInsure = "2+"; break;
                case 6: $levelInsure = "2"; break;
                case 7: $levelInsure = "3+"; break;
                case 8: $levelInsure = "3"; break;
            }
            $this->printText("ประกันภัยชั้น ". $levelInsure, 34, $y_list);  
            $this->printText(number_format($arrData['changeOfInsure'],2), 114.5, $y_list, "R", "M", 20.5, 7);  
            $this->printText(number_format($arrData['dutyInsure'],2), 135, $y_list, "R", "M", 20.5, 7);  
            $this->printText(number_format($arrData['taxInsure'],2), 155.7, $y_list, "R", "M", 20.5, 7);  
            $this->printText(number_format($arrData['totalInsure'],2), 176.5, $y_list, "R", "M", 20.5, 7);
            $y_list = $y_list + 5;
            $order++;  
        }
        
        if(!empty($arrData['actType'])){
            $this->printText($order, 21, $y_list);
            if($arrData['freeCarAct']){
                $this->printText("พรบ. :  ". $arrData['actTypeName'] . ' (ฟรี)', 34, $y_list);        
            }else{
                $this->printText("พรบ. :  ". $arrData['actTypeName'], 34, $y_list);    
            }
            
            $this->printText(number_format($arrData['changeOfCaract'],2), 114.5, $y_list, "R", "M", 20.5, 7);  
            $this->printText(number_format($arrData['dutyCaract'],2), 135, $y_list, "R", "M", 20.5, 7);  
            $this->printText(number_format($arrData['taxCaract'],2), 155.7, $y_list, "R", "M", 20.5, 7);  
            $this->printText(number_format($arrData['totalCaract'],2), 176.5, $y_list, "R", "M", 20.5, 7);  
        }
        
        $this->printText(number_format($arrData['agentDiscount'],2), 176.5, 100, "R", "M", 20.5, 7);  
        $this->printText(number_format($arrData['totalPaymentInsure'],2), 176.5, 106.5, "R", "M", 20.5, 7);  
        $this->printText(number_format($arrData['totalPaymentInsure'],2), 47, 123.5, "R", "M", 20.5, 7);
        
        $payRound = 0;
        if($arrData['payType'] == 1){
            $payRound = 1;    
        }else if($arrData['payType'] == 2){
            $payRound = $arrData['selectPaydown'];  
        }
        $this->printText($payRound, 110, 123);
        $orderId = "0000000000"; //empty($arrData['orderId']) ? "0000000000" : sprintf("%010d", $arrData['orderId']);   
        for($i=1;$i<=$payRound;$i++){
            $this->printText($orderId . $i . $arrData['selectPaydown'], 13, 137+(($i-1)*4)); 
            $this->printText($i, 37.5, 137+(($i-1)*5)); 
            $this->printText($arrData['dueDate'][$i], 67, 137+(($i-1)*5), "C", "M", 20.5, 7);  
            $this->printText(number_format($arrData['amount'][$i],2), 176.5, 137+(($i-1)*5), "R", "M", 20.5, 7);  
        }
        
        $code = "|010556007065200\r" . $arrData['custId'] . "\r" . $orderId . "\r000";
                
        $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
        $qrCode = new QrCode($code);
        $qrCode->setWriterByName('png');
        
        $this->pdf->Image("data:image/png;base64," . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_128)) , 103, 215, 90.5, 10, 'PNG');
        $this->pdf->Image("data:image/png;base64," . base64_encode($qrCode->writeString()) , 83, 214, 18, 0, 'PNG');
        
        $this->pdf->Output();
    }
    
    public function bill($index = 0, Request $request){
        $this->pdf->AddFont('cordia','','cordia.php');
        $this->pdf->SetTextColor(10,10,255);
        
        $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
        $orderId = "0000000000"; //empty($arrData['orderId']) ? "0000000000" : sprintf("%010d", $arrData['orderId']);
        
        $arrData = $this->normalizeData($request);
        
        $i = 1;
        $payRound = 0;
        if($arrData['payType'] == 1){
            $arrData['selectPaydown'] = 1;    
        }
        if($index > 0){
            $i = $index;    
            $payRound = $index;
        }else{
            $payRound = $arrData['selectPaydown'];
        }
        
        for(;$i<=$payRound;$i++){
            if($arrData['paymentStatus'][$i]!=0){
                continue;
            }
            
            $this->pdf->SetFont('cordia','',12);
            $this->pdf->AddPage();
            $this->pdf->Image(url("img/smileinsure_applicationform_blank.jpg"), 4, 5, 201, 289);
            
            $priceprint = number_format($arrData['amount'][$i],2,'','');
            
            $code = "|010556007065200\r" . $arrData['custId'] . "\r" . $orderId . $i . $arrData['selectPaydown'] . "\r" . $priceprint;

            $qrCode = new QrCode($code);
            $qrCode->setWriterByName('png');

            $this->pdf->Image("data:image/png;base64," . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_128)) , 95, 115, 103.5, 10, 'PNG');
            $this->pdf->Image("data:image/png;base64," . base64_encode($generator->getBarcode($code, $generator::TYPE_CODE_128)) , 95, 257, 103.5, 10, 'PNG');
            
            $this->pdf->Image("data:image/png;base64," . base64_encode($qrCode->writeString()), 17, 115, 18, 0, 'PNG');
            $this->pdf->Image("data:image/png;base64," . base64_encode($qrCode->writeString()), 17, 257, 18, 0, 'PNG');
            
            $this->printText($arrData['custName'], 140, 52);
            $this->printText($arrData['custName'], 140, 194);
            
            $this->printText($arrData['phoneNo'], 140, 57);
            $this->printText($arrData['phoneNo'], 140, 199);
            
            $this->printText($arrData['custId'], 145, 67);
            $this->printText($arrData['custId'], 145, 209);
            
            $this->printText($orderId . $i . $arrData['selectPaydown'], 145, 72);
            $this->printText($orderId . $i . $arrData['selectPaydown'], 145, 214);
                        
            $this->printText($arrData['dueDate'][$i], 142, 47);
            $this->printText($arrData['dueDate'][$i], 142, 189);
                                    
            $this->printText(StringHelper::getThaiPrice($arrData['amount'][$i]), 18, 104);
            $this->printText(StringHelper::getThaiPrice($arrData['amount'][$i]), 18, 246);
            
            //$pdf->Text(18, 104, iconv('UTF-8', 'TIS-620', Yii::$app->WebUtils->getThaiPrice($price)));
            //$pdf->Text(18, 246, iconv('UTF-8', 'TIS-620', Yii::$app->WebUtils->getThaiPrice($price)));
            
            $this->pdf->SetFont('cordia','',20);
            
            if(empty($arrData['selectPaydown'])){
                $this->printText($i, 175, 92);
                $this->printText($i, 175, 234);
            }else{
                $this->printText($i . "/" . $arrData['selectPaydown'], 175, 92);
                $this->printText($i . "/" . $arrData['selectPaydown'], 175, 234);
            }                                                                          

            $this->printText(number_format($arrData['amount'][$i],2), 150, 103, "R", "T", 49, 15);
            $this->printText(number_format($arrData['amount'][$i],2), 150, 245, "R", "T", 49, 15);
        }
        $this->pdf->Output();
    }
    
    public function receipt($index = 0, Request $request){
        $this->pdf->AddFont('cordia','','cordia.php');
        $this->pdf->SetTextColor(10,10,255);
        
        $arrData = $this->normalizeData($request);
        
        $i = 1;
        $payRound = 0;
        if($arrData['payType'] == 1){
            $arrData['selectPaydown'] = 1;    
        }
        if($index > 0){
            $i = $index;    
            $payRound = $index;
        }else{
            $payRound = $arrData['selectPaydown'];
        }
        
        for(;$i<=$payRound;$i++){
            if($arrData['paymentStatus'][$i]!=1){
                continue;
            }
            
            $this->pdf->SetFont('cordia','',14);
            $this->pdf->AddPage();
            $this->pdf->Image(url("img/smileinsure_Proof_of_payment_A4.jpg"), 4, 5, 201, 289);
            
            $this->printText($arrData['custName'], 35, 58);
            $this->printText($arrData['custName'], 35, 206);
            
            $this->printText($arrData['phoneNo'], 155, 58);
            $this->printText($arrData['phoneNo'], 155, 206);
            
            $arrData['manuName'] = !empty($arrData['manuName']) ? $arrData['manuName'] : "-";
            $arrData['modelName'] = !empty($arrData['modelName']) ? $arrData['modelName'] : "-";
            $arrData['engineCapacity'] = !empty($arrData['engineCapacity']) ? number_format($arrData['engineCapacity'],0) : "-";

            $this->printText($arrData['manuName'] . "/" . $arrData['modelName'] . "/" . $arrData['engineCapacity'], 42, 64);
            $this->printText($arrData['manuName'] . "/" . $arrData['modelName'] . "/" . $arrData['engineCapacity'], 42, 213);
            
            $this->printText($arrData['vehLicenseNo'] . " " . $arrData['vehProvName'], 33, 71);
            $this->printText($arrData['vehLicenseNo'] . " " . $arrData['vehProvName'], 33, 219);
            
            $this->printText($arrData['modelYear'], 115, 71);
            $this->printText($arrData['modelYear'], 115, 219);
            
            $this->printText($arrData['partnerName'], 36, 78);
            $this->printText($arrData['partnerName'], 36, 226);
            
            $levelInsure = "";
            switch($arrData['insureType']){
                case 4: $levelInsure = "1"; break;
                case 5: $levelInsure = "2+"; break;
                case 6: $levelInsure = "2"; break;
                case 7: $levelInsure = "3+"; break;
                case 8: $levelInsure = "3"; break;
            }
            
            $this->printText($levelInsure, 180, 78);
            $this->printText($levelInsure, 180, 226);
            
            $this->printText($arrData['totalPaymentInsure'], 56, 85);
            $this->printText($arrData['totalPaymentInsure'], 56, 233);
            
            $this->printText($arrData['selectPaydown'], 150, 85);
            $this->printText($arrData['selectPaydown'], 150, 233);
            
            $this->printText($arrData['fullAddr'], 30, 92);
            $this->printText($arrData['fullAddr'], 30, 240);

            $this->pdf->SetFont('cordia','',16);
            
            $this->printText($i, 143, 114);
            $this->printText($i, 143, 262);
            
            $this->printText(number_format($arrData['amount'][$i],2), 153, 114, 'R', 'T', 44, 15);
            $this->printText(number_format($arrData['amount'][$i],2), 153, 262, 'R', 'T', 44, 15);
        }
        $this->pdf->Output();
    }
    
    public function quotation(Request $request){
        $returnData = QuotationController::update($request->all());
        $returnData = json_decode($returnData->content(),true);
        if(empty($returnData['result']['status'])){
            echo 'Cannot Preview in  PDF';
            exit;
        }
        if($returnData['result']['status'] != "success" ){
            echo 'Cannot Preview in  PDF';
            exit; 
        }
        $this->pdf->AddPage();
        $this->pdf->Image(url("img/smileinsure_quotation.jpg"), 4, 5, 201, 289);
        $this->pdf->AddFont('cordia','','cordia.php');
        $this->pdf->SetFont('cordia','',12);
        $this->pdf->SetTextColor(10,10,255);
        
        $arrData = $this->normalizeData($request);
        
        $this->printText($arrData['refNo'], 164, 16.5);
        $this->printText($arrData['saleAgentName'], 163, 21.5);
        $this->printText($arrData['custName'], 26, 50.3);
        $this->printText($arrData['carInfo'], 36, 55.3);
        $this->printText($arrData['vehLicenseNo'], 31, 60.3);
        $this->printText($arrData['phoneNo'], 32, 65.3);
        if($arrData['isSpecifyDriver']){
            $i=0;
            $this->pdf->Image(url("img/checkbox.png"), 124.7, 47.5, 3, 3);
            foreach($arrData['driverList'] as $data){
                $this->printText($data['driverName'], 139, 50.3+(5*$i));
                $this->printText($data['age'], 177, 50.3+(5*$i));
                $i++;        
            }    
        }else{
            $this->pdf->Image(url("img/checkbox.png"), 102.3, 47.5, 3, 3);
        }
        $this->printText($arrData['proposedDate'], 172, 65);
        $i = 0;
        foreach($arrData['quotationList'] as $item){
            $this->printText($item['quotItemName'], 91+($i*29),86, 'C','T',29,11);
            $this->printText(number_format($item['extLifeDamagePerPerson'],2), 91+($i*29),97, 'R','M',29,5.5);
            $this->printText(number_format($item['extLifeDamagePerTime'],2), 91+($i*29),102.5, 'R','M',29,5.5);
            $this->printText(number_format($item['extAssetDamagePerTime'],2), 91+($i*29),108, 'R','M',29,5);

            $this->printText(number_format($item['vehDamagePerTime'],2), 91+($i*29),118.5, 'R','M',29,5.5);
            $this->printText(number_format($item['vehLostPerTime'],2), 91+($i*29),124, 'R','M',29,5.5);
            $this->printText(number_format($item['vehExcessPerTime'],2), 91+($i*29),129.5, 'R','M',29,5.5);
            $this->printText($item['permDisabilityPassengerPerson'], 91+($i*29),135, 'R','M',29,5.3);
            $this->printText(number_format($item['permDisabilityPassenger'],2), 91+($i*29),140.3, 'R','M',29,5.3);
            $this->printText(number_format($item['treatmentPassenger'],2), 91+($i*29),145.6, 'R','M',29,5.3);
            $this->printText(number_format($item['bailDriver'],2), 91+($i*29),150.9, 'R','M',29,5.3);
            $this->printText($item['repairTypeName'], 91+($i*29),156.2, 'R','M',29,5.3);

            if(is_numeric($item['promotionCustom1'])){
                $this->printText(number_format($item['promotionCustom1'],2), 91+($i*29),166.8, 'R','M',29,5.3);    
            }else{
                $this->printText($item['promotionCustom1'], 91+($i*29),166.8, 'R','M',29,5.3);    
            }
            
            if(is_numeric($item['promotionCustom2'])){
                $this->printText(number_format($item['promotionCustom2'],2), 91+($i*29),172.1, 'R','M',29,5.3);    
            }else{
                $this->printText($item['promotionCustom2'], 91+($i*29),172.1, 'R','M',29,5.3);    
            }
            
            if(is_numeric($item['promotionCustom3'])){
                $this->printText(number_format($item['promotionCustom3'],2), 91+($i*29),177.4, 'R','M',29,5.3);    
            }else{
                $this->printText($item['promotionCustom3'], 91+($i*29),177.4, 'R','M',29,5.3);    
            }
            
            $this->printText(number_format($item['voluntaryPrice'],2), 91+($i*29),192, 'R','M',29,5.3);
            $this->printText(number_format($item['compulsoryPrice'],2), 91+($i*29),197.3, 'R','M',29,5.3);
            $this->printText(number_format($item['totalPrice'],2), 91+($i*29),202.6, 'R','M',29,4.5);
            $i++;   
        }
        
        $this->printText($arrData['promotionStaticRow1'], 15,166.8, 'L','M',29,5.3);
        $this->printText($arrData['promotionStaticRow2'], 15,172.1, 'L','M',29,5.3);
        $this->printText($arrData['promotionStaticRow3'], 15,177.4, 'L','M',29,5.3);

        $this->pdf->Output();
    }
    
    public function viriya(Request $request){
        $this->pdf->AddPage();
        $this->pdf->Image(url("img/v-bg.jpg"), 4, 5, 201, 289);
        $this->pdf->AddFont('cordia','','cordia.php');
        $this->pdf->SetFont('cordia','',12);
        $this->pdf->SetTextColor(10,10,255);
        
        $arrData = $this->normalizeViriyaData($request);
        //echo json_encode($arrData);
        
        $this->printText($arrData['branchName'], 163, 9.8);
        $this->printText($arrData['agentNo'], 169, 15);
        $this->printText($arrData['insureNo'], 169, 20.3);
        $this->printText($arrData['contractDate'], 169, 25.8);

        if($arrData['workType']==1){
            $this->pdf->Image(url("img/checkbox.png"), 40, 42.8, 3, 3);    
        }else if($arrData['workType']==2){
            $this->pdf->Image(url("img/checkbox.png"), 61.5, 42.8, 3, 3);
            $this->printText($arrData['oldPolicyNo'], 97, 45);    
        }
        
        if($arrData['insureType']==4){
            $this->pdf->Image(url("img/checkbox.png"), 148.5, 42.8, 3, 3);    
        }if($arrData['insureType']==6){
            $this->pdf->Image(url("img/checkbox.png"), 158.5, 42.8, 3, 3);    
        }if($arrData['insureType']==8){
            $this->pdf->Image(url("img/checkbox.png"), 168.5, 42.8, 3, 3);    
        }if($arrData['insureType']==9){
            $this->pdf->Image(url("img/checkbox.png"), 176, 42.8, 3, 3);
            $this->printText($arrData['insureTypeOther'], 186, 45);    
        }

        if(!empty($arrData['isPDF'])){
            $this->pdf->Image(url("img/checkbox.png"), 61, 48.5, 3, 3);    
        }
        $this->printText($arrData['email'], 139, 50);  
        $this->printText($arrData['custName'], 38, 55.3);  
        $this->printText($arrData['occupation'], 132, 55.3);  
        $this->printText($arrData['idCardNumber'], 65, 60.3);  
        
        if(!empty($arrData['isHq'])){
            if($arrData['isHq'] == -1)
                $this->pdf->Image(url("img/checkbox.png"), 128, 58, 3, 3);
            else{
                $this->pdf->Image(url("img/checkbox.png"), 154, 58, 3, 3);    
                $this->printText($arrData['branchNo'], 168, 60.3);
            }
        }
        $this->printText($arrData['fullAddr'], 20, 65.5);
        $this->printText($arrData['zipCode'], 113, 70.5);
        $this->printText($arrData['phoneNo'], 140, 70.5);
        
        if(empty($arrData['driverReceive'])){
            $this->pdf->Image(url("img/checkbox.png"), 61, 74, 3, 3);    
        }else{
            $this->pdf->Image(url("img/checkbox.png"), 98, 74, 3, 3);
            if(!empty($arrData['driverList'])){
                $i=0;
                foreach($arrData['driverList'] as $item){
                    
                    $this->printText($item['driverName'], 23, 81+($i*10));
                    $this->printText($item['birthDateD'], 102, 81+($i*10));
                    $this->printText($item['birthDateM'], 110, 81+($i*10));
                    $this->printText($item['birthDateY'], 122, 81+($i*10));
                    $this->printText($item['occupation'], 142, 81+($i*10));
                    $this->printText($item['driveLicenseNo'], 36, 86+($i*10));
                    $this->printText($item['driveExpiryDateD'], 102, 86+($i*10));
                    $this->printText($item['driveExpiryDateM'], 110, 86+($i*10));
                    $this->printText($item['driveExpiryDateY'], 122, 86+($i*10));
                    $this->printText($item['driveProvName'], 161, 86+($i*10));
                    $i++;
                    if($i==2)
                        break;    
                }    
            }
            
        }
        
        $this->printText($arrData['policyNo'], 41, 103);
        $this->printText($arrData['receiveExpireDate'], 146, 103);

        if($arrData['vehTypeSelect']==1){
            $this->pdf->Image(url("img/checkbox.png"), 34, 106.5, 3, 3);    
        }else if($arrData['vehTypeSelect']==2){
            $this->pdf->Image(url("img/checkbox.png"), 61, 106.5, 3, 3);    
        }else if($arrData['vehTypeSelect']==3){
            $this->pdf->Image(url("img/checkbox.png"), 98, 106.5, 3, 3);    
        }else if($arrData['vehTypeSelect']==4){
            $this->pdf->Image(url("img/checkbox.png"), 140.5, 106.5, 3, 3);    
        }else if($arrData['vehTypeSelect']==5){
            $this->printText($arrData['vehTypeName'], 181, 108.5);    
        }
        
        if(empty($arrData['beneficiary'])){
            $this->pdf->Image(url("img/checkbox.png"), 39, 113, 3, 3);    
        }else{
             $this->pdf->Image(url("img/checkbox.png"), 61, 113, 3, 3);
             $this->printText($arrData['beneficiary_name'], 82, 115);  
        }
        
        $this->printText(1, 16, 138); 
        $this->printText($arrData['vehTypeCode'], 25, 138); 
        $this->printText($arrData['manuName'], 34, 133, "C", "M", 28, 6);
        $this->printText($arrData['modelName'], 34, 139, "C", "M", 28, 6);
        $this->printText($arrData['vehLicenseNo'] . ' ' . $arrData['vehProvName'], 62, 133, "C", "M", 21.5, 18);
        $this->printText($arrData['chasisNo'], 83.5, 133, "C", "M", 40, 6);
        $this->printText($arrData['engineNo'], 83.5, 145, "C", "M", 40, 6);
        $this->printText($arrData['modelYear'], 123.5, 133, "C", "M", 12, 18);
        $this->printText($arrData['bodyType'], 135.5, 133, "C", "M", 16, 18);
        $this->printText($arrData['pCW'], 151.5, 133, "C", "M", 25, 18);
        $this->printText($arrData['totalInsure1'], 176.5, 133, "C", "M", 25, 18);
        
        $this->printText($arrData['accessory'], 14, 153.5);
        $this->printText($arrData['accessoryPrice'], 164, 155.5, "R", "T", 30, 4);
        
        if(!empty($arrData['isSpecialAccessory'])){
            if($arrData['isSpecialAccessory']==-1){
                $this->pdf->Image(url("img/checkbox.png"), 41, 156, 3, 3);    
            }else{
                $this->pdf->Image(url("img/checkbox.png"), 61, 156, 3, 3);
                if(!empty($arrData['SpecialAccessoryType'])){
                    switch(intval($arrData['SpecialAccessoryType'])){
                        case 1: $this->pdf->Image(url("img/checkbox.png"), 83, 156, 3, 3); break;
                        case 2: $this->pdf->Image(url("img/checkbox.png"), 108, 156, 3, 3); break;
                        case 3: $this->pdf->Image(url("img/checkbox.png"), 135, 156, 3, 3); break;
                        case 4: $this->pdf->Image(url("img/checkbox.png"), 165, 156, 3, 3);
                                $this->printText($arrData['SpecialAccessoryOther'], 174, 158.5);
                                break;
                    }    
                }    
            } 
        }
        
        $this->printText($arrData['extLifeDamagePerPerson'], 26, 181.5, "R", "T", 35, 4);
        $this->printText($arrData['extLifeDamagePerTime'], 26, 186, "R", "T", 35, 4);
        $this->printText($arrData['extAssetDamagePerTime'], 26, 195.5, "R", "T", 35, 4);
        $this->printText($arrData['extExcessPerTime'], 26, 204.5, "R", "T", 35, 4);
        
        $this->printText($arrData['vehDamagePerTime'], 84, 177, "R", "T", 35, 4);
        $this->printText($arrData['vehExcessPerTime'], 84, 186, "R", "T", 35, 4);
        $this->printText($arrData['vehLostPerTime'], 84, 195.5, "R", "T", 35, 4);
        
        $this->printText($arrData['permDisabilityDriver'], 158, 181, "R", "T", 35, 4);
        $this->printText($arrData['permDisabilityPassengerPerson'], 153, 185.5, "R", "T", 10, 4);
        $this->printText($arrData['permDisabilityPassenger'], 170, 185.5, "R", "T", 19, 4);
        
        $this->printText($arrData['tempDisabilityDriver'], 156, 194, "R", "T", 28, 4);
        $this->printText($arrData['tempDisabilityPassengerPerson'], 152, 198.5, "R", "T", 10, 4);
        $this->printText($arrData['tempDisabilityPassenger'], 168, 198.5, "R", "T", 11, 4);
 
        $this->printText($arrData['treatmentDriver'], 142, 206, "R", "T", 47, 4);
        $this->printText($arrData['bailDriver'], 142, 213.5, "R", "T", 47, 4);
        
        if(!empty($arrData['isTerrorism'])){
            $this->pdf->Image(url("img/checkbox.png"), 140.5, 215.5, 3, 3);    
        }
        
        $this->printText($arrData['totalInsure2'], 68, 224.8, "R", "T", 47, 4);
        $this->printText("", 73, 229.8, "R", "T", 40, 4);
        $this->printText("", 172, 226, "R", "T", 22, 4);
        
        $this->printText($arrData['receiveDateD'], 75, 239); 
        $this->printText($arrData['receiveDateM'], 86, 239); 
        $this->printText($arrData['receiveDateY'], 97, 239);
        
        $this->printText($arrData['receiveExpireDateD'], 122, 239); 
        $this->printText($arrData['receiveExpireDateM'], 133, 239); 
        $this->printText($arrData['receiveExpireDateY'], 147, 239); 
        
        $this->pdf->Output();
    }
    
    public function sentMessenger(Request $request){
        $arrData = $request->all();
        if(empty($arrData['sentMessengerId'])){
            echo 'Data is Empty, Please Select the data for printing.';
            echo "<META HTTP-EQUIV='Refresh' CONTENT = '3;URL=" . url('messenger/list') . "'>";    
        }else{
            $this->pdf->AddFont('cordia','','cordia.php');
            $this->pdf->SetFont('cordia','',12);
            $this->pdf->SetTextColor(0,0,0);
            $slot = 0;
            foreach($arrData['sentMessengerId'] as $id){
                $indexTag = 0;
                if($slot % 4 == 0){
                    $this->pdf->AddPage();   
                }
                $this->pdf->Image(url("img/Appointment-paper.jpg"), 0, 75*($slot % 4), 210, 74);
                $this->printText($arrData['sentMessenger'][$id]['detail'], 18, 18+(75*($slot % 4)), "L", "T", 93.5, 23.6, false);
                $this->printText($arrData['sentMessenger'][$id]['notificationDate'], 27, 10+(75*($slot % 4)), "L", "T", 93.5, 23.6, false);
                $this->printText($arrData['sentMessenger'][$id]['dueDateSM'], 33, 44.5+(75*($slot % 4)), "L", "T", 23, 4, false);
                $this->printText($arrData['sentMessenger'][$id]['dueTimeSM'], 71, 44.5+(75*($slot % 4)), "L", "T", 23, 4, false);
                $this->printText($arrData['sentMessenger'][$id]['note'], 43, 52+(75*($slot % 4)), "L", "T", 69, 18.5, false);
                
                if(!empty($arrData['sentMessenger'][$id]['receiveDocTag'])){
                    $this->printText(__('messages.receiveDoc'), 113, 18+($indexTag*5)+(75*($slot % 4)), "L", "T", 35, 4, false);
                    $this->printText($arrData['sentMessenger'][$id]['receiveDocDetail'], 140, 18+($indexTag*5)+(75*($slot % 4)), "L", "T", 58, 5, true);
                    $indexTag++;        
                }
                
                if(!empty($arrData['sentMessenger'][$id]['sentBill'])){
                    $this->printText(__('messages.sentReceipt'), 113, 18+($indexTag*5)+(75*($slot % 4)), "L", "T", 35, 4, false);
                    $indexTag++;      
                }
                
                if(!empty($arrData['sentMessenger'][$id]['sentReq'])){
                    $this->printText(__('messages.sentReq'), 113, 18+($indexTag*5)+(75*($slot % 4)), "L", "T", 35, 4, false);
                    $indexTag++;      
                }
                
                if(!empty($arrData['sentMessenger'][$id]['sentAct'])){
                    $this->printText(__('messages.sentAct'), 113, 18+($indexTag*5)+(75*($slot % 4)), "L", "T", 35, 4, false);
                    $indexTag++;      
                }
                
                if(!empty($arrData['sentMessenger'][$id]['sentDepartment'])){
                    $this->printText(__('messages.sentDepartment'), 113, 18+($indexTag*5)+(75*($slot % 4)), "L", "T", 35, 4, false);
                    $indexTag++;      
                }
                
                if(!empty($arrData['sentMessenger'][$id]['receiveFirstInstallmentTag'])){
                    $this->printText(__('messages.receiveFirstInstallment'), 113, 18+($indexTag*5)+(75*($slot % 4)), "L", "T", 35, 4, false);
                    $this->printText($arrData['sentMessenger'][$id]['receiveFirstInstallmentDetail'], 140, 18+($indexTag*5)+(75*($slot % 4)), "L", "T", 58, 5, true);
                    $indexTag++;      
                }
                
                if(!empty($arrData['sentMessenger'][$id]['photographTag'])){
                    $this->printText(__('messages.photograph'), 113, 18+($indexTag*5)+(75*($slot % 4)), "L", "T", 35, 4, false);
                    $this->printText($arrData['sentMessenger'][$id]['photographDetail'], 140, 18+($indexTag*5)+(75*($slot % 4)), "L", "T", 58, 5, true);
                    $indexTag++;      
                }
                
                if(!empty($arrData['sentMessenger'][$id]['receiveLastInstallmentTag'])){
                    $this->printText(__('messages.receiveLastInstallment'), 113, 18+($indexTag*5)+(75*($slot % 4)), "L", "T", 35, 4, false);
                    $this->printText($arrData['sentMessenger'][$id]['receiveLastInstallmentDetail'], 140, 18+($indexTag*5)+(75*($slot % 4)), "L", "T", 58, 5, true);
                    $indexTag++;      
                }
                
                
                $slot++;
            }
            $this->pdf->Output();                                                                                                                                
        }
        
    }
}