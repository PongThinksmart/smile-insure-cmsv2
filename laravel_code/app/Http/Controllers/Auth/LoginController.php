<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Api;
use Illuminate\Routing\Route;
//use Illuminate\Foundation\Auth\AuthenticatesUsers;


class LoginController extends Controller
{
    public function showLoginForm(Request $request)
    {
        if ($request->session()->get('authenticated',false) === true){
            return redirect(config( "user." . intval(session('userlevelId',0)) . ".urldefault" ));    
            //return redirect('customer');    
        }
        return view('login');
    }
    
    public function login(Request $request){  
        $data = array(
            "username" => $request->input('username'),
            "password" => base64_encode($request->input('password')),
        );
        $returnData = Api::request('login', $data);
        if(!empty($returnData['result'])){
            $request->session()->put('authenticated',true);
            $request->session()->put('username', $request->input('username'));
            $request->session()->put('userlevelId', $returnData['result']['agentLevelId']);
            $request->session()->put('userId', $returnData['result']['agentId']);
            $request->session()->put('agentLevelName', $returnData['result']['agentLevelName']);
            $request->session()->put('nickname', $returnData['result']['nickname']);
            $request->session()->put('agentName', $returnData['result']['agentName']);   
        }else{
            //return redirect('login');
        }
        return response()->json($returnData);
    }
    
    public function logout(Request $request){
        $request->session()->forget('authenticated');
        $request->session()->forget('username');
        $request->session()->forget('userlevelId');
        $request->session()->forget('userId');
        $request->session()->forget('agentLevelName');
        $request->session()->forget('nickname');
        $request->session()->forget('agentName');   
        return redirect('login');
    }
    
}
