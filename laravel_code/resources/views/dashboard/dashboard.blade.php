<?php
    $activeMenu = !empty($activeMenu) ? $activeMenu : '';
?>

<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin - Blank Page</title>

  <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}"/>
  <link rel="stylesheet" href="{{ asset('fontawesome-free/css/all.min.css') }}"/>
  <link rel="stylesheet" href="{{ asset('datatables/dataTables.bootstrap4.css') }}"/>
  <link rel="stylesheet" href="{{ asset('css/sb-admin.css') }}"/>
  <link rel="stylesheet" href="{{ asset('css/style.css') }}"/>
  <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css') }}"/>
  <link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css') }}"/>
  <link rel="stylesheet" href="{{ asset('css/bootstrap-datetimepicker.min.css') }}"/>
  
  @yield('css')

</head>

<body id="page-top">
     <!-- Sidebar -->
     <nav id="sidebar" class="active">
        <ul class="list-unstyled components">
            @include('dashboard.menu', [ 'activeMenu' => $activeMenu ] )
        </ul>
        <div id="sidebar_button">Menu</div>
     </nav>
     <nav class="navbar navbar-expand navbar-dark bg-dark static-top">
        <a class="navbar-brand mr-1" href="{{ url("") }}"><?php echo config('setting.' . env('APP_ENV') . '.sitetitle'); ?></a>
        <!--<button class="btn btn-link btn-sm text-white order-0 order-md-0" id="sidebarToggle" href="#">
            <i class="fas fa-bars"></i>
        </button>-->

        <!-- Navbar -->
        <ul class="navbar-nav ml-auto">
            <a class="mr-1 navbar-brand" href="#">Welcome, <?php echo session('nickname',""); ?></a>
            <li class="nav-item dropdown no-arrow">
                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-user-circle fa-fw"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <a class="dropdown-item" href="#">Edit Profile</a>
                    <div class="dropdown-divider"></div>
                    <a href="<?php echo url('logout'); ?>" class="dropdown-item" >Logout</a>
                </div>
            </li>
        </ul>
     </nav>

    <div id="wrapper">
        <div id="content-wrapper">
            <div class="container-fluid">
                <!-- Breadcrumbs-->
                <ol class="breadcrumb">
                    @isset($breadcrumb)
                        @foreach ($breadcrumb as $item)
                            @if( !empty($item['uri']) )
                            <li class="breadcrumb-item"><a href="{{ url($item['uri']) }}">{{ $item['name'] }}</a></li>
                            @else
                            <li class="breadcrumb-item">{{ $item['name'] }}</li>
                            @endif
                        @endforeach
                    @endisset
                </ol>

                <!-- Page Content -->
                @yield('content')

            </div>
            <!-- /.container-fluid -->
            @extends('footer')

        </div>
        <!-- /.content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    @include('alert')

	<!-- Bootstrap core JavaScript-->
	<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script> 
	<script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script type="text/javascript" src="{{ asset('js/jquery.easing.min.js') }}"></script>

    <!-- Custom scripts for all pages-->
    <script type="text/javascript" src="{{ asset('js/sb-admin.js') }}"></script>
    
    <!-- DataTable-->
    <script type="text/javascript" src="{{ asset('datatables/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('datatables/dataTables.bootstrap4.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bs-custom-file-input.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-select.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/autonumeric.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/moment.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/bootstrap-datetimepicker.min.js') }}"></script>
    
    <script type="text/javascript">
        $.fn.datepicker.defaults.format = "yyyy-mm-dd";
    </script>

    @yield('script')

</body>

</html>