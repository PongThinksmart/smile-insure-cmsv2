<?php

namespace App\Helper;

class StringHelper
{
    public static function getThaiPrice($price, $currency = "บาท", $subcurrency = "สตางค์", $baseLoop = true){
        $text = "";
        $numberThai = ["ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า"];
        $intvalue = floor($price);
        $isZero = true;
        $isEdCall = false;
        if($intvalue >= 1000000){
            $text = self::getThaiPrice(floor($intvalue/1000000),"","",false) . "ล้าน";
            $intvalue = intval(substr(strval($intvalue), -6));
            //echo $intvalue . '| ';
            $isZero = false;   
        }
        
        if($intvalue >= 100000){
            $text = $text . $numberThai[intval($intvalue/100000)] . "แสน";
            $intvalue = $intvalue%100000;
            $isZero = false;
            $isEdCall = true;   
        }
        
        if($intvalue >= 10000){
            $text = $text . $numberThai[intval($intvalue/10000)] . "หมื่น";
            $intvalue = $intvalue%10000;
            $isZero = false;
            $isEdCall = true;   
        }
        if($intvalue >= 1000){
            $text = $text . $numberThai[intval($intvalue/1000)] . "พัน";
            $intvalue = $intvalue%1000;
            $isZero = false;
            $isEdCall = true;   
        }
        if($intvalue >= 100){
            $text = $text . $numberThai[intval($intvalue/100)] . "ร้อย";
            $intvalue = $intvalue%100;
            $isZero = false;
            $isEdCall = true;   
        }
        if($intvalue >= 10){
            if(intval($intvalue/10) == 1){
                $text = $text . "สิบ";    
            }elseif(intval($intvalue/10) == 2){
                $text = $text . "ยี่สิบ";
            }else{
                $text = $text . $numberThai[intval($intvalue/10)] . "สิบ";    
            }
            $intvalue = $intvalue%10;
            $isZero = false;
            $isEdCall = true;   
        }
        if(($intvalue == 0) && ($isZero) && ($baseLoop)){
            $text = "ศูนย์";    
        }elseif(($intvalue == 1) && $isEdCall){
            $text = $text . "เอ็ด";
        }elseif($intvalue > 0){
            $text = $text . $numberThai[intval($intvalue)];
        }
        
        if($baseLoop){
            $text = $text . $currency;
            $arr_decimal = explode('.', number_format( $price, 2, '.', ','));
            
            if(!empty($arr_decimal[1])){
                $decimal = intval($arr_decimal[1]);
                if($decimal<=0){
                    $text = $text . "ถ้วน";    
                }else{
                    $isEdCall = false;
                    if($decimal >= 10){
                        if(intval($decimal/10) == 1){
                            $text = $text . "สิบ";    
                        }elseif(intval($decimal/10) == 2){
                            $text = $text . "ยี่สิบ";
                        }else{
                            $text = $text . $numberThai[intval($decimal/10)] . "สิบ";    
                        }
                        $decimal = $decimal%10;
                        $isEdCall = true;   
                    }
                    if(($decimal == 1) && $isEdCall){
                        $text = $text . "เอ็ด";
                    }elseif($decimal > 0){
                        $text = $text . $numberThai[intval($decimal)];
                    }
                    $text = $text . $subcurrency;    
                }    
            }else{
                $text = $text . "ถ้วน";   
            }
        }
        
        return $text;       
    }
}
