<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Model\Api;
use Exception;

class ImportCustomerController extends Controller
{
    public function index(){
        
        $date = date_create_from_format('d/m/Y', '02/02/2007');
        
        $optionData = Api::request("listSources",array("isWalkin" => null));
        $selectOptionList['listSources'] = !empty($optionData['result']['sources']) ? $optionData['result']['sources'] : array();
        return view('import.index', [ 'selectOptionList' => $selectOptionList ]);    
    }
    
    public function new_detail(Request $request){
        $optionData = Api::request("listSources",array("isWalkin" => null));
        $selectOptionList['listSources'] = !empty($optionData['result']['sources']) ? $optionData['result']['sources'] : array();
        return view('import.detail', [ 'selectOptionList' => $selectOptionList ]);    
    }
    
    public function getListImport(Request $request){
        $params = array(
            "sourceId" => $request->has('sourceId') ? intval($request->input('sourceId')) : null,
            "startDate" => $request->has('startDate') ? $request->input('startDate') : null,
            "endDate" => $request->has('endDate') ? $request->input('endDate') : null,
            'status' => $request->has('status') ? intval($request->input('status')) : null,
            'offset' => $request->has('start') ? intval($request->input('start')) : 0 ,
            'limit' => $request->has('length') ? intval($request->input('length')) : 0,
        );
        $returnData = Api::request('getImportLot', $params);
        
        $responseData = array();
        if(!empty($returnData['result'])){
            $total = !empty($returnData['result']['total']) ? intval($returnData['result']['total']) : count($returnData['result']['importLots']);
            $listData = $returnData['result']['importLots'];
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'recordsTotal' => $total,
                'recordsFiltered' => $total,
                'data' => $listData,
            );        
        }else{
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'error' => $returnData['error']['message'],
                'data' => [],
            );    
        }
        return response()->json($responseData);
        
    }
    
    public function convertData(Request $request){
        $path = $request->file('csvFile');
        $row = 0;
        list($usec, $sec) = explode(" ", microtime());
        $time = round(($usec * 1000) + $sec);
        $returnData = array (
            'jsonrpc' => '2.0',
            'id' => $time,
            'result' => [],
        );
        
        try{
            if(!$request->has('patternId')){
                throw new Exception('Not found patternId.');    
            }
            $patternId = intval($request->input('patternId'));
            if(($patternId <= 0)&&($patternId > 7)){
                throw new Exception('invaild input : patternId must be 1-7.');           
            }
            $patternType = config( 'import.' . $patternId . ".format");
            $isCE = config( 'import.' . $patternId . ".isCE");
            
            if (($handle = fopen($path->path(), "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 0, ',')) !== FALSE) {
                    $item = array();
                    $row++;
                    if( $row == 1 ){
                        continue;    
                    }    
                    
                    $num = count($data);
                    if($num < 26){
                        throw new Exception('invaild csv - row #' . ($row-1) . ' : Input in row must have more than 26 fields.');
                    }
                    // Data validate
                    $item['Name'] = trim($data[0] . " " . $data[1] . " " . $data[2]);
                    $item['Address'] = trim($data[3] . " " . $data[4] . " " . $data[5] . " " . $data[6] . " " . $data[7] . " " . $data[8] . " " . $data[9] . " " . $data[10]);
                    $item['SupDistrict'] = trim($data[11]);
                    $item['District'] = trim($data[12]);
                    $item['Province'] = trim($data[13]);
                    $item['Zipcode'] = trim($data[14]);
                    $item['Tel'] = trim($data[15]);
                    $item['Email'] = trim($data[16]);
                    $item['LicensePlate'] = trim($data[17] . " " . $data[18]);
                    $item['LicensePlateProvince'] = trim($data[19]);
                    $item['Brand'] = trim($data[20]);
                    $item['Model'] = trim($data[21]);
                    $item['CC'] = intval($data[22]);
                    $item['ChasisNo'] = trim($data[23]);
                    $item['VehicleType'] = ""; //$data[24]; Not use in this time.
                    $item['VehicleColor'] = trim($data[25]);
                    $item['VehicleYear'] = trim($data[26]);
                    if($patternType==null){
                        
                        if(isset($data[27]) && isset($data[28]) && isset($data[29]))
                            $item['ExpireDate'] = date("Y-m-d", strtotime($data[29] . "-" . $data[28] . "-" . $data[27]));
                        
                        if(isset($data[30]) && isset($data[31]) && isset($data[32]))
                            $item['TaxDate'] = date("Y-m-d", strtotime($data[32] . "-" . $data[31] . "-" . $data[30]));
                        
                        if(isset($data[33]) && isset($data[34]) && isset($data[35]))
                            $item['BirthDate'] = date("Y-m-d", strtotime($data[35] . "-" . $data[34] . "-" . $data[33]));
                        
                        if(isset($data[36]) && isset($data[37]) && isset($data[38]))
                            $item['CarDate'] = date("Y-m-d", strtotime($data[38] . "-" . $data[37] . "-" . $data[36]));
                        
                        if(isset($data[39]) && isset($data[40]) && isset($data[41]))
                            $item['ReceiveDate'] = date("Y-m-d", strtotime($data[41] . "-" . $data[40] . "-" . $data[39]));       
                    }else{
                        if(isset($data[27])){
                            $tmpdate = date_create_from_format($patternType, $data[27]);
                            $item['ExpireDate'] = !empty($tmpdate) ? ( $isCE ? $tmpdate->format('Y-m-d') : $tmpdate->modify('-543 year')->format('Y-m-d') ) : null;
                        }
                        
                        if(isset($data[30])){
                            $tmpdate = date_create_from_format($patternType, $data[30]);
                            $item['TaxDate'] = !empty($tmpdate) ? ( $isCE ? $tmpdate->format('Y-m-d') : $tmpdate->modify('-543 year')->format('Y-m-d') ) : null;
                        }
                        
                        if(isset($data[33])){
                            $tmpdate = date_create_from_format($patternType, $data[33]);
                            $item['BirthDate'] = !empty($tmpdate) ? ( $isCE ? $tmpdate->format('Y-m-d') : $tmpdate->modify('-543 year')->format('Y-m-d') ) : null;
                        }
                        
                        if(isset($data[36])){
                            $tmpdate = date_create_from_format($patternType, $data[36]);
                            $item['CarDate'] = !empty($tmpdate) ? ( $isCE ? $tmpdate->format('Y-m-d') : $tmpdate->modify('-543 year')->format('Y-m-d') ) : null;
                        }
                        
                        if(isset($data[39])){
                            $tmpdate = date_create_from_format($patternType, $data[39]);
                            $item['ReceiveDate'] = !empty($tmpdate) ? ( $isCE ? $tmpdate->format('Y-m-d') : $tmpdate->modify('-543 year')->format('Y-m-d') ) : null;
                        }
                    }
                    
                    if(empty($item['Tel'])){
                        throw new Exception('invaild csv - row #' . ($row-1) . ' : Tel is empty.');    
                    }
                    
                    $checkListStr = [
                        array( "name" => "Name", "max" => 255, "min" => 1 ),
                        array( "name" => "Address", "max" => 255 ),
                        array( "name" => "SupDistrict", "max" => 50 ),
                        array( "name" => "District", "max" => 50 ),
                        array( "name" => "Province", "max" => 50, "min" => 1 ),
                        array( "name" => "Zipcode", "max" => 5 ),
                        array( "name" => "Tel", "max" => 40 ),
                        array( "name" => "Email", "max" => 255 ),
                        array( "name" => "LicensePlate", "max" => 20 ),
                        array( "name" => "LicensePlateProvince", "max" => 50 ),
                        array( "name" => "Brand", "max" => 50, "min" => 1 ),
                        array( "name" => "Model", "max" => 50, "min" => 1 ),
                        array( "name" => "ChasisNo", "max" => 30 ),
                        array( "name" => "VehicleType", "max" => 255 ),
                        array( "name" => "VehicleColor", "max" => 30 ),   
                    ];
                    
                    foreach($checkListStr as $itemStr){
                        if( strlen($item[$itemStr["name"]]) > $itemStr["max"] ){
                            throw new Exception('invaild csv - row #' . ($row-1) . ' : ' . $itemStr["name"] . ' must less than or equal ' . $itemStr["max"] . ' charecter.');
                        }    
                    }

                    if( (!empty($item['CC'])) && (($item['CC'] > 10000) || ($item['CC'] < 1000 )) ){
                        throw new Exception('invaild csv - row #' . ($row-1) . ' : CC must be 1000-10000.');
                    }
                    
                    if((!empty($item['VehicleYear'])) && (intval($item['VehicleYear'])) > 9999) {
                        throw new Exception('invaild csv - row #' . ($row-1) . ' : VehicleYear must less than 10000.');
                    }
                    $returnData['result'][] = $item; 
                }
                fclose($handle);
            }else{
                throw new Exception('File cannot access.');     
            }    
        }catch(Exception $e){
            $returnData = [
                "id" => null,
                "error" => [
                    "code" => 442,
                    "message" => $e->getMessage(),
                ],
                "jsonrpc" => "2.0"
            ];    
        }
        
        return response()->json($returnData);
    }
    
    public function importLot(Request $request){
        $data = array(
            "sourceId" => $request->has('sourceId') ? intval($request->input('sourceId')) : 1,
            "fileName" => $request->has('fileName') ? $request->input('fileName') : null,
            "agentId" => intval(session('userId',0)),
        );
        $returnData = Api::request('addImportLot', $data);
        return response()->json($returnData);   
    }
    
    public function importCustomer(Request $request){
        foreach($request->input('customers') as $key => $value){
            foreach($value as $keyCustomers => $valueCustomers){
                $_REQUEST['customers'][$key][$keyCustomers] = ($valueCustomers != '' && $valueCustomers != ' ')?$valueCustomers:null;
            }
            //$_REQUEST['customers'][$key] = ($value != '' && $value != ' ')?$value:null;
        }
        $data = array(
            "lotId" => $request->has('lotId') ? intval($request->input('lotId')) : null,
            "customers" => $request->has('customers') ? $request->input('customers') : null,
            "agentId" => intval(session('userId',0)),
        );
        $returnData = Api::request('importCustomers', $data);
        return response()->json($returnData);   
    }
    
}
