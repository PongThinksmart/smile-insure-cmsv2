@extends('dashboard.dashboard', [
    'activeMenu' => 'import',
    'breadcrumb' => [
        array(
            'name' => 'Customer Management',
            'uri' => ''   
        ),
        array(
            'name' => 'Import Customer',
            'uri' => '' 
        )
    ]

] )
@section('css')

@endsection

@section('content')
    <div class="card mb-3">
        <div class="card-header ">
            <!-- searching layout -->
            <div class="row">
                <div class="form-group offset-lg-2 col-lg-4 col-md-6">
                    <label for="sourceId"><?php echo __('messages.typedata'); ?> <span style="color:red;">*</span></label>
                    <select class="custom-select" id="sourceId" name="sourceId">
                        <option value="0"><?php echo __('messages.all'); ?></option>
                        @foreach ($selectOptionList['listSources'] as $item)
                            <option value="{{$item['id']}}" >{{$item['name']}}</option>
                        @endforeach    
                    </select>
                </div>
                <div class="form-group col-lg-2 col-md-3">
                    <label for="startDate">นำเข้าวันที่</label>
                    <input data-provide="datepicker" class="form-control" name="startDate" id="startDate">        
                </div>
                <div class="form-group col-lg-2 col-md-3">
                    <label for="endDate">ถึงวันที่</label>
                    <input data-provide="datepicker" class="form-control" name="endDate" id="endDate">        
                </div>
            </div>
            <div class="row">
                <div class="form-group offset-lg-4 col-lg-4 col-md-12">
                    <button type="button" class="btn btn-primary" style="width:100%;" id="searchButton"><i class="fas fa-search"></i> <?php echo __('messages.search'); ?></button>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group offset-lg-10 col-lg-2 col-md-12">
                    <a href="<?php echo url('import'); ?>/new"><button type="button" class="btn btn-success" style="width:100%;" id="createButton"><?php echo __('messages.new'); ?></button></a>
                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              </table>
            </div>
        </div>
        <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
    </div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#dataTable').on( 'error.dt', function ( e, settings, techNote, message ) {
            $("#alertModalBody").text(message);
            $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
            $("#alertModal").modal('show');
            //console.log( 'An error has been reported by DataTables: ', message );
        })
        .DataTable({
            columns: [
                {   "title": "<?php echo __('messages.lotnoEN'); ?>", "data": "lotNo" },
                {   "title": "<?php echo __('messages.typedata'); ?>", "data": "sourceName" },
                {   "title": "<?php echo __('messages.filenameimport'); ?>", "data": "fileName" },
                {   "title": "<?php echo __('messages.amountdata'); ?>", "data": "successRows" },
                {
                    "title": "<?php echo __('messages.status'); ?>",
                    "data": "status",
                    "render": function (data, type, row) {
                        switch(data){
                            case 1 : return 'Waiting';    
                            case 2 : return 'Approved';    
                            case 3 : return 'Rejected';    
                        }     
                    }
                },
                
                {   "title": "<?php echo __('messages.madedate'); ?>", "data": "createDate"},
                {   "title": "<?php echo __('messages.editdate'); ?>", "data": "updateDate"},
            ],
            lengthMenu: [ 50, 100, 200, 500 ],
            ordering: false,
            searching: false,
            processing: true,
            serverSide: true,
            ajax:{
                url: "<?php echo url('import/getListImport'); ?>",
                type: 'GET',
                data:function (d) {
                    d.sourceId = $("#sourceId").val();
                    d.startDate = $("#startDate").val();
                    d.endDate = $("#endDate").val();
                    /*if($("#status").val() == 1){
                        d.status = 1;
                    }else if($("#status").val() == 2){
                        d.status = 0;
                    } */
                },
            },
            "dom": 'pltip',
        });
        
        $('#searchButton').click( function( e ){
            $('#dataTable').DataTable().draw(true);          
        });  
    });
</script>
@endsection

