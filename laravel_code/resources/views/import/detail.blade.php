@extends('dashboard.dashboard', [
    'activeMenu' => 'import',
    'breadcrumb' => [
        array(
            'name' => 'Customer Management',
            'uri' => ''   
        ),
        array(
            'name' => 'Import Customer',
            'uri' => 'import' 
        ),
        array(
            'name' => 'New',
            'uri' => '' 
        )
    ]

] )
@section('css')

@endsection

@section('content')
<form method="post" id="formUpdate" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-row mb-3">
        <div class="offset-lg-2 col-lg-4 col-md-12">
            <label for="sourceTypeId"><?php echo __('messages.typedata'); ?> <span style="color:red;">*</span></label>
            <select class="custom-select" id="sourceId" name="sourceId" required>
                @foreach ($selectOptionList['listSources'] as $item)
                <option value="{{$item['id']}}" >{{$item['name']}}</option>
                @endforeach    
            </select>
        </div>
        <div class="col-lg-4 col-md-12">
            <label for="patternId"><?php echo __('messages.style'); ?> <span style="color:red;">*</span></label>
            <select class="custom-select" id="patternId" name="patternId" required>
                @foreach (config('import') as $key => $item)
                    <option value="{{$key}}" >{{$item['name']}}</option>
                @endforeach    
            </select>
        </div>
        <div class="offset-lg-2 col-lg-8 col-md-12 mt-3">
            <label><?php echo __('messages.CSVFile'); ?> <span style="color:red;">*</span></label>
        </div>
        <div class="offset-lg-2 col-lg-8 col-md-12 custom-file">
            <input type="file" class="custom-file-input" id="csvFile" name="csvFile" required>
            <label class="custom-file-label" for="csvFile">Choose file</label>
        </div>
       
    </div>
    <div class="form-row mb-3">
        <div class="form-group offset-lg-2 col-lg-4 col-md-6" >
            <button id="saveButton" type="submit" class="btn btn-primary" style="width:100%;" ><?php echo __('messages.save'); ?></button>
        </div>
        <div class="form-group col-lg-4 col-md-6">
            <a href="<?php echo url('source'); ?>"><button type="button" class="btn btn-secondary" style="width:100%;" id="cancleButton"><?php echo __('messages.cancel'); ?></button></a>
        </div>
        
    </div>
</form>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('js/jquery.form.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        bsCustomFileInput.init();
        bsCustomFileInput.inputSelector = '.custom-file input[type="file"]';
    });

    $("#saveButton").click(function(){
        $("#formUpdate").ajaxForm({
            url: "<?php echo url('import/convertData'); ?>",
            type: "POST",
            beforeSubmit: function() {},
            success: function(resp) {
                //var jsonObj = jQuery.parseJSON(resp);
                jsonObj = resp;
                if(typeof jsonObj.error !== 'undefined'){
                    $("#alertModalBody").text("Error : " + jsonObj.error.message);    
                    $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                    $("#alertModal").modal('show');    
                }else{
                    var listData = jsonObj.result;
                    $.ajax({
                        url: "<?php echo url('import/importLot'); ?>",
                        type: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            sourceId: $("#sourceId").val(),
                            fileName: $("#csvFile")[0].files[0].name
                        },
                        beforeSend: function() {},
                        success: function(resp_lot) {
                            if(typeof resp_lot.error !== 'undefined'){
                                $("#alertModalBody").text("Error : " + jsonObj.error.message);    
                                $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                                $("#alertModal").modal('show');    
                            }else{
                                var lotId = resp_lot.result.lotId;
                                var deferreds = [];
                                $.each(listData, function(index, item){
                                    deferreds.push(
                                        $.ajax({
                                            url: "<?php echo url('import/importCustomer'); ?>",
                                            data: {
                                                _token: "{{ csrf_token() }}",
                                                lotId: lotId,
                                                customers: [{
                                                     custName : item.Name,
                                                     birthDate : item.BirthDate,
                                                     email : item.Email,
                                                     phoneNo : item.Tel,
                                                     address : item.Address,
                                                     distName : item.SupDistrict,
                                                     amphName : item.District,
                                                     provName : item.Province,
                                                     zipCode : item.Zipcode,
                                                     chasisNo : item.ChasisNo,
                                                     vehLicenseNo : item.LicensePlate,
                                                     vehProvName : item.LicensePlateProvince,
                                                     vehTypeName : item.VehicleType,
                                                     manuName : item.Brand,
                                                     modelName : item.Model,
                                                     modelYear : item.VehicleYear,
                                                     color : item.VehicleColor,
                                                     bodyType : null,
                                                     engineCapacity : item.CC,
                                                     buyDate : item.CarDate,
                                                     receiveDate : item.ReceiveDate,
                                                     taxDate : item.TaxDate,
                                                     lastExpireDate : item.ExpireDate,
                                                     lastPartnerName : null,   
                                                }],
                                            },
                                            type: 'POST'
                                        })
                                    );
                                });
                                $.when.apply($, deferreds).then(function(){
                                    $("#alertModalBody").text("Success : <?php echo __('messages.imported'); ?>");    
                                    $("#alertModalLabel").text("<?php echo __('messages.success'); ?>");
                                    $('#alertModal').on('hidden.bs.modal', function () {
                                        window.open("<?php echo url('import'); ?>","_self");    
                                    }).modal('show');    
                                }).fail(function(){
                                    $("#alertModalBody").text("Error : Importing a each data has error");    
                                    $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                                    $("#alertModal").modal('show');
                                });
                            }  
                        },
                        error:function(resp) {
                            $("#alertModalBody").text("Error : " + resp.statusText);
                            $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                            $("#alertModal").modal('show');
                        }
                    });
                }
            },
            error:function(resp) {
                $("#alertModalBody").text("Error : " + resp.statusText);
                $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                $("#alertModal").modal('show');
            },
                
        });   
    });
</script>
@endsection