<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Model\Api;

class ModelController extends Controller
{
    public function index(){
        // getManufacturer
        $returnData = Api::request("getManufacturer");
        $arrManuList = !empty($returnData['result']['manufacturers']) ? $returnData['result']['manufacturers'] : array();
        $selectOptionList['manuList'] = array();
        foreach($arrManuList as $model){
            if(!empty($model['status'])){
                array_push($selectOptionList['manuList'],$model);       
            }        
        }
        return view('model.index', [ 'selectOptionList' => $selectOptionList ]);    
    }
    
    public function new_detail(Request $request){
        $returnData = Api::request("getManufacturer");
        $arrManuList = !empty($returnData['result']['manufacturers']) ? $returnData['result']['manufacturers'] : array();
        $selectOptionList['manuList'] = array();
        foreach($arrManuList as $model){
            if(!empty($model['status'])){
                array_push($selectOptionList['manuList'],$model);       
            }        
        }
        
        return view('model.detail', [ 'data' => [ 'modelId' => -1 ] , 'selectOptionList' => $selectOptionList ]);    
    }
    
    public function detail($id, Request $request){
        $params = array(
            'modelId' => intval($id)
        );
        $modelData = Api::request('getModelById', $params);
        if($modelData['result']==null){
            $sidebarAndBreadcrumb = [
                'activeMenu' => 'brand',
                'breadcrumb' => [
                    array(
                        'name' => 'Cars Management',
                        'uri' => ''   
                    ),
                    array(
                        'name' => 'Model List',
                        'uri' => 'model' 
                    ),
                    array(
                        'name' => ( $id >= 0 ) ? 'Edit a Model' : 'Create a new Model',
                        'uri' => '' 
                    )
                ]
            ];
            return view('notfound', [ 'sidebarAndBreadcrumb' => $sidebarAndBreadcrumb, 'backlink' => [ 'url' => url('brand'), 'text' => 'Back to Brand List', ], ] );    
        }
        $returnData = Api::request("getManufacturer");
        $arrManuList = !empty($returnData['result']['manufacturers']) ? $returnData['result']['manufacturers'] : array();
        $selectOptionList['manuList'] = array();
        foreach($arrManuList as $model){
            if(!empty($model['status'])){
                array_push($selectOptionList['manuList'],$model);       
            }        
        }
        return view('model.detail', [ 'data' => $modelData['result'], 'selectOptionList' => $selectOptionList ]);                            
    }
    
    public function mapping(){
        $returnData = Api::request("listWaitingImportLots");
        $selectOptionList['lotId'] = !empty($returnData['result']['importLots']) ? $returnData['result']['importLots'] : array();
        
        $returnData = Api::request("getManufacturer");
        $arrManuList = !empty($returnData['result']['manufacturers']) ? $returnData['result']['manufacturers'] : array();
        $selectOptionList['manuList'] = array();
        foreach($arrManuList as $model){
            if(!empty($model['status'])){
                array_push($selectOptionList['manuList'],$model);       
            }        
        }
        
        return view('model.mapping', [ 'selectOptionList' => $selectOptionList ]);    
    }
    
    public function getListModel(Request $request){
        $data = array(
            'keyword' => $request->has('keyword') ? $request->input('keyword') : null,
            'status' => $request->has('status') ? intval($request->input('status')) : null,
            'manuId' => $request->has('manuId') ? intval($request->input('manuId')) : 0,
            'offset' => $request->has('start') ? intval($request->input('start')) : 0 ,
            'limit' => $request->has('length') ? intval($request->input('length')) : 0,
        );
        $returnData = Api::request('searchModel', $data);
        
        $data = array(
            'manuId' => $request->has('manuId') ? intval($request->input('manuId')) : 0,
            'keyword' => $request->has('keyword') ? $request->input('keyword') : null,
            'status' => $request->has('status') ? intval($request->input('status')) : null,
        );
        $countdata = Api::request("countModel",$data);
        
        $responseData = array();
        if(!empty($returnData['result'])){
            $total = !empty($countdata['result']['total']) ? intval($countdata['result']['total']) : count($returnData['result']['models']);
            $listData = $returnData['result']['models'];
            $i = $request->has('start') ? intval($request->input('start')) : 0;
            if(!empty($listData)){
                foreach($listData as $key => $item){
                    $listData[$key]['order'] = $i+1;
                    $i++;       
                }
            }else{
                $listData = [];   
            }
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'recordsTotal' => $total,
                'recordsFiltered' => $total,
                'data' => $listData,
            );        
        }else{
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'error' => $returnData['error']['message'],
                'data' => [],
            );    
        }
        return response()->json($responseData);    
    }
    
    public function getAllModel(Request $request){
        $data = array(
            'manuId' => $request->has('manuId') ? intval($request->input('manuId')) : 0,
        );
        $responseData = Api::request('getModel', $data);
        return response()->json($responseData);    
    }
    
    public function getMismatch(Request $request){
        $data = array(
            'lotId' => $request->has('lotId') ? intval($request->input('lotId')) : null,
            'manuName' => $request->has('manuName') ? $request->input('manuName') : null, 
            'modelName' => $request->has('modelName') ? $request->input('modelName') : null,
            'offset' => $request->has('start') ? intval($request->input('start')) : 0 ,
            'limit' => $request->has('length') ? intval($request->input('length')) : 0,
        );
        $returnData = Api::request('getMismatchModelsByLotId', $data);
        $responseData = array();
        if(!empty($returnData['result'])){
            $total = !empty($countdata['result']['total']) ? intval($countdata['result']['total']) : count($returnData['result']['mismatches']);
            $listData = !empty($returnData['result']['mismatches']) ? $returnData['result']['mismatches'] : array();
            $i = $request->has('start') ? intval($request->input('start')) : 0;
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'recordsTotal' => $total,
                'recordsFiltered' => $total,
                'data' => $listData,
            );        
        }else{
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'error' => $returnData['error']['message'],
                'data' => [],
            );    
        }
        return response()->json($responseData);    
    }
    
    public function create(Request $request){  
        $data = array(
            "manuId" => $request->has('manuId') ? intval($request->input('manuId')) : 1,
            "modelName" => $request->has('modelName') ? $request->input('modelName') : "",
            "status" => $request->has('status') ? intval($request->input('status')) : 1,
            "agentId" => intval(session('userId',0)),
        );
        $returnData = Api::request('addModel', $data);
        return response()->json($returnData);
    }
    
    public function update($id, Request $request){  
        $data = array(
            "manuId" => $request->has('manuId') ? intval($request->input('manuId')) : 1,
            "modelId" => intval($id),
            "modelName" => $request->has('modelName') ? $request->input('modelName') : "",
            "status" => $request->has('status') ? intval($request->input('status')) : 1,
            "agentId" => intval(session('userId',0)),
        );
        $returnData = Api::request('editModel', $data);
        return response()->json($returnData);
    }
    
    public function replace($id, Request $request){
        $mismatches = array();
        if($request->has('checkList')){
            $arrCheckList = $request->input('checkList');
            foreach($arrCheckList as $manuName => $modelList ){
                if(!empty($modelList)){
                    foreach($modelList as $modelName){
                        $mismatches[] = array('manuName' => $manuName, 'modelName' => $modelName);    
                    }     
                }
                   
            }
        }  
        $data = array(
            "lotId" => intval($id),
            "manuId" => $request->has('manuId') ? intval($request->input('manuId')) : 0,
            "modelId" => $request->has('manuId') ? intval($request->input('modelId')) : 0,
            "agentId" => intval(session('userId',0)),
            "mismatches" => $mismatches,
        );
        $returnData = Api::request('replaceMismatchModels', $data);
        return response()->json($returnData);
    }
}
