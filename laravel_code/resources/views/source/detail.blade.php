@extends('dashboard.dashboard', [
    'activeMenu' => 'source',
    'breadcrumb' => [
        array(
            'name' => 'Source Management',
            'uri' => ''   
        ),
        array(
            'name' => 'Source List',
            'uri' => 'source' 
        ),
        array(
            'name' => ( $data['sourceId'] >= 0 ) ? 'Edit a Source' : 'Create a new Source',
            'uri' => '' 
        )
    ]

] )
@section('css')

@endsection

@section('content')
<form method="{{ ( $data['sourceId'] >= 0 ) ? 'put' : 'post' }}" id="formUpdate">
    {{ csrf_field() }}
    <div class="form-row mb-3">
        <div class="offset-lg-2 col-lg-4 col-md-6">
            <label for="sourceTypeId"><?php echo __('messages.typedata'); ?> <span style="color:red;">*</span></label>
            <select class="custom-select" id="sourceTypeId" name="sourceTypeId" required>
            @foreach ($selectOptionList['listSourceType'] as $item)
                <option value="{{$item['id']}}" >{{$item['name']}}</option>
            @endforeach    
            </select>
        </div>
        <div class="col-lg-4 col-md-6">
            <label for="sourceName"><?php echo __('messages.source'); ?> <span style="color:red;">*</span></label>
            <input type="text" class="form-control" id="sourceName" name="sourceName" required>
        </div>
        <div class="offset-lg-2 col-lg-6 col-md-9">
            <label for="sourceAbbr"><?php echo __('messages.abbr'); ?> <span style="color:red;">*</span></label>
            <input type="text" class="form-control" id="sourceAbbr" name="sourceAbbr" required>
        </div>
        <div class="col-lg-2 col-md-3">
            <label for="status"><?php echo __('messages.status'); ?> <span style="color:red;">*</span></label>
            <select class="custom-select" id="status" name="status" required>
                <option value="1">Active</option>
                <option value="0">Inactive</option>
            </select>
        </div>
    </div>
    @if ($data['sourceId'] >= 0)
    <div class="card mb-3 offset-lg-2 col-lg-8">
        <div class="card-body">
            <dl class="row">
                <dt class="col-md-3"><?php echo __('messages.madedate'); ?></dt>
                <dd class="col-md-9">{{ empty($data['createDate']) ? "" : $data['createDate'] }}</dd>

                <dt class="col-md-3"><?php echo __('messages.editdate'); ?></dt>
                <dd class="col-md-9">{{ empty($data['updateDate']) ? "" : $data['updateDate'] }}</dd>

                <dt class="col-md-3"><?php echo __('messages.editauthor'); ?></dt>
                <dd class="col-md-9">{{ empty($data['updateAgentName']) ? "" : $data['updateAgentName'] }}</dd>
            </dl>
        </div>
    </div>
    @endif
    <div class="form-row mb-3">
        <div class="form-group offset-lg-2 col-lg-4 col-md-6" >
            <button id="saveButton" type="submit" class="btn btn-primary" style="width:100%;" ><?php echo __('messages.save'); ?></button>
        </div>
        <div class="form-group col-lg-4 col-md-6">
            <a href="<?php echo url('source'); ?>"><button type="button" class="btn btn-secondary" style="width:100%;" id="cancleButton"><?php echo __('messages.cancel'); ?></button></a>
        </div>
        
    </div>
</form>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('js/jquery.form.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#status").val( {{ isset($data['status']) ? $data['status'] : 1 }} );
        $("#sourceTypeId").val( {{ isset($data['sourceTypeId']) ? $data['sourceTypeId'] : 1 }} );
        $("#sourceName").val( "{{ isset($data['sourceName']) ? $data['sourceName'] : "" }}" );
        $("#sourceAbbr").val( "{{ isset($data['sourceAbbr']) ? $data['sourceAbbr'] : "" }}" );
    });
    
    $("#saveButton").click(function(){
        $("#formUpdate").ajaxForm({
            url: "<?php echo ( $data['sourceId'] >= 0 ) ? url('source/' . $data['sourceId']) : url('source'); ?>",
            type: "{{ ( $data['sourceId'] >= 0 ) ? 'PUT' : 'POST' }}",
            beforeSubmit: function() {
                    
            },
            success: function(resp) {
                //var jsonObj = jQuery.parseJSON(resp);
                jsonObj = resp;
                if(typeof jsonObj.error !== 'undefined'){
                    $("#alertModalBody").text("Error : " + jsonObj.error.message);    
                    $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                    $("#alertModal").modal('show');    
                }else{
                    $("#alertModalBody").text("Success : <?php echo ( $data['sourceId'] >= 0 ) ? __('messages.saved') : __('messages.created'); ?>");    
                    $("#alertModalLabel").text("<?php echo __('messages.success'); ?>");
                    $("#alertModal")
                    $('#alertModal').on('hidden.bs.modal', function () {
                        window.open("<?php echo url('source'); ?>","_self");    
                    }).modal('show');
                }
            },
            error:function(resp) {
                $("#alertModalBody").text("Network Error : " + resp.statusText);
                $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                $("#alertModal").modal('show');
            },
                
        });   
    });
</script>
@endsection