@extends('dashboard.dashboard', [
    'activeMenu' => 'messenger/post',
    'breadcrumb' => [
        array(
            'name' => 'Messenger Management',
            'uri' => ''   
        ),
        array(
            'name' => 'Post List',
            'uri' => '' 
        ),
    ]

] )
@section('css')

@endsection

@section('content')
@if( count($data) <= 0 )
<div class="page-wrap d-flex flex-row align-items-center">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12 text-center">
                <span class="display-1 d-block">ยังไม่มีข้อมูล ณ ขณะนี้</span>
                <!--<div class="mb-4 lead"><?php echo __('messages.notfounddata'); ?></div>-->
            </div>
        </div>
    </div>
</div>
@else
<div class="row">
    @foreach($data as $item)
    <div class="col-lg-4 col-md-6 col-sm-12 mb-3">
        <div class="card">
            <form id="form_{{ $item['messengerId'] }}" method="POST">
            {{ csrf_field() }}
            <div class="card-header form-inline">{{ __('messages.notificationDate') }} : <div class="datared pl-1"><?php echo empty($item['notificationDate']) ? '-' : $item['notificationDate']; ?></div></div>
            <div class="card-body">
                <input type="hidden" name="messengerId" value="{{ $item['messengerId'] }}">
                <input type="hidden" name="messengerStatusFinish" id="messengerStatusFinish_{{ $item['messengerId'] }}" value="">
                <div class="row">
                    <div class="form-group col-6">
                        <textarea class="form-control description" id="description_{{ $item['messengerId'] }}" placeholder="<?php echo __('messages.customerData'); ?>" rows="7" readonly>{{ $item['description'] }}</textarea>
                        <div style="padding-top: 10px;">
                            <span>{{ __('messages.messengerTypeId') }} : <span class="datared pl-1"><?php echo empty($item['messengerTypeName']) ? '-' : $item['messengerTypeName']; ?></span></span>
                            <button type="button" class="btn btn-primary copyDescription" class="btn btn-primary" style="float:right;margin-top: -7px;">{{ __('messages.copy') }}</button>
                        </div>
                    </div>
                    <div class="form-group col-6">
                        <input type="text" name="sentReqEms" class="form-control" placeholder="{{ __('messages.inputEMS') }}" value="{{ $item['sentReqEms'] }}" >
                        <div class="pt-2" style="width: 70%;float: right;display: flex;">
                            <input data-provide="datepicker" type="text" style="width: 71%;" class="form-control" id="sentReqDate_{{ $item['messengerId'] }}" name="sentReqDate" placeholder="{{ __('messages.inputDateEMS') }}" value="{{ $item['sentReqDate'] }}">
                            <div class="pl-2 form-inline">
                                <input class="form-check-input" type="checkbox" value="1" id="isToday_{{ $item['messengerId'] }}" onclick=isToday({{ $item['messengerId'] }})>
                                <label class="form-check-label" for="isToday_{{ $item['messengerId'] }}">วันนี้</label>
                            </div>
                        </div>
                        <div class="row pt-2" style="display: flex;width:100%;font-size: 13px;">
                            @if ( !empty($item['sentReceiptTag']) )<div class="col-6">{{ __('messages.sentReceipt') }}</div>@endif   
                            @if ( !empty($item['sentReqTag']) )<div class="col-6">{{ __('messages.sentReq') }}</div>@endif   
                            @if ( !empty($item['sentActTag']) )<div class="col-6">{{ __('messages.sentAct') }}</div>@endif   
                            @if ( !empty($item['sentFormCutCardTag']) )<div class="col-6">{{ __('messages.sentFormCutCard') }}</div>@endif   
                            @if ( !empty($item['sentDepartmentTag']) )<div class="col-6">{{ __('messages.sentDepartment') }}</div>@endif   
                            @if ( !empty($item['sentEndorseTag']) )<div class="col-6">{{ __('messages.sentEndorse') }}</div>@endif   
                            @if ( !empty($item['sentRenewTag']) )<div class="col-6">{{ __('messages.sentRenew') }}</div>@endif    
                        </div>
                        <div class="row pt-2" style="display: flex;width:100%;font-size: 13px;">
                            <div class="col-6">
                                <div class="form-check-label" >{{ __('messages.campaignSpecialInsure') }}</div>
                                <textarea class="form-control" rows="6" placeholder="" style="width: 125%;" readonly>{{ $item['campaignDetail'] }}</textarea>
                            </div>    
                            <div class="col-6">
                                <div class="form-check-label" >{{ __('messages.sentOther') }}</div>
                                <textarea class="form-control" rows="6" placeholder="{{ __('messages.description') }}" style="width: 125%;" readonly><?php echo empty($item['sentOtherItemList'][0]['sentOtherDetail']) ? "" : $item['sentOtherItemList'][0]['sentOtherDetail']; ?></textarea>
                            </div>  
                        </div>
                    </div>
                    <div class="form-group col-12">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="1" name="waitingEmsTag" <?php echo $item['waitingEmsTag'] ? "checked" : ""; ?>>
                            <label class="form-check-label" for="defaultCheck1">{{ __('messages.waitEMS') }}</label>
                            
                            <input type="button" class="btn btn-primary" style="float:right;" value="{{ __('messages.finish') }}" onclick="clickForm({{ $item['messengerId'] }}, 1)">
                            <input type="button" class="btn btn-secondary mr-2" style="float:right;" value="{{ __('messages.cancel') }}" onclick="clickForm({{ $item['messengerId'] }}, -1)">
                        </div>
                    </div>
                </div>    
            </div>
            </form>
        </div>
    </div>
    
    @endforeach
</div>
@endif
@endsection

@section('script')
<script type="text/javascript">

    var objDate = new Date();
    var dd = objDate.getDate();
    var mm = objDate.getMonth()+1; //As January is 0.
    var yyyy = objDate.getFullYear();
    var formId;

    if(dd<10) dd='0'+dd;
    if(mm<10) mm='0'+mm;
    
    var today = (yyyy + "-" + mm + "-" + dd);
    
    $('.copyDescription').on('click', function(event) {
        var textToPaste = ($(this).parent().parent().find(".description").first().select());
        document.execCommand('copy');
    });

    function clickForm(id, messengerStatusFinish){
        formId = id;
        
        var form = document.getElementById('form_' + formId);
        form.action = "{{ url('messenger/post') }}";
        form.method = "POST";
        $("#messengerStatusFinish_" + formId).val(messengerStatusFinish);
        
        $("#alertModalBody").text("Confirm Action?");
        $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
        $("#modelSuccess").show();
        $('#alertModal').on('hidden.bs.modal', function () {
            $("#modelSuccess").hide();    
        }).modal('show');
        
        //
    }
    
    $("#modelSuccess").click(function() {
        var form = document.getElementById('form_' + formId);
        form.submit();       
    });
    
    function isToday(id){
        
        if($("#isToday_" + id).is(':checked')){
            $("#sentReqDate_" + id).val(today).prop("readonly" , true);
        }else{
            $("#sentReqDate_" + id).prop("readonly" , false);
        }
    }

$(document).ready(function() {
    
    
}); 
</script>
@endsection

