@extends('dashboard.dashboard', [
    'activeMenu' => 'lead',
    'breadcrumb' => [
        array(
            'name' => 'Customer Management',
            'uri' => ''   
        ),
        array(
            'name' => 'Lead From Website',
            'uri' => '' 
        )
    ]

] )
@section('css')

@endsection

@section('content')
<form method="post" id="formUpdate" enctype="multipart/form-data" action="<?php echo url('lead/exportExcel'); ?>">
    {{ csrf_field() }}
    <div class="card mb-3">
        <div class="card-header ">
            <!-- searching layout -->
            <div class="row">
                <div class="form-group offset-lg-2 col-lg-4 col-md-6">
                    <label for="startDate">นำเข้าวันที่ <span style="color:red;">*</span></label>
                    <input data-provide="datepicker" class="form-control" name="startDate" id="startDate" required>        
                </div>
                <div class="form-group col-lg-4 col-md-6">
                    <label for="endDate">ถึงวันที่ <span style="color:red;">*</span></label>
                    <input data-provide="datepicker" class="form-control" name="endDate" id="endDate" required>        
                </div>
            </div>
            <div class="row">
                <div class="form-group offset-lg-4 col-lg-4 col-md-12">
                    <button type="submit" class="btn btn-primary" style="width:100%;" id="exportButton"><?php echo __('messages.ExportExcel'); ?></button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        @if($isEmpty)
            $("#alertModalBody").text("ไม่มีข้อมูลในช่วงเวลานี้");    
            $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
            $("#alertModal").modal('show');
        @endif
    });
</script>
@endsection

