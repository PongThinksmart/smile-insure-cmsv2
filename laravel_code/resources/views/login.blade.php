<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin - Login</title>

  <!-- Custom fonts for this template-->
  <link rel="stylesheet" href="{{ asset('fontawesome-free/css/all.min.css') }}"/>

  <!-- Custom styles for this template-->
  <link rel="stylesheet" href="{{ asset('css/sb-admin.css') }}"/>

</head>

<body>
    <div class="container">
        <div class="card card-login mx-auto mt-5">
            <div class="card-header">Smile-CMS Login</div>
            <div class="card-body">
                <form method="post" id="loginForm">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="username" id="inputUsername" name="username" class="form-control" placeholder="Username" required="required" autofocus="autofocus">
                            <label for="inputUsername">Username</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-label-group">
                            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required="required">
                            <label for="inputPassword">Password</label>
                        </div>
                    </div>
                    <button id="buttonLogin" type="submit" class="btn btn-primary btn-block">Login</button>
                </form>
            </div>
        </div>
        @include('footer')
    </div>
    @include('alert')

	<!-- Bootstrap core JavaScript-->
	<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script> 
	<script type="text/javascript" src="{{ asset('js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.form.js') }}"></script>

    <!-- Core plugin JavaScript-->
    <script type="text/javascript" src="{{ asset('js/jquery.easing.min.js') }}"></script>
    
    <script type="text/javascript">
        $("#buttonLogin").click(function(){
            $("#loginForm").ajaxForm({
                url: "<?php echo url('login'); ?>",
                type: "POST",
                beforeSubmit: function() {
                    
                },
                success: function(resp) {
                    //var jsonObj = jQuery.parseJSON(resp);
                    jsonObj = resp;
                    if(typeof jsonObj.error !== 'undefined'){
                        if(jsonObj.error.code == 400){
                            $("#alertModalBody").text("Login : <?php echo __('messages.LoginDataNotFound'); ?>");    
                        }else{
                            $("#alertModalBody").text("Login : " + jsonObj.error.message);    
                        }
                        $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                        $("#alertModal").modal('show');    
                    }else{
                        window.open("login","_self");
                    }
                },
                error:function(resp) {
                    $("#alertModalBody").text("Network : " + resp.statusText);
                    $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                    $("#alertModal").modal('show');
                },
                
            });   
        });        
    </script>

</body>

</html>
