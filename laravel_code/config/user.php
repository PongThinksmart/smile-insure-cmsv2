<?php

return [
    1 => array(
        'levelId' => 1,
        'levelName' => 'Admin',
        'urldefault' => 'customer',
    ),
    2 => array(
        'levelId' => 2,
        'levelName' => 'Supervisor',
        'urldefault' => 'customer',
    ),
    3 => array(
        'levelId' => 3,
        'levelName' => 'Data Entry',
        'urldefault' => 'import',
    ),
    4 => array(
        'levelId' => 4,
        'levelName' => 'Scout',
        'urldefault' => 'customer',
    ),
    5 => array(
        'levelId' => 5,
        'levelName' => 'Sale',
        'urldefault' => 'customer',
    ),
    6 => array(
        'levelId' => 6,
        'levelName' => 'Sale Support',
        'urldefault' => 'customer',
    ),
    7 => array(
        'levelId' => 7,
        'levelName' => 'Messenger',
        'urldefault' => 'customer',
    ),
    99 => array(
        'levelId' => 99,
        'levelName' => 'Developer',
        'urldefault' => 'customer',
    ),
];
