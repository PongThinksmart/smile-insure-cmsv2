<?php

namespace App\Model;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Api extends Model
{
    protected static function request($method, $data = null){
        $client = new Client([
            'base_uri' => config('setting.' . env('APP_ENV') . '.CMSUri'),
        ]);
        
        list($usec, $sec) = explode(" ", microtime());
        $time = round(($usec * 1000) + $sec);
        $header = config('setting.' . env('APP_ENV') . '.CMSUser') . ':' . config('setting.' . env('APP_ENV') . '.CMSPass');
        
        /*$headers = array (
            "Authorization: Basic " . base64_encode ( $header ),
        );
        $fields = array (
                'jsonrpc' => '2.0',
                'id' => $time,
                'method' => $method,
                'params' => $data 
        );
        
        $fields_string = json_encode ( $fields );
        //$memberId = intval(999); 
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_URL, config('setting.' . env('APP_ENV') . '.CMSUri') . config('setting.' . env('APP_ENV') . '.APIPath') );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ( $ch, CURLOPT_POST, 1 );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields_string );
        curl_setopt ( $ch, CURLOPT_TIMEOUT, 30 );
        curl_setopt ( $ch, CURLOPT_CONNECTTIMEOUT, 30 );
        curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt ( $ch, CURLOPT_FAILONERROR, true);   
        Log::info('Request: '.$fields_string);
        $result = curl_exec ( $ch ); 
        if(curl_errno($ch)){
            Log::error('Error Request: '.curl_error($ch));
            $obj = [
                "id" => null,
                "error" => [
                    "code" => curl_errno($ch),
                    "message" => curl_error($ch),
                ],
                "jsonrpc" => "2.0"
            ];
            
        }else{
            $obj = json_decode ( $result, true );
            Log::info('Response : ' . $result);
        }
        curl_close ($ch);
        return $obj;
        */
        try {
            Log::info('Request: '.json_encode([
                'jsonrpc' => '2.0',
                'id' => $time,
                'method' => $method,
                'params' => $data
            ]));
            $response = $client->request('POST', config('setting.' . env('APP_ENV') . '.APIPath'), [
                'headers' => [
                    "Authorization" => "Basic " . base64_encode ( $header ),
                    "accept" => "application/json",
                ],
                'body' => json_encode([
                    'jsonrpc' => '2.0',
                    'id' => $time,
                    'method' => $method,
                    'params' => $data
                ]),
                'verify' => false,
                'timeout' => 5,
            ]);
            
            $body = $response->getBody();
            Log::info('Response : ' . $body);
            $body = json_decode((string)$body, true);
        }
        catch(\GuzzleHttp\Exception\RequestException $e){
            if($e->hasResponse()){
                $body = [
                    "id" => null,
                    "error" => [
                        "code" => $e->getResponse()->getStatusCode(),
                        "message" => $e->getMessage(),
                    ],
                    "jsonrpc" => "2.0"
                ]; 
            }else{
                $errorData = $e->getHandlerContext();
                $body = [
                    "id" => null,
                    "error" => [
                        "code" => $errorData['errno'],
                        "message" => $errorData['error'],
                    ],
                    "jsonrpc" => "2.0"
                ];     
            }
            Log::error('Error Request: '. json_encode($body) );
        }
        return $body;
    }
}
