@extends('dashboard.dashboard', [
    'activeMenu' => 'user',
    'breadcrumb' => [
        array(
            'name' => 'User Management',
            'uri' => ''   
        ),
        array(
            'name' => 'User List',
            'uri' => 'user' 
        ),
        array(
            'name' => ( $data['agentId'] >= 0 ) ? 'Edit a User' : 'Create a new User',
            'uri' => '' 
        )
    ]

] )
@section('css')

@endsection

@section('content')
<form method="{{ ( $data['agentId'] >= 0 ) ? 'put' : 'post' }}" id="formUpdate" class="row">
    {{ csrf_field() }}
    <div class="col-lg-6 col-md-12">
        <div class="form-group col-lg-12">
            <label for="agentLevelId"><?php echo __('messages.usertype'); ?> <span style="color:red;">*</span></label>
            <select class="custom-select" id="agentLevelId" name="agentLevelId" required >
                @foreach ($selectOptionList['agentLevels'] as $item)
                    <option value="{{$item['id']}}" >{{$item['name']}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-lg-12">
            <label for="agentName"><?php echo __('messages.name'); ?> <span style="color:red;">*</span></label>
            <input type="text" class="form-control" name="agentName" id="agentName" required>
        </div>
        <div class="form-group col-lg-12">
            <label for="nickname"><?php echo __('messages.nickname'); ?> <span style="color:red;">*</span></label>
            <input type="text" class="form-control" name="nickname" id="nickname" required>
        </div>
        <div class="form-group col-lg-12">
            <label for="username"><?php echo __('messages.username'); ?> <span style="color:red;">*</span></label>
            <input type="text" class="form-control" name="username" id="username" required>
        </div>
        <div class="form-group col-lg-12">
            <label for="password"><?php echo __('messages.password'); ?> @if ($data['agentId'] < 0) <span style="color:red;">*</span> @endif</label>
            <input type="password" class="form-control" name="password" id="password" @if ($data['agentId'] < 0) required @endif>
        </div>
        <div class="form-group col-lg-12">
            <label for="repassword"><?php echo __('messages.repassword'); ?> @if ($data['agentId'] < 0) <span style="color:red;">*</span> @endif</label>
            <input type="password" class="form-control" name="repassword" id="repassword" @if ($data['agentId'] < 0) required @endif>
        </div>
        <div class="form-group col-lg-12">
            <label for="phoneNo"><?php echo __('messages.phoneno'); ?></label>
            <input type="text" class="form-control" name="phoneNo" id="phoneNo">                             
        </div>
    </div>
    <div class="col-lg-6 col-md-12">
        <div class="form-group col-lg-12">
            <label for="employTypeId"><?php echo __('messages.employtype'); ?> <span style="color:red;">*</span></label>
            <select class="custom-select" id="employTypeId" name="employTypeId" required >
                @foreach ($selectOptionList['employmentTypes'] as $item)
                    <option value="{{$item['id']}}" >{{$item['name']}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-lg-12">
            <label for="startDate"><?php echo __('messages.startdate'); ?></label>
            <input data-provide="datepicker" class="form-control" name="startDate" id="startDate" >
        </div>
        <div class="form-group col-lg-12">
            <label for="employDate"><?php echo __('messages.employdate'); ?></label>
            <input data-provide="datepicker" class="form-control" name="employDate" id="employDate">
        </div>
        <div class="form-group col-lg-12">
            <label for="endDate"><?php echo __('messages.enddate'); ?></label>
            <input data-provide="datepicker" class="form-control" name="endDate" id="endDate">
        </div>
    </div>
    @if (!$data['status'])
    <div class="form-row col-lg-12 col-md-12 mt-3">
        <div class="form-group offset-lg-4 col-lg-4 col-md-12">
            <a href="<?php echo url('user'); ?>"><button type="button" class="btn btn-secondary" style="width:100%;" id="cancleButton"><?php echo __('messages.cancel'); ?></button></a>
        </div>
    </div>
    @elseif ($data['agentId'] >= 0)
    <div class="form-row col-lg-12 col-md-12 mt-3">
        <div class="form-group offset-lg-3 col-lg-2 col-md-12" >
            <button id="saveButton" type="submit" class="btn btn-primary" style="width:100%;" ><?php echo __('messages.save'); ?></button>
        </div>
        <div class="form-group col-lg-2 col-md-6">
            <button id="inactiveButton" type="button" class="btn btn-danger" style="width:100%;" id="cancleButton"><?php echo __('messages.inactiveuser'); ?></button>
        </div>
        <div class="form-group col-lg-2 col-md-6">
            <a href="<?php echo url('user'); ?>"><button type="button" class="btn btn-secondary" style="width:100%;" id="cancleButton"><?php echo __('messages.cancel'); ?></button></a>
        </div>
    </div>
    @else
    <div class="form-row col-lg-12 col-md-12 mt-3">
        <div class="form-group offset-lg-2 col-lg-4 col-md-6" >
            <button id="saveButton" type="submit" class="btn btn-primary" style="width:100%;" ><?php echo __('messages.save'); ?></button>
        </div>
        <div class="form-group col-lg-4 col-md-6">
            <a href="<?php echo url('user'); ?>"><button type="button" class="btn btn-secondary" style="width:100%;" id="cancleButton"><?php echo __('messages.cancel'); ?></button></a>
        </div>
    </div>
    @endif
</form>
@endsection

@section('script')
<script type="text/javascript" src="{{ asset('js/jquery.form.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        var status = {{ $data['status'] }};
        $("#agentLevelId").val( "{{ isset($data['agentLevelId']) ? $data['agentLevelId'] : 1 }}" );
        $("#employTypeId").val( "{{ isset($data['employTypeId']) ? $data['employTypeId'] : 1 }}" );
        $("#username").val( "{{ isset($data['username']) ? $data['username'] : "" }}" );
        $("#agentName").val( "{{ isset($data['agentName']) ? $data['agentName'] : "" }}" );
        $("#nickname").val( "{{ isset($data['nickname']) ? $data['nickname'] : "" }}" );
        $("#phoneNo").val( "{{ isset($data['phoneNo']) ? $data['phoneNo'] : "" }}" );
        $("#startDate").val( "{{ isset($data['startDate']) ? $data['startDate'] : "" }}" );
        $("#endDate").val( "{{ isset($data['endDate']) ? $data['endDate'] : "" }}" );
        $("#employDate").val( "{{ isset($data['employDate']) ? $data['employDate'] : "" }}" );    
        
        if (!status){
            $("#formUpdate :input").prop("disabled", true);
            $("#cancleButton").prop("disabled", false);    
        }
    });
    
    $("#inactiveButton").click(function(){
        $("#alertModalBody").text("ยืนยันว่าจะปิดการใช้งานปัญชีนี้\nหากปิดแล้วจะไม่สามารถกลับมาใช้บัญชีนี้ได้อีก\nหากเป็น Sale งานเดิมของบัญชีนี้ จะกระจายไปยังบัญชีของ Sale คนอื่นๆ");    
        $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
        $("#modelSuccess").show();
        $('#alertModal').on('hidden.bs.modal', function () {
            $("#modelSuccess").hide();    
        }).modal('show');    
    });
    
    $("#modelSuccess").click(function() {
        $.ajax({
            url: "<?php echo url('user/inactiveUser'); ?>/{{ $data['agentId'] }}",
            method: "GET",
            success: function(resp){
                jsonObj = resp;
                if(typeof jsonObj.error !== 'undefined'){
                    $("#alertModalBody").text("Error : " + jsonObj.error.message);    
                    $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                    $("#alertModal").modal('show');    
                }else {
                    window.open("<?php echo url('user'); ?>","_self");      
                }           
            },
            error:function(resp) {
                $("#alertModalBody").text("Network Error : " + resp.statusText);
                $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                $("#alertModal").modal('show');
            },
        });    
    });
    
    $("#saveButton").click(function(){
        $("#formUpdate").ajaxForm({
            url: "<?php echo ( $data['agentId'] >= 0 ) ? url('user/' . $data['agentId']) : url('user'); ?>",
            type: "{{ ( $data['agentId'] >= 0 ) ? 'PUT' : 'POST' }}",
            beforeSubmit: function() {
                if( $("#password").val() != $("#repassword").val() ){
                    $("#alertModalBody").text("Error : <?php echo __('messages.passwordmismatch'); ?>");    
                    $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                    $("#alertModal").modal('show');
                    return false;
                }        
            },
            success: function(resp) {
                //var jsonObj = jQuery.parseJSON(resp);
                jsonObj = resp;
                if(typeof jsonObj.error !== 'undefined'){
                    $("#alertModalBody").text("Error : " + jsonObj.error.message);    
                    $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                    $("#alertModal").modal('show');    
                }else{
                    $("#alertModalBody").text("Success : <?php echo ( $data['agentId'] >= 0 ) ? __('messages.saved') : __('messages.created'); ?>");    
                    $("#alertModalLabel").text("<?php echo __('messages.success'); ?>");
                    $('#alertModal').on('hidden.bs.modal', function () {
                        window.open("<?php echo url('user'); ?>","_self");    
                    }).modal('show');
                }
            },
            error:function(resp) {
                $("#alertModalBody").text("Network Error : " + resp.statusText);
                $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                $("#alertModal").modal('show');
            },
                
        });   
    });
</script>
@endsection

