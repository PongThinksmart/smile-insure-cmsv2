@extends('dashboard.dashboard', [
    'activeMenu' => 'customer',
    'breadcrumb' => [
        array(
            'name' => 'Customer Management',
            'uri' => ''   
        ),
        array(
            'name' => 'Customer List',
            'uri' => 'customer' 
        ),
        array(
            'name' => 'Edit a Customer',
            'uri' =>  'customer/' . $data['vehId'],
        ),
        array(
            'name' => 'Quotation',
            'uri' => '' 
        )
    ]

] )
@section('css')
<style>
    .form-control {
        position: absolute;
        height: 1.25rem;
        font-size: 0.75rem;
    }
</style>
@endsection

@section('content')
<form action="{{ url('pdf/viriya') }}" method="post" target="_blank">
{{ csrf_field() }}
<div id="form" style="background: url('{{ url("img/v-bg-form.jpg") }}');background-size: 1053px 1211px;height:1211px;width:1053px;margin: auto;">
    <input type="text" name="branchName" value="" class="form-control" style="width: 204px;margin-left: 823px;margin-top: 6px;">
    <input type="text" name="agentNo" value="" class="form-control" style="width: 171px;margin-left: 856px;margin-top: 32px;">
    <input type="text" name="insureNo" value="{{$data['insureNo']}}" class="form-control" style="width: 169px;margin-left: 858px;margin-top: 59px;">
    <input type="text" data-provide="datepicker" name="contractDate" value="{{$data['contractDate']}}" class="form-control" style="width: 173px;margin-left: 854px;margin-top: 85px;">
    <div style="margin-left: 40px;margin-top: 185px;position: absolute;">
        <input type="radio" name="workType" value="1" class="form-check-input" style="margin-left: 148px;margin-top: 7px;" {{ ($data['workType']==1) ? "checked" : ""}} >
        <input type="radio" name="workType" value="2" class="form-check-input" style="margin-left: 261px;margin-top: 7px;" {{ ($data['workType']==2) ? "checked" : ""}} >
        
        <input type="text" name="oldPolicyNo" value="" class="form-control" style="width: 150px;margin-left: 440px;margin-top: 4px;">
        
        <input type="radio" name="insureType" value="4" class="form-check-input" style="margin-left: 717px;margin-top: 7px;" {{ ($data['insureType']==4) ? "checked" : ""}}>
        <input type="radio" name="insureType" value="6" class="form-check-input" style="margin-left: 768px;margin-top: 7px;" {{ ($data['insureType']==6) ? "checked" : ""}}>
        <input type="radio" name="insureType" value="8" class="form-check-input" style="margin-left: 820px;margin-top: 7px;" {{ ($data['insureType']==8) ? "checked" : ""}}>
        <input type="radio" name="insureType" value="9" class="form-check-input" style="margin-left: 860px;margin-top: 7px;" {{ ($data['insureType']==9) ? "checked" : ""}}>
        <input type="text" name="insureTypeOther" value="{{$data['insureTypeOther']}}" class="form-control" style="width: 70px;margin-left: 910px;margin-top: 4px;">
        
        <input type="checkbox" name="isPDF" value="1" class="form-check-input" style="margin-left: 261px;margin-top: 37px;">
        
        <input type="text" name="email" value="{{$data['email']}}" class="form-control" style="width: 339px;margin-left: 641px;margin-top: 31px;">
        <input type="text" name="custName" value="{{$data['custName']}}" class="form-control" style="width: 454px;margin-left: 130px;margin-top: 55px;">
        <input type="text" name="occupation" value="" class="form-control" style="width: 351px;margin-left: 629px;margin-top: 55px;">
        <input type="text" name="idCardNumber" value="{{$data['idCardNumber']}}" class="form-control" style="width: 306px;margin-left: 278px;margin-top: 82px;">
        
        <input type="radio" name="isHq" value="1" class="form-check-input" style="margin-left: 608px;margin-top: 86px;">
        <input type="radio" name="isHq" value="-1" class="form-check-input" style="margin-left: 747px;margin-top: 86px;">
        <input type="text" name="branchNo" value="" class="form-control" style="width: 171px;margin-left: 809px;margin-top: 81px;">
        
        <input type="text" name="fullAddr" value="{{$data['fullAddr']}}" class="form-control" style="width: 941px;margin-left: 39px;margin-top: 108px;">
        <input type="text" name="zipCode" value="{{$data['zipCode']}}" class="form-control" style="width: 70px;margin-left: 524px;margin-top: 131px;">
        <input type="text" name="phoneNo" value="{{$data['phoneNo']}}" class="form-control" style="width: 330px;margin-left: 650px;margin-top: 131px;">
    </div>
    <div style="margin-left: 40px;margin-top: 342px;position: absolute;">
        <input type="radio" name="driverReceive" value="0" class="form-check-input" style="margin-left: 262px;margin-top: 7px;" {{ ($data['driverReceive']==0) ? "checked" : ""}}>
        <input type="radio" name="driverReceive" value="1" class="form-check-input" style="margin-left: 453px;margin-top: 7px;" {{ ($data['driverReceive']==1) ? "checked" : ""}}>
    </div>
    <div style="margin-left: 40px;margin-top: 342px;position: absolute;">
        <input type="text" name="driverList[0][driverName]" value="{{ !empty($data['driverList'][0]['driverName']) ? $data['driverList'][0]['driverName'] : ''}}" class="form-control" style="width: 324px;margin-left: 43px;margin-top: 29px;">
        <input type="text" name="driverList[0][birthDateD]" value="{{ !empty($data['driverList'][0]['birthDateD']) ? $data['driverList'][0]['birthDateD'] : ''}}" class="form-control" style="width: 30px;margin-left: 460px;margin-top: 29px;padding-right: 0px;">
        <input type="text" name="driverList[0][birthDateM]" value="{{ !empty($data['driverList'][0]['birthDateM']) ? $data['driverList'][0]['birthDateM'] : ''}}" class="form-control" style="width: 68px;margin-left: 496px;margin-top: 29px;">
        <input type="text" name="driverList[0][birthDateY]" value="{{ !empty($data['driverList'][0]['birthDateY']) ? $data['driverList'][0]['birthDateY'] : ''}}" class="form-control" style="width: 58px;margin-left: 570px;margin-top: 29px;">
        <input type="text" name="driverList[0][occupation]" value="{{ !empty($data['driverList'][0]['occupation']) ? $data['driverList'][0]['occupation'] : ''}}" class="form-control" style="width: 311px;margin-left: 670px;margin-top: 29px;">
        <input type="text" name="driverList[0][driveLicenseNo]" value="{{ !empty($data['driverList'][0]['driveLicenseNo']) ? $data['driverList'][0]['driveLicenseNo'] : ''}}" class="form-control" style="width: 263px;margin-left: 104px;margin-top: 54px;">
        <input type="text" name="driverList[0][driveExpiryDateD]" value="{{ !empty($data['driverList'][0]['driveExpiryDateD']) ? $data['driverList'][0]['driveExpiryDateD'] : ''}}" class="form-control" style="width: 67px;margin-left: 425px;margin-top: 54px;">
        <input type="text" name="driverList[0][driveExpiryDateM]" value="{{ !empty($data['driverList'][0]['driveExpiryDateM']) ? $data['driverList'][0]['driveExpiryDateM'] : ''}}" class="form-control" style="width: 68px;margin-left: 498px;margin-top: 54px;">
        <input type="text" name="driverList[0][driveExpiryDateY]" value="{{ !empty($data['driverList'][0]['driveExpiryDateY']) ? $data['driverList'][0]['driveExpiryDateY'] : ''}}" class="form-control" style="width: 57px;margin-left: 571px;margin-top: 54px;">
        <input type="text" name="driverList[0][driveProvName]" value="{{ !empty($data['driverList'][0]['driveProvName']) ? $data['driverList'][0]['driveProvName'] : ''}}" class="form-control" style="width: 224px;margin-left: 757px;margin-top: 54px;">
    </div>
    <div style="margin-left: 40px;margin-top: 391px;position: absolute;">
        <input type="text" name="driverList[1][driverName]" value="{{ !empty($data['driverList'][1]['driverName']) ? $data['driverList'][1]['driverName'] : ''}}" class="form-control" style="width: 324px;margin-left: 43px;margin-top: 29px;">
        <input type="text" name="driverList[1][birthDateD]" value="{{ !empty($data['driverList'][1]['birthDateD']) ? $data['driverList'][1]['birthDateD'] : ''}}" class="form-control" style="width: 30px;margin-left: 460px;margin-top: 29px;padding-right: 0px;">
        <input type="text" name="driverList[1][birthDateM]" value="{{ !empty($data['driverList'][1]['birthDateM']) ? $data['driverList'][1]['birthDateM'] : ''}}" class="form-control" style="width: 68px;margin-left: 496px;margin-top: 29px;">
        <input type="text" name="driverList[1][birthDateY]" value="{{ !empty($data['driverList'][1]['birthDateY']) ? $data['driverList'][1]['birthDateY'] : ''}}" class="form-control" style="width: 58px;margin-left: 570px;margin-top: 29px;">
        <input type="text" name="driverList[1][occupation]" value="{{ !empty($data['driverList'][1]['occupation']) ? $data['driverList'][1]['occupation'] : ''}}" class="form-control" style="width: 311px;margin-left: 670px;margin-top: 29px;">
        <input type="text" name="driverList[1][driveLicenseNo]" value="{{ !empty($data['driverList'][1]['driveLicenseNo']) ? $data['driverList'][1]['driveLicenseNo'] : ''}}" class="form-control" style="width: 263px;margin-left: 104px;margin-top: 54px;">
        <input type="text" name="driverList[1][driveExpiryDateD]" value="{{ !empty($data['driverList'][1]['driveExpiryDateD']) ? $data['driverList'][1]['driveExpiryDateD'] : ''}}" class="form-control" style="width: 67px;margin-left: 425px;margin-top: 54px;">
        <input type="text" name="driverList[1][driveExpiryDateM]" value="{{ !empty($data['driverList'][1]['driveExpiryDateM']) ? $data['driverList'][1]['driveExpiryDateM'] : ''}}" class="form-control" style="width: 68px;margin-left: 498px;margin-top: 54px;">
        <input type="text" name="driverList[1][driveExpiryDateY]" value="{{ !empty($data['driverList'][1]['driveExpiryDateY']) ? $data['driverList'][1]['driveExpiryDateY'] : ''}}" class="form-control" style="width: 57px;margin-left: 571px;margin-top: 54px;">
        <input type="text" name="driverList[1][driveProvName]" value="{{ !empty($data['driverList'][1]['driveProvName']) ? $data['driverList'][1]['driveProvName'] : ''}}" class="form-control" style="width: 224px;margin-left: 757px;margin-top: 54px;">
    </div>
    <div style="margin-left: 40px;margin-top: 474px;position: absolute;">
        <input type="text" name="policyNo" value="" class="form-control" style="width: 480px;margin-left: 149px;margin-top: 4px;">
        <input type="text" name="receiveExpireDate" value="" class="form-control" style="width: 286px;margin-left: 695px;margin-top: 4px;">
    </div>
    <div style="margin-left: 40px;margin-top: 505px;position: absolute;">
        <input type="radio" name="vehTypeSelect" value="1" class="form-check-input" style="margin-left: 117px;margin-top: 8px;" {{ ($data['vehTypeSelect']==1) ? "checked" : ""}} >
        <input type="radio" name="vehTypeSelect" value="2" class="form-check-input" style="margin-left: 259px;margin-top: 8px;" {{ ($data['vehTypeSelect']==2) ? "checked" : ""}} >
        <input type="radio" name="vehTypeSelect" value="3" class="form-check-input" style="margin-left: 455px;margin-top: 8px;" {{ ($data['vehTypeSelect']==3) ? "checked" : ""}} >
        <input type="radio" name="vehTypeSelect" value="4" class="form-check-input" style="margin-left: 675px;margin-top: 8px;" {{ ($data['vehTypeSelect']==4) ? "checked" : ""}} >
        <input type="radio" name="vehTypeSelect" value="5" class="form-check-input" style="margin-left: 838px;margin-top: 8px;" {{ ($data['vehTypeSelect']==5) ? "checked" : ""}} >
        <input type="text" name="vehTypeName" value="{{$data['vehTypeName']}}" class="form-control" style="width: 102px;margin-left: 879px;margin-top: 3px;">
    </div>
    <div style="margin-left: 40px;margin-top: 537px;position: absolute;">
        <input type="radio" name="beneficiary" value="0" class="form-check-input" style="margin-left: 141px;margin-top: 8px;" {{ (!$data['beneficiary']) ? "checked" : ""}}>
        <input type="radio" name="beneficiary" value="1" class="form-check-input" style="margin-left: 259px;margin-top: 8px;" {{ ($data['beneficiary']) ? "checked" : ""}}>
        <input type="text" name="beneficiary_name" value="{{ $data['beneficiary_name'] }}" class="form-control" style="width: 622px;margin-left: 359px;margin-top: 3px;">
    </div>
    <div style="margin-left: 40px;margin-top: 621px;position: absolute;">
        <input type="text" name="vehTypeCode" value="{{$data['vehTypeCode']}}" class="form-control" style="width: 57px;margin-left: 54px;margin-top: 33px;">
        <input type="text" name="manuName" value="{{$data['manuName']}}" class="form-control" style="width: 144px;margin-left: 118px;margin-top: 3px;">
        <input type="text" name="modelName" value="{{$data['modelName']}}" class="form-control" style="width: 144px;margin-left: 118px;margin-top: 33px;">
        <input type="text" name="vehLicenseNo" value="{{$data['vehLicenseNo']}}" class="form-control" style="width: 103px;margin-left: 269px;margin-top: 3px;">
        <input type="text" name="vehProvName" value="{{$data['vehProvName']}}" class="form-control" style="width: 103px;margin-left: 269px;margin-top: 33px;">
        <input type="text" name="chasisNo" value="{{$data['chasisNo']}}" class="form-control" style="width: 202px;margin-left: 379px;margin-top: 3px;">
        <input type="text" name="engineNo" value="{{$data['engineNo']}}" class="form-control" style="width: 202px;margin-left: 379px;margin-top: 63px;">
        <input type="text" name="modelYear" value="{{$data['modelYear']}}" class="form-control" style="width: 56px;margin-left: 588px;margin-top: 33px;">
        <input type="text" name="bodyType" value="{{$data['bodyType']}}" class="form-control" style="width: 79px;margin-left: 651px;margin-top: 33px;">
        <input type="text" name="pCW" value="{{$data['pcw']}}" class="form-control" style="width: 120px;margin-left: 737px;margin-top: 33px;">
        <input type="text" name="totalInsure1" value="" class="form-control" style="width: 120px;margin-left: 866px;margin-top: 33px;">
    </div>
    <div style="margin-left: 40px;margin-top: 710px;position: absolute;">
        <input type="text" name="accessory" value="{{$data['accessory']}}" class="form-control" style="width: 687px;margin-left: 8px;margin-top: 25px;">
        <input type="text" name="accessoryPrice" value="{{$data['accessoryPrice']}}" class="form-control" style="width: 232px;margin-left: 754px;margin-top: 25px;">   
        <input type="radio" name="isSpecialAccessory" value="-1" class="form-check-input" style="margin-left: 154px;margin-top: 51px;">
        <input type="radio" name="isSpecialAccessory" value="1" class="form-check-input" style="margin-left: 260px;margin-top: 51px;">
        <input type="radio" name="SpecialAccessoryType" value="1" class="form-check-input" style="margin-left: 372px;margin-top: 51px;">
        <input type="radio" name="SpecialAccessoryType" value="2" class="form-check-input" style="margin-left: 506px;margin-top: 51px;">
        <input type="radio" name="SpecialAccessoryType" value="3" class="form-check-input" style="margin-left: 646px;margin-top: 51px;">
        <input type="radio" name="SpecialAccessoryType" value="4" class="form-check-input" style="margin-left: 800px;margin-top: 51px;">
        <input type="text" name="SpecialAccessoryOther" value="" class="form-control" style="width: 138px;margin-left: 848px;margin-top: 49px;">
    </div>
    <div style="margin-left: 40px;margin-top: 817px;position: absolute;">
        <input type="text" name="extLifeDamagePerPerson" value="{{$data['extLifeDamagePerPerson']}}" class="form-control" style="width: 247px;margin-left: 8px;margin-top: 49px;">
        <input type="text" name="extLifeDamagePerTime" value="{{$data['extLifeDamagePerTime']}}" class="form-control" style="width: 247px;margin-left: 8px;margin-top: 73px;">
        <input type="text" name="extAssetDamagePerTime" value="{{$data['extAssetDamagePerTime']}}" class="form-control" style="width: 247px;margin-left: 8px;margin-top: 119px;">
        <input type="text" name="extExcessPerTime" value="{{$data['extExcessPerTime']}}" class="form-control" style="width: 247px;margin-left: 8px;margin-top: 165px;">
    </div>
    <div style="margin-left: 360px;margin-top: 817px;position: absolute;">
        <input type="text" name="vehDamagePerTime" value="{{$data['vehDamagePerTime']}}" class="form-control" style="width: 235px;margin-left: 8px;margin-top: 26px;">
        <input type="text" name="vehExcessPerTime" value="{{$data['vehExcessPerTime']}}" class="form-control" style="width: 235px;margin-left: 8px;margin-top: 72px;">
        <input type="text" name="vehLostPerTime" value="{{$data['vehLostPerTime']}}" class="form-control" style="width: 241px;margin-left: 8px;margin-top: 119px;">
    </div>
    <div style="margin-left: 673px;margin-top: 817px;position: absolute;">
        <input type="text" name="permDisabilityDriver" value="{{$data['permDisabilityDriver']}}" class="form-control" style="width: 197px;margin-left: 120px;margin-top: 46px;">
        <input type="text" name="permDisabilityPassengerPerson" value="{{$data['permDisabilityPassengerPerson']}}" class="form-control" style="width: 66px;margin-left: 102px;margin-top: 68px;">
        <input type="text" name="permDisabilityPassenger" value="{{$data['permDisabilityPassenger']}}" class="form-control" style="width: 110px;margin-left: 188px;margin-top: 68px;">
        <input type="text" name="tempDisabilityDriver" value="{{$data['tempDisabilityDriver']}}" class="form-control" style="width: 152px;margin-left: 120px;margin-top: 112px;">
        <input type="text" name="tempDisabilityPassengerPerson" value="{{$data['tempDisabilityPassengerPerson']}}" class="form-control" style="width: 56px;margin-left: 103px;margin-top: 134px;">
        <input type="text" name="tempDisabilityPassenger" value="{{$data['tempDisabilityPassenger']}}" class="form-control" style="width: 67px;margin-left: 179px;margin-top: 134px;">
        <input type="text" name="treatmentDriver" value="{{$data['treatmentDriver']}}" class="form-control" style="width: 268px;margin-left: 29px;margin-top: 173px;">
        <input type="text" name="bailDriver" value="{{$data['bailDriver']}}" class="form-control" style="width: 264px;margin-left: 29px;margin-top: 212px;">
        <input type="checkbox" name="isTerrorism" value="1" class="form-check-input" style="margin-left: 41px;margin-top: 242px;">
    </div>
    <div style="margin-left: 40px;margin-top: 1078px;position: absolute;">
        <input type="text" name="totalInsure2" value="{{$data['totalInsure']}}" class="form-control" style="width: 265px;margin-left: 275px;margin-top: 6px;">
        <input type="text" value="-" class="form-control" style="width: 209px;margin-left: 320px;margin-top: 29px;">
        <input type="text" value="-" class="form-control" style="width: 109px;margin-left: 844px;margin-top: 13px;">
    </div>
    <div style="margin-left: 40px;margin-top: 1154px;position: absolute;">
        <input type="text" name="receiveDateD" value="{{$data['receiveDateD']}}" class="form-control" style="width: 53px;margin-left: 317px;margin-top: 6px;">
        <input type="text" name="receiveDateM" value="{{$data['receiveDateM']}}" class="form-control" style="width: 59px;margin-left: 375px;margin-top: 6px;">
        <input type="text" name="receiveDateY" value="{{$data['receiveDateY']}}" class="form-control" style="width: 57px;margin-left: 439px;margin-top: 6px;">
        <input type="text" name="receiveExpireDateD" value="{{$data['receiveExpireDateD']}}" class="form-control" style="width: 57px;margin-left: 561px;margin-top: 6px;">
        <input type="text" name="receiveExpireDateM" value="{{$data['receiveExpireDateM']}}" class="form-control" style="width: 62px;margin-left: 623px;margin-top: 6px;">
        <input type="text" name="receiveExpireDateY" value="{{$data['receiveExpireDateY']}}" class="form-control" style="width: 72px;margin-left: 690px;margin-top: 6px;">
    </div>
</div>
<div class="text-center"><button type="submit" class="btn btn-success btn-lg active">Preview</button></div>
</form>
@endsection

@section('script')
<script type="text/javascript">
    autonumber_arr = new Array();
    $(document).ready(function() {
        $( ".number2Digit" ).each(function( index ) {
            var id = $(this).attr('id');
            autonumber_arr[id] = new AutoNumeric('#' + id, { 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'true' , 'emptyInputBehavior' : 'zero' });    
        });
    });
</script>
@endsection

