@extends('dashboard.dashboard', [
    'activeMenu' => 'customer',
    'breadcrumb' => [
        array(
            'name' => 'Customer Management',
            'uri' => ''   
        ),
        array(
            'name' => 'Customer List',
            'uri' => '' 
        )
    ]

] )
@section('css')
<link rel="stylesheet" href="{{ asset('css/customer.css') }}"/>
@endsection

@section('content')

<!-- DataTables Example -->
    <div class="card mb-3">
        <div class="card-header ">
            <!-- searching layout -->
            <div class="row">
                <div class="form-group col-lg-3 col-md-6">
                    <label for="phoneNumber"><?php echo __('messages.phoneNumber'); ?></label>
                    <input type="tel" class="form-control" id="phoneNumber" >
                </div>
                <div class="form-group col-lg-2 col-md-6">
                    <label for="licenseNo"><?php echo __('messages.licenseNo'); ?></label>
                    <input type="text" class="form-control" id="licenseNo" >
                </div>
                <div class="form-group col-lg-7 col-md-7">
                    <label for="searchCustomerName"><?php echo __('messages.customerName'); ?></label>
                    <input type="tel" class="form-control" id="searchCustomerName" >
                </div>
                <div class="form-group col-lg-4 col-md-5">
                    <label for="status"><?php echo __('messages.status'); ?></label>
                    <select class="custom-select" id="status">
                        <option class="canEdit" value="0"><?php echo __('messages.all'); ?></option>
                        @foreach ($selectOptionList['customerStatus'] as $item)
                            @if ( ($item['custStatusid'] == 901) && (intval(session('userlevelId')) != 1) )
                                @continue        
                            @elseif (!empty($item['flagCanEdit']))
                            <option class="canEdit" value="{{$item['custStatusid']}}" >{{$item['custStatusid']}} - {{$item['custStatusName']}}</option>
                            @else
                            <option value="{{$item['custStatusid']}}" >{{$item['custStatusid']}} - {{$item['custStatusName']}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-lg-4 col-md-6">
                    <label for="sale"><?php echo __('messages.sale'); ?></label>
                    @if (session('userlevelId') == 5)
                        <select class="custom-select" id="sale" disabled >
                            <option value="{{ session('userId') }}">{{ session('nickname') }}</option>
                        </select>
                    @else
                        <select class="custom-select" id="sale" >
                            <option value="0"><?php echo __('messages.all'); ?></option>
                            @foreach ($selectOptionList['agentList'] as $item)
                                <option value="{{$item['id']}}" >{{$item['name']}}</option>
                            @endforeach
                        </select>
                    @endif
                    
                </div>
                <div class="form-group col-lg-4 col-md-6">
                    <label for="vehId"><?php echo __('messages.customerId'); ?></label>
                    <input type="number" class="form-control" id="vehId" >
                </div>
                <div class="form-group offset-lg-2 col-lg-4 col-md-6">
                    <label for="dueDatePaymentStart"><?php echo __('messages.dueDatePaymentStart'); ?></label>
                    <input data-provide="datepicker" type="text" class="form-control" id="dueDatePaymentStart" >
                </div>
                <div class="form-group col-lg-4 col-md-6">
                    <label for="dueDatePaymentEnd"><?php echo __('messages.dueDatePaymentEnd'); ?></label>
                    <input data-provide="datepicker" type="text" class="form-control" id="dueDatePaymentEnd" >
                </div>
                @if (session('userlevelId') == 5)
                    <div class="col-lg-4 offset-lg-2">
                        <button type="button" class="btn btn-primary" style="width:100%;" id="searchButton"><i class="fas fa-search"></i> <?php echo __('messages.search'); ?></button>
                    </div>
                    
                    <div class="col-lg-4">
                        <button type="button" class="btn btn-success" style="width:100%;" id="requestButton"><i class="fas fa-plus-circle"></i> <?php echo __('messages.requestlead'); ?></button>
                    </div>
                @else
                    <div class="col-lg-4 offset-lg-4">
                        <button type="button" class="btn btn-primary" style="width:100%;" id="searchButton"><i class="fas fa-search"></i> <?php echo __('messages.search'); ?></button>
                    </div>
                @endif
            </div> 
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              </table>
            </div>
        </div>
    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
</div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        
        $.fn.dataTable.ext.errMode = 'none';
        
        $('#dataTable').on('click', 'tbody tr', function() {
            var tempData = $('#dataTable').DataTable().row(this).data();
            window.open('<?php echo url("customer"); ?>/' + tempData.vehId,'_blank');
        })
        
        $('#dataTable').on( 'error.dt', function ( e, settings, techNote, message ) {
            $("#alertModalBody").text(message);
            $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
            $("#alertModal").modal('show');
            //console.log( 'An error has been reported by DataTables: ', message );
        })
        .DataTable({
            columns: [
                { "title": "ลำดับ", "data" : "order"},
                { "title": "วันที่นัดหมาย", "data": "dueDate"},
                {
                    "title": "Note",
                    "data": "staffNote",
                    "render": function ( data, type, row ) {
                        if(data){
                            return data.length > 30 ?
                            data.substr( 0, 30 ) +'…' :
                            data;
                        }
                        return "";
                    }
                },
                { "title": "ชื่อลูกค้า", "data": "custName"},
                { "title": "เลขทะเบียน", "data": "vehLicenseNo"},
                { "title": "ยี่ห้อรถยนต์", "data": "manuName"},
                { 
                    "title": "รุ่นรถยนต์",
                    "data": "modelName",
                    "render": function ( data, type, row ) {
                        if(data){
                            return data.length > 20 ?
                            data.substr( 0, 20 ) +'…' :
                            data;
                        }
                        return "";
                    }},
                { "title": "วันที่หมดประกัน", "data": "lastExpireDate"},
                { "title": "สถานะลูกค้า", "data": "statusName"},
                { "title": "วันนัดชำระ", "data": "dueDatePayment"},
                { "title": "พนักงานขาย", "data": "saleAgentName"}
            ],
            lengthMenu: [ 50, 100, 200, 500 ],
            ordering: false,
            searching: false,
            processing: true,
            serverSide: true,
            ajax:{
                url: "<?php echo url('customer/getCustomerList'); ?>",
                type: 'GET',
                data:function (d) {
                    d.phonenumber = $("#phoneNumber").val();
                    d.licenseno = $("#licenseNo").val();
                    d.customername = $("#searchCustomerName").val();
                    d.status = $("#status").val();
                    d.agent = $("#sale").val();
                    d.vehId = $("#vehId").val();
                    d.dueDatePaymentStart = $("#dueDatePaymentStart").val();
                    d.dueDatePaymentEnd = $("#dueDatePaymentEnd").val();
                },
            },
            "dom": 'pltip',
        });
        
        $('#searchButton').click( function( e ){
            console.log($("#status").val());
            $('#dataTable').DataTable().draw(true);          
        });
        
        $('#requestButton').click( function( e ){
            $.ajax({
                url: '<?php echo url('customer/requestLeadData'); ?>',
                type: "GET",
                data:{
                    agent: {{ session('userId') }},
                },
                success: function(resp) {
                    window.open("<?php echo url('customer'); ?>","_self");
                },
                error:function(resp) {
                    //var jsonObj = jQuery.parseJSON(resp);
                    jsonObj = resp;
                    $("#alertModalBody").text("Login : " + jsonObj.error.message);    
                    $("#alertModalLabel").text("<?php echo __('messages.warning'); ?>");
                    $("#alertModal").modal('show');
                },
            });          
        });
    });
</script>
@endsection

