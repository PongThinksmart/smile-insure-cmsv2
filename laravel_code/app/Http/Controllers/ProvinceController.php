<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Model\Api;

class ProvinceController extends Controller
{
    public function mapping(){
        $returnData = Api::request("listWaitingImportLots");
        $selectOptionList['lotId'] = !empty($returnData['result']['importLots']) ? $returnData['result']['importLots'] : array();
        
        $returnData = Api::request("listProvinces");
        $selectOptionList['listProvinces'] = !empty($returnData['result']['provinces']) ? $returnData['result']['provinces'] : array();
        foreach($selectOptionList['listProvinces'] as $key => $item){
            if($item['id']==0){
                unset($selectOptionList['listProvinces'][$key]);
                break;
            }
        }
        return view('province.mapping', [ 'selectOptionList' => $selectOptionList ]);    
    }
    
    public function getMismatch(Request $request){
        $data = array(
            'lotId' => $request->has('lotId') ? intval($request->input('lotId')) : null,
            'provName' => $request->has('provName') ? $request->input('provName') : null,
            'offset' => $request->has('start') ? intval($request->input('start')) : 0 ,
            'limit' => $request->has('length') ? intval($request->input('length')) : 0,
        );
        $returnData = Api::request('getMismatchProvincesByLotId', $data);
        $responseData = array();
        if(!empty($returnData['result'])){
            $total = !empty($countdata['result']['total']) ? intval($countdata['result']['total']) : count($returnData['result']['mismatches']);
            $listData = !empty($returnData['result']['mismatches']) ? $returnData['result']['mismatches'] : array();
            $i = $request->has('start') ? intval($request->input('start')) : 0;
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'recordsTotal' => $total,
                'recordsFiltered' => $total,
                'data' => $listData,
            );        
        }else{
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'error' => $returnData['error']['message'],
                'data' => [],
            );    
        }
        return response()->json($responseData);     
    }
    
    public function replace($id, Request $request){
        $mismatches = array();
        if($request->has('checkList')){
            $arrCheckList = $request->input('checkList');
            foreach($arrCheckList as $provName ){
                $mismatches[] = array('provName' => $provName);    
            }
        }  
        $data = array(
            "lotId" => intval($id),
            "provId" => $request->has('provId') ? intval($request->input('provId')) : 0,
            "agentId" => intval(session('userId',0)),
            "mismatches" => $mismatches,
        );
        $returnData = Api::request('replaceMismatchProvinces', $data);
        return response()->json($returnData);
    }
}
