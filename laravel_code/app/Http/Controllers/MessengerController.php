<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Model\Api;

class MessengerController extends Controller
{
    public function postIndex(){
        $returnData = Api::request('getAllMessenger');
        $data = empty($returnData['result']['messengerObj']) ? array() : $returnData['result']['messengerObj'];
        
        $listMessengerType = Api::request("getMessengerType");
        $listMessengerType = (!empty($listMessengerType['result'])) ? $listMessengerType['result']['messengerTypeList'] : array();
        
        foreach($data as $key => $item){
            $data[$key]['notificationDate'] = !empty($item['notificationDate']) ? date("Y-m-d", ($item['notificationDate']/1000)) : '';
            $data[$key]['sentReqDate'] = !empty($item['sentReqDate']) ? date("Y-m-d", ($item['sentReqDate']/1000)) : '';
            $data[$key]['messengerTypeName'] = null;
            foreach($listMessengerType as $subItem){
                if($subItem['messengerTypeId'] == $item['messengerTypeId']){
                    $data[$key]['messengerTypeName'] = $subItem['messengerTypeName'];
                    break;
                }
            }
        }
        return view('messenger.post', [ "data" => $data ]);
    }
    
    public function postUpdate(Request $request){
        $data = $request->all();
        $messengerObj = array();
        $messengerObj[] = array(
            'messengerId' => intval($data['messengerId']),
            //'messengerStatusFinish' => empty($data['messengerStatusFinish']) ? null : boolval($data['messengerStatusFinish']),
            'sentReqEms' => empty($data['sentReqEms']) ? null : $data['sentReqEms'],
            'sentReqDate' => empty($item['sentReqDate']) ? null : (strtotime( $item['sentReqDate'] . " 00:00:00")*1000),
            'waitingEmsTag' => empty($item['waitingEmsTag']) ? false : boolval($item['waitingEmsTag']),
        );
        $returnData = Api::request('updateMessenger', array( "messengerObj" => $messengerObj , "messengerStatusFinish" => empty($data['messengerStatusFinish']) ? null : boolval($data['messengerStatusFinish']) ) );
        
        //echo json_encode($returnData);
        return redirect('messenger/post');
    }
    
    public function listIndex(){
        $returnData = Api::request('getAllSentMessenger');
        $data = empty($returnData['result']['sentMessengerObj']) ? array() : $returnData['result']['sentMessengerObj'];
        foreach($data as $key => $item){
            $data[$key]['notificationDate'] = !empty($item['notificationDate']) ? date("Y-m-d", ($item['notificationDate']/1000)) : '';
            $data[$key]['dueDateSM'] = !empty($item['dueDate']) ? date("Y-m-d", ($item['dueDate']/1000)) : '';
            $data[$key]['dueTimeSM'] = !empty($item['dueDate']) ? date("H:i", ($item['dueDate']/1000)) : '';
        }
        return view('messenger.list', [ "data" => $data ]);
    }
}
