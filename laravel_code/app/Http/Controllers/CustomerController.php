<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use App\Model\Api;
use App\Helper\ArrayHelper;

class CustomerController extends Controller
{
    public function index(){
        // listSaleAgents
        $returnData = Api::request("listSaleAgents");
        $selectOptionList['agentList'] = !empty($returnData['result']['agents']) ? $returnData['result']['agents'] : array();
        
        // listCustomerStatus
        $returnData = Api::request("listCustomerStatus", array(
            "agentLevelId" => intval(session('userlevelId')),
            "actionTypeId" => intval(2),
        ));
        $ArrayHelper = new ArrayHelper();
        $selectOptionList['customerStatus'] = !empty($returnData['result']['customerStatus']) ? $returnData['result']['customerStatus'] : array();
        $selectOptionList['customerStatus'] = $ArrayHelper->sortArray($selectOptionList['customerStatus'], 'custStatusid');
        
        return view('customer.index', [ 'selectOptionList' => $selectOptionList ]);
    }
    
    public function new_detail(){
        $returnData['result']['manuId'] = 0;
        $returnData['result']['modelId'] = 0;

        $returnData['result']['dueDateHours'] = '0';
        $returnData['result']['dueDateMinutes'] = '0';
        $returnData['result']['dueDate'] = '';
        
        $manuData = Api::request("getManufacturer");
        $manuData = !empty($manuData['result']['manufacturers']) ? $manuData['result']['manufacturers'] : array();
        foreach($manuData as $key => $item){
            if(empty($item['status'])){
                unset($manuData[$key]);
                continue;
            }  
        }
        $modelData = array();
        
        //Normalize
        $returnData['result']['sourceId'] = 0;
        $returnData['result']['status'] = 100;
        $returnData['result']['staffNote'] = '';
        $returnData['result']['custName'] = '';
        $returnData['result']['idCardNo'] = '';
        $returnData['result']['birthDate'] = '';
        $returnData['result']['email'] = '';
        $returnData['result']['vehTypeId'] = 0;
        $returnData['result']['vehLicenseNo'] = '';
        $returnData['result']['chasisNo'] = '';
        $returnData['result']['vehProvId'] = 0;
        $returnData['result']['engineCapacity'] = 0;
        $returnData['result']['engineNo'] = '';
        $returnData['result']['modelYear'] = '';
        $returnData['result']['passenger'] = 0;
        $returnData['result']['bodyType'] = '';
        $returnData['result']['accessory'] = '';
        $returnData['result']['accessoryPrice'] = 0.00;
        $returnData['result']['taxDate'] = '';
        $returnData['result']['lastExpireDate'] = '';
        $returnData['result']['buyDate'] = '';
        $returnData['result']['lastPartnerName'] = '';
        $returnData['result']['custId'] = -1;
        $returnData['result']['vehId'] = -1;
        $returnData['result']['hasCctv'] = 0;
        $returnData['result']['isReadOnly'] = empty($returnData['result']['isReadOnly']) ? 0 : $returnData['result']['isReadOnly'];
        $returnData['result']['payType'] = 1;
        $returnData['result']['select_paydown'] = 2;
        $returnData['result']['workType'] = 1;
        $returnData['result']['paychannel'] = 1;
        $returnData['result']['driverRecrive'] = 0;
        $returnData['result']['beneficiary'] = 0;
        $returnData['result']['saleAgentName'] = "";
        $returnData['result']['saleAgentId'] = 0;
        $returnData['result']['messengerTypeId'] = 0;
        
        $returnData['result']['insureId'] = "";
        $returnData['result']['insureNo'] = "";
        $returnData['result']['postNo'] = "";
        $returnData['result']['insureType'] = 0;
        $returnData['result']['partnerType'] = 0;
        $returnData['result']['insureRate'] = 0;
        $returnData['result']['repairType'] = 0;
        $returnData['result']['receiveDate'] = '';
        $returnData['result']['receiveExpireDate'] = '';
        $returnData['result']['contractDate'] = '';
        $returnData['result']['beneficiary_id'] = null;
        $returnData['result']['beneficiary_name'] = '';
        
        $returnData['result']['itemProtectId'] = null;
        
        $returnData['result']['extLifeDamagePerPerson'] = 300000;
        $returnData['result']['extLifeDamagePerTime'] = 10000000;
        $returnData['result']['extAssetDamagePerTime'] = 600000;
        $returnData['result']['extExcessPerTime'] = 0;
        
        $returnData['result']['vehDamagePerTime'] = 0;
        $returnData['result']['vehExcessPerTime'] = 0;
        $returnData['result']['vehLostPerTime'] = 0;
        
        $returnData['result']['permDisabilityDriver'] = 0;
        $returnData['result']['permDisabilityPassengerPerson'] = 0;
        $returnData['result']['permDisabilityPassenger'] = 0;
        
        $returnData['result']['tempDisabilityDriver'] = 0;
        $returnData['result']['tempDisabilityPassengerPerson'] = 0;
        $returnData['result']['tempDisabilityPassenger'] = 0;
        
        $returnData['result']['treatmentDriver'] = 0;
        $returnData['result']['bailDriver'] = 0;

        $returnData['result']['actPostcode'] = '';
        $returnData['result']['actType'] = 0;
        $returnData['result']['actPartnerType'] = 0;
        $returnData['result']['actReceiveDate'] = '';
        $returnData['result']['actReceiveExpireDate'] = '';
        $returnData['result']['caractId'] = '';
        
        $returnData['result']['orderStatus'] = 1;
        $returnData['result']['orderId'] = null;
        $returnData['result']['contactTypeId'] = 1;
        
        $returnData['result']['changeOfInsure'] = 0.00;
        $returnData['result']['changeOfCaract'] = 0.00;
        $returnData['result']['dutyInsure'] = 0.00;
        $returnData['result']['dutyCaract'] = 0.00;
        $returnData['result']['taxInsure'] = 0.00;
        $returnData['result']['taxCaract'] = 0.00;
        $returnData['result']['totalInsure'] = 0.00;
        $returnData['result']['totalCaract'] = 0.00;
        $returnData['result']['agentDiscount'] = 0.00;
        $returnData['result']['freeCarAct'] = 0;
        $returnData['result']['totalPaymentInsure'] = 0.00;
        
        $returnData['result']['campaignSpecialInsure'] = "";
        $returnData['result']['totalGiftFree'] = 0.00;
        
        $returnData['result']['phoneList'] = [];
        $returnData['result']['lineList'] = [];
        $returnData['result']['addressList'] = [];
        
        $returnData['result']['paymentList'] = array();
        for( $count = 1; $count <=6 ; $count++ ){
            $returnData['result']['paymentList'][$count] = array(
                'amount' => 0,
                'payId' => null,
                'status' => 0,
                'dueDate' => "",    
            );    
        }
        
        $returnData['result']['driverList'] = array(
            0 => array(
                'driverName' => '',    
                'birthDate' => '',    
                'occupation' => '',    
                'driveLicenseNo' => '',    
                'driveExpiryDate' => '',    
                'driveProvId' => 0,
                'driverId' => null
            ),
            1 => array(
                'driverName' => '',    
                'birthDate' => '',    
                'occupation' => '',    
                'driveLicenseNo' => '',    
                'driveExpiryDate' => '',    
                'driveProvId' => 0,
                'driverId' => null
            ),
        );
        
        // OptionList 
        $selectOptionList = Api::request("getCustomerDetailDropdowns", array(
            "agentId" => intval(session('userId')),
        ));
        $selectOptionList = !empty($selectOptionList['result']) ? $selectOptionList['result'] : array();
        
        $customerStatus = Api::request("listCustomerStatus", array(
            "agentLevelId" => intval(session('userlevelId')),
            "actionTypeId" => intval(1),
        ));
        $listSaleAgents = Api::request("listSaleAgents");
        
        $param = array(
            "isWalkin" => null,
        );
        $listSources = Api::request("listSources",$param);
        $listMessengerType = Api::request("getMessengerType");
        
        $ArrayHelper = new ArrayHelper();
        $selectOptionList['customerStatus'] = (!empty($customerStatus['result'])) ? $customerStatus['result']['customerStatus'] : array();
        $selectOptionList['customerStatus'] = $ArrayHelper->sortArray($selectOptionList['customerStatus'], 'custStatusid');
        $selectOptionList['listMessengerType'] = (!empty($listMessengerType['result'])) ? $listMessengerType['result']['messengerTypeList'] : array();
        $selectOptionList['listSaleAgents'] = (!empty($listSaleAgents['result'])) ? $listSaleAgents['result']['agents'] : array();
        $selectOptionList['listSources'] = (!empty($listSources['result'])) ? $listSources['result']['sources'] : array();
        $selectOptionList['listManu'] = $manuData;
        $selectOptionList['listModel'] = $modelData;
        
        return view('customer.detail', [ 'data' => $returnData['result'], "selectOptionList" => $selectOptionList ]);
    }
    
    public function detail($id, Request $request){
        $returnData = Api::request("getCustomerDetailsByVehId", array(
            "vehId" => intval($id),
            "updateAgentId" => intval(session('userId',0)),
        ));
        if(empty($returnData['result'])){
            $sidebarAndBreadcrumb = [
                'activeMenu' => 'brand',
                'breadcrumb' => [
                    array(
                        'name' => 'Customer Management',
                        'uri' => ''   
                    ),
                    array(
                        'name' => 'Customer List',
                        'uri' => 'customer' 
                    ),
                    array(
                        'name' => ( intval($id) >= 0 ) ? 'Edit a Customer' : 'Create a new Customer',
                        'uri' => '' 
                    )
                ]
            ];
            return view('notfound', [ 'sidebarAndBreadcrumb' => $sidebarAndBreadcrumb, 'backlink' => [ 'url' => url('customer'), 'text' => 'Back to Customer List', ], ] );    
        }
        
        $returnData['result']['manuId'] = 0;
        $returnData['result']['modelId'] = 0;

        if(!empty($returnData['result']['dueDate'])){   
            $returnData['result']['dueDateHours'] = date("G", strtotime($returnData['result']['dueDate']));    
            $returnData['result']['dueDateMinutes'] = intval(date("i", strtotime($returnData['result']['dueDate']))) . "";
            $returnData['result']['dueDate'] = date("Y-m-d", strtotime($returnData['result']['dueDate']));    
        }else{
            $returnData['result']['dueDateHours'] = '0';
            $returnData['result']['dueDateMinutes'] = '0';
        }
        
        // Manu Search
        $manuData = Api::request("getManufacturer");
        $manuData = !empty($manuData['result']['manufacturers']) ? $manuData['result']['manufacturers'] : array();
        foreach($manuData as $key => $item){
            if(empty($item['status'])){
                unset($manuData[$key]);
                continue;
            }
            if(!empty($returnData['result']['manuName'])){
                if($item['manuName']==$returnData['result']['manuName']){
                    $returnData['result']['manuId'] =  $item['manuId'];
                }    
            }    
        }    
        
        // Model Search
        $modelData = array();
        if(!empty($returnData['result']['manuId'])){
            $modelData = Api::request("getModel", array(
                "manuId" => intval($returnData['result']['manuId']),
            ));
            $modelData = !empty($modelData['result']['models']) ? $modelData['result']['models'] : array();
            foreach($modelData as $key => $item){
                if(empty($item['status'])){
                    unset($modelData[$key]);
                    continue;
                }
                if(!empty($returnData['result']['modelName'])){
                    if($item['modelName']==$returnData['result']['modelName']){
                        $returnData['result']['modelId'] =  $item['modelId']; 
                    }   
                }
                
            }       
        }
        
        //Normalize
        $returnData['result']['isReadOnly'] = empty($returnData['result']['isReadOnly']) ? 0 : $returnData['result']['isReadOnly'];
        $returnData['result']['payType'] = 1;
        $returnData['result']['select_paydown'] = 2;
        $returnData['result']['workType'] = 1;
        $returnData['result']['paychannel'] = 1;
        $returnData['result']['driverRecrive'] = 0;
        $returnData['result']['beneficiary'] = 0;
        $returnData['result']['saleAgentName'] = empty($returnData['result']['saleAgentName']) ? "" : $returnData['result']['saleAgentName'];
        
        $returnData['result']['insureId'] = "";
        $returnData['result']['insureNo'] = "";
        $returnData['result']['postNo'] = "";
        $returnData['result']['insureType'] = 0;
        $returnData['result']['partnerType'] = 0;
        $returnData['result']['insureRate'] = 0;
        $returnData['result']['repairType'] = 0;
        $returnData['result']['receiveDate'] = '';
        $returnData['result']['receiveExpireDate'] = '';
        $returnData['result']['contractDate'] = '';
        $returnData['result']['beneficiary_id'] = null;
        $returnData['result']['beneficiary_name'] = '';
        
        $returnData['result']['itemProtectId'] = null;
        
        $returnData['result']['extLifeDamagePerPerson'] = 300000;
        $returnData['result']['extLifeDamagePerTime'] = 10000000;
        $returnData['result']['extAssetDamagePerTime'] = 600000;
        $returnData['result']['extExcessPerTime'] = 0;
        
        $returnData['result']['vehDamagePerTime'] = 0;
        $returnData['result']['vehExcessPerTime'] = 0;
        $returnData['result']['vehLostPerTime'] = 0;
        
        $returnData['result']['permDisabilityDriver'] = 0;
        $returnData['result']['permDisabilityPassengerPerson'] = 0;
        $returnData['result']['permDisabilityPassenger'] = 0;
        
        $returnData['result']['tempDisabilityDriver'] = 0;
        $returnData['result']['tempDisabilityPassengerPerson'] = 0;
        $returnData['result']['tempDisabilityPassenger'] = 0;
        
        $returnData['result']['treatmentDriver'] = 0;
        $returnData['result']['bailDriver'] = 0;

        $returnData['result']['actPostcode'] = '';
        $returnData['result']['actType'] = 0;
        $returnData['result']['actPartnerType'] = 0;
        $returnData['result']['actReceiveDate'] = '';
        $returnData['result']['actReceiveExpireDate'] = '';
        $returnData['result']['caractId'] = '';
        
        $returnData['result']['orderStatus'] = 1;
        $returnData['result']['orderId'] = null;
        $returnData['result']['contactTypeId'] = 1;
        
        $returnData['result']['changeOfInsure'] = 0.00;
        $returnData['result']['changeOfCaract'] = 0.00;
        $returnData['result']['dutyInsure'] = 0.00;
        $returnData['result']['dutyCaract'] = 0.00;
        $returnData['result']['taxInsure'] = 0.00;
        $returnData['result']['taxCaract'] = 0.00;
        $returnData['result']['totalInsure'] = 0.00;
        $returnData['result']['totalCaract'] = 0.00;
        $returnData['result']['agentDiscount'] = 0.00;
        $returnData['result']['freeCarAct'] = 0;
        $returnData['result']['totalPaymentInsure'] = 0.00;
        
        $returnData['result']['campaignSpecialInsure'] = "";
        $returnData['result']['totalGiftFree'] = 0.00;
        
        $returnData['result']['paymentList'] = array();
        for( $count = 1; $count <=6 ; $count++ ){
            $returnData['result']['paymentList'][$count] = array(
                'amount' => 0,
                'payId' => null,
                'status' => 0,
                'dueDate' => "",    
            );    
        }
        
        $returnData['result']['driverList'] = array(
            0 => array(
                'driverName' => '',    
                'birthDate' => '',    
                'occupation' => '',    
                'driveLicenseNo' => '',    
                'driveExpiryDate' => '',    
                'driveProvId' => 0,
                'driverId' => null
            ),
            1 => array(
                'driverName' => '',    
                'birthDate' => '',    
                'occupation' => '',    
                'driveLicenseNo' => '',    
                'driveExpiryDate' => '',    
                'driveProvId' => 0,
                'driverId' => null
            ),
        );
        
        $returnData['result']['messengerTypeId'] = 0;
        
        if(empty($returnData['result']['messengerObj'])){
            $returnData['result']['messengerObj'] = [];
            $returnData['result']['messengerObj'][] = array(
                "sentReqDate" => '',
                "sentActTag" => false,
                "sentReqEms" => '',
                "description" => '',
                "messengerId" => null,
                "messengerStatusFinish" => false,
                "sentRenewTag" => false,
                "messengerTypeId" => null,
                "sentEndorseTag" => false,
                "campaign" => false,
                "campaignDetail" => "",
                "sentReqTag" => false,
                "sentOtherItemList" => [],
                "sentReceiptTag" => false,
                "sentDepartmentTag" => false,
                "notificationDate" => '',
                "sentFormCutCardTag" => false,
                "sentOtherItemList" => array(
                    array(
                        "sentOtherItemId" => null,
                        "sentOtherDetail" => '',    
                        "sentOtherTag" => false,    
                    )
                )
            );    
        }else{
            $hasMessengerStatusUnFinish = false;
            foreach($returnData['result']['messengerObj'] as $key => $item){
                $returnData['result']['messengerObj'][$key]['notificationDate'] = !empty($item['notificationDate']) ? date("Y-m-d", ($item['notificationDate']/1000)) : '';
                $returnData['result']['messengerObj'][$key]['sentReqDate'] = !empty($item['sentReqDate']) ? date("Y-m-d", ($item['sentReqDate']/1000)) : '';
                if(empty($item['messengerStatusFinish'])){
                    $hasMessengerStatusUnFinish = true;
                }
                if(empty($item['sentOtherItemList'][0])){
                    $returnData['result']['messengerObj'][$key]['sentOtherItemList'] = array();
                    $returnData['result']['messengerObj'][$key]['sentOtherItemList'][] = array(
                        "sentOtherItemId" => null,
                        "sentOtherDetail" => "",
                        "sentOtherTag" => false,
                    );    
                }
            }
            if( (!$hasMessengerStatusUnFinish) && (intval($returnData['result']['status']) != 701)){
                array_unshift($returnData['result']['messengerObj'], array(
                    "sentReqDate" => '',
                    "sentActTag" => false,
                    "sentReqEms" => '',
                    "description" => '',
                    "messengerId" => null,
                    "messengerStatusFinish" => false,
                    "sentRenewTag" => false,
                    "messengerTypeId" => null,
                    "sentEndorseTag" => false,
                    "campaign" => false,
                    "campaignDetail" => "",
                    "sentReqTag" => false,
                    "sentOtherItemList" => [],
                    "sentReceiptTag" => false,
                    "sentDepartmentTag" => false,
                    "notificationDate" => '',
                    "sentFormCutCardTag" => false,
                    "sentOtherItemList" => array(
                        array(
                            "sentOtherItemId" => null,
                            "sentOtherDetail" => '',    
                            "sentOtherTag" => false,    
                        )
                    )
                ));    
            }
        }
        
        if(empty($returnData['result']['sentMessengerObj'])){
            $returnData['result']['sentMessengerObj'] = [];
            $returnData['result']['sentMessengerObj'][] = array(
                "sentMessengerId" => null,
                "detail" => "",
                "dueDateSM" => "",
                "dueTimeSM" => "",
                "notificationDate" => "",
                "note" => "",
                "receiveDocTag" => false,
                "receiveDocDetail" => "",
                "sentBill" => false,
                "sentReq" => false,
                "sentAct" => false,
                "sentDepartment" => false,
                "receiveFirstInstallmentTag" => false,
                "receiveFirstInstallmentDetail" => "",
                "receiveLastInstallmentTag" => false,
                "receiveLastInstallmentDetail" => "",
                "photographTag" => false,
                "photographDetail" => "",
                "completeSentMessengerFlag" => false,
            );    
        }else{
            $hasSentMessengerObjUnFinish = false;
            foreach($returnData['result']['sentMessengerObj'] as $key => $item){
                if(empty($item['completeSentMessengerFlag'])){
                    $hasSentMessengerObjUnFinish = true;
                }
                $returnData['result']['sentMessengerObj'][$key]['notificationDate'] = !empty($item['notificationDate']) ? date("Y-m-d", ($item['notificationDate']/1000)) : '';
                $returnData['result']['sentMessengerObj'][$key]['dueDateSM'] = !empty($item['dueDate']) ? date("Y-m-d", ($item['dueDate']/1000)) : '';
                $returnData['result']['sentMessengerObj'][$key]['dueTimeSM'] = !empty($item['dueDate']) ? date("H:i", ($item['dueDate']/1000)) : '';
            }
            if( !$hasSentMessengerObjUnFinish && (intval($returnData['result']['status']) != 105) ){
                array_unshift($returnData['result']['sentMessengerObj'], array(
                    "sentMessengerId" => null,
                    "detail" => "",
                    "dueDateSM" => "",
                    "dueTimeSM" => "",
                    "notificationDate" => "",
                    "note" => "",
                    "receiveDocTag" => false,
                    "receiveDocDetail" => "",
                    "sentBill" => false,
                    "sentReq" => false,
                    "sentAct" => false,
                    "sentDepartment" => false,
                    "receiveFirstInstallmentTag" => false,
                    "receiveFirstInstallmentDetail" => "",
                    "receiveLastInstallmentTag" => false,
                    "receiveLastInstallmentDetail" => "",
                    "photographTag" => false,
                    "photographDetail" => "",
                    "completeSentMessengerFlag" => false,
                ));
            }
        }

        if(!empty($returnData['result']['orderList'][0]['isRecent'])){
            $returnData['result']['orderStatus'] = $returnData['result']['orderList'][0]['status']; 
            $returnData['result']['contractDate'] = $returnData['result']['orderList'][0]['contractDate'];
            $returnData['result']["freeCarAct"] = $returnData['result']['orderList'][0]['isFreeCompulsory'];
            $returnData['result']['orderId'] = $returnData['result']['orderList'][0]['orderId'];
            $returnData['result']['workType'] = (!empty($returnData['result']['orderList'][0]['orderTypeId'])) ? $returnData['result']['orderList'][0]['orderTypeId'] : 1;
            $returnData['result']['totalPaymentInsure'] = $returnData['result']['orderList'][0]['totalPrice'];
            $returnData['result']['totalDiscount'] = $returnData['result']['orderList'][0]['totalDiscount'];
            $returnData['result']['totalGiftFree'] = $returnData['result']['orderList'][0]['totalPromotionPrice'];
            $returnData['result']['campaignSpecialInsure'] = $returnData['result']['orderList'][0]['promotion'];
            $returnData['result']['agentDiscount'] = $returnData['result']['orderList'][0]['agentDiscount'];
            $returnData['result']['contactTypeId'] = $returnData['result']['orderList'][0]['contactTypeId'];
            $returnData['result']['paidPeriod'] = $returnData['result']['orderList'][0]['paidPeriod'];
            
            if(isset($returnData['result']['orderList'][0]['orderItemList'])){
                foreach($returnData['result']['orderList'][0]['orderItemList'] as $productKey => $productValue){
                    switch($productValue['isCompulsory']){
                        case 0:
                            $returnData['result']['insureId'] = $productValue['itemId'];
                            $returnData['result']['insureNo'] = $productValue['refCode'];
                            $returnData['result']['postNo'] = $productValue['emsCode'];
                            $returnData['result']['insureType'] = $productValue['prodTypeId'];
                            $returnData['result']['partnerType'] = $productValue['partnerId'];
                            $returnData['result']['insureRate'] = $productValue['capital'];
                            $returnData['result']['repairType'] = $productValue['repairTypeId'];
                            $returnData['result']['receiveDate'] = $productValue['startDate'];
                            $returnData['result']['receiveExpireDate'] = $productValue['endDate'];

                            $returnData['result']['tempDisabilityDriver'] = $productValue['protectionDetail']['tempDisabilityDriver'];
                            $returnData['result']['tempDisabilityPassenger'] = $productValue['protectionDetail']['tempDisabilityPassenger'];
                            $returnData['result']['bailDriver'] = $productValue['protectionDetail']['bailDriver'];
                            $returnData['result']['vehDisasterPerTime'] = $productValue['protectionDetail']['vehDisasterPerTime'];
                            $returnData['result']['tempDisabilityPassengerPerson'] = $productValue['protectionDetail']['tempDisabilityPassengerPerson'];
                            $returnData['result']['extLifeDamagePerPerson'] = $productValue['protectionDetail']['extLifeDamagePerPerson'];
                            $returnData['result']['permDisabilityPassengerPerson'] = $productValue['protectionDetail']['permDisabilityPassengerPerson'];
                            $returnData['result']['extAssetDamagePerTime'] = $productValue['protectionDetail']['extAssetDamagePerTime'];
                            $returnData['result']['treatmentDriver'] = $productValue['protectionDetail']['treatmentDriver'];
                            $returnData['result']['terrorismProtect'] = $productValue['protectionDetail']['terrorismProtect'];
                            $returnData['result']['vehDamagePerTime'] = $productValue['protectionDetail']['vehDamagePerTime'];
                            $returnData['result']['extExcessPerTime'] = $productValue['protectionDetail']['extExcessPerTime'];
                            $returnData['result']['extLifeDamagePerTime'] = $productValue['protectionDetail']['extLifeDamagePerTime'];
                            $returnData['result']['treatmentPassengerPerson'] = $productValue['protectionDetail']['treatmentPassengerPerson'];
                            $returnData['result']['permDisabilityPassenger'] = $productValue['protectionDetail']['permDisabilityPassenger'];
                            $returnData['result']['vehLostPerTime'] = $productValue['protectionDetail']['vehLostPerTime'];
                            $returnData['result']['permDisabilityDriver'] = $productValue['protectionDetail']['permDisabilityDriver'];
                            $returnData['result']['vehExcessPerTime'] = $productValue['protectionDetail']['vehExcessPerTime'];
                            $returnData['result']['treatmentPassenger'] = $productValue['protectionDetail']['treatmentPassenger'];
                            $returnData['result']['itemProtectId'] = $productValue['protectionDetail']['itemProtectId'];

                            $returnData['result']['changeOfInsure'] = $productValue['netPrice'];
                            $returnData['result']['dutyInsure'] = $productValue['tax'];
                            $returnData['result']['taxInsure'] = $productValue['vat'];
                            $returnData['result']['totalInsure'] = $productValue['price'];
                            $returnData['result']['beneficiary'] = (ISSET($productValue['beneficiaryList']))?1:0;
                            if(ISSET($productValue['beneficiaryList'])){
                                $returnData['result']['beneficiary_name']  = $productValue['beneficiaryList'][0]['beneName'];
                                $returnData['result']['beneficiary_id']  = $productValue['beneficiaryList'][0]['beneId'];
                            }
                            if(count($productValue['driverList']) > 0){
                                $returnData['result']['driverRecrive'] = 1;
                                $returnData['result']['driverList'] = $productValue['driverList'];
                                if(empty($returnData['result']['driverList'][1])){
                                    $returnData['result']['driverList'][1] = array(
                                        'driverName' => '',    
                                        'birthDate' => '',    
                                        'occupation' => '',    
                                        'driveLicenseNo' => '',    
                                        'driveExpiryDate' => '',    
                                        'driveProvId' => 0,
                                        'driverId' => -1    
                                    ); 
                                }
                            }
                            break;
                        case 1:
                            $returnData['result']['actPostcode'] = $productValue['emsCode'];
                            $returnData['result']['actType'] = $productValue['prodTypeId'];
                            $returnData['result']['actPartnerType'] = $productValue['partnerId'];
                            $returnData['result']['actReceiveDate'] = $productValue['startDate'];
                            $returnData['result']['actReceiveExpireDate'] = $productValue['endDate'];

                            $returnData['result']['changeOfCaract'] = $productValue['netPrice'];
                            $returnData['result']['dutyCaract'] = $productValue['tax'];
                            $returnData['result']['taxCaract'] = $productValue['vat'];
                            $returnData['result']['totalCaract'] = $productValue['price'];
                            $returnData['result']['caractId'] = $productValue['itemId'];
                            break;
                    }
                }
            }
                    
            if(isset($returnData['result']['orderList'][0]['paymentList'])){
                $returnData['result']['payType'] = $returnData['result']['orderList'][0]['paymentList'][0]['payTypeId'];                
                $returnData['result']['select_paydown'] = $returnData['result']['orderList'][0]['totalPeriod'];
                $returnData['result']['paychannel'] = $returnData['result']['orderList'][0]['paymentList'][0]['payChannelId'];
                
                $count = 0;
                foreach($returnData['result']['orderList'][0]['paymentList'] as $value){
                    $count++;
                    $returnData['result']['paymentList'][$count] = array(
                        'amount' => $value['amount'],
                        'payId' => $value['payId'],
                        'status' => $value['status'],
                        'dueDate' => $value['dueDate'],    
                    );
                }
            }    
        }
               
        // OptionList 
        $selectOptionList = Api::request("getCustomerDetailDropdowns", array(
            "agentId" => intval(session('userId')),
        ));
        $selectOptionList = !empty($selectOptionList['result']) ? $selectOptionList['result'] : array();
        
        $customerStatus = Api::request("listCustomerStatus", array(
            "agentLevelId" => intval(session('userlevelId')),
            "actionTypeId" => intval(1),
        ));
        $listSaleAgents = Api::request("listSaleAgents");
        $listMessengerType = Api::request("getMessengerType");
        
        $param = array(
            "isWalkin" => null,
        );
        $listSources = Api::request("listSources",$param);
        
        $ArrayHelper = new ArrayHelper();
        $selectOptionList['customerStatus'] = (!empty($customerStatus['result'])) ? $customerStatus['result']['customerStatus'] : array();
        $selectOptionList['listMessengerType'] = (!empty($listMessengerType['result'])) ? $listMessengerType['result']['messengerTypeList'] : array();
        $selectOptionList['listSaleAgents'] = (!empty($listSaleAgents['result'])) ? $listSaleAgents['result']['agents'] : array();
        $selectOptionList['listSources'] = (!empty($listSources['result'])) ? $listSources['result']['sources'] : array();
        $selectOptionList['listManu'] = $manuData;
        $selectOptionList['listModel'] = $modelData;

        return view('customer.detail', [ "data" => $returnData['result'], "selectOptionList" => $selectOptionList ]);
    }
    
    public function getNewPhoneList($id = 0){
        $id = empty(intval($id)) ? 0 : intval($id);
        return view('customer.phonelist', [ "id" => $id ]);
    }
    
    public function getNewLineList($id = 0){
        $id = empty(intval($id)) ? 0 : intval($id);
        return view('customer.linelist', [ "id" => $id ]);
    }
    
    public function getNewAddrList($id = 0){
        // OptionList 
        $selectOptionList = Api::request("getCustomerDetailDropdowns", array(
            "agentId" => intval(session('userId')),
        ));
        $selectOptionList = !empty($selectOptionList['result']) ? $selectOptionList['result'] : array();
        
        $id = empty(intval($id)) ? 0 : intval($id);
        return view('customer.addrlist', [ "id" => $id, "selectOptionList" => $selectOptionList ]);
    }
    
    //--------------------------REQUEST TO API------------------------------//
    
    public function getCustomerList(Request $request){
        $data = array(
            'updateAgentId' => intval(session('userId',0)),
            'vehId' => $request->has('vehId') ? intval($request->input('vehId')) : null,
            'custName' => $request->has('customername') ? $request->input('customername') : null,
            'saleAgentId' => $request->has('agent') ? intval($request->input('agent')) : null,
            'phoneNo' => $request->has('phonenumber') ? $request->input('phonenumber') : null,
            'vehLicenseNo' => $request->has('licenseno') ? $request->input('licenseno') : null,
            'status' => $request->has('status') ? intval($request->input('status')) : null,
            'offset' => $request->has('start') ? intval($request->input('start')) : 0 ,
            'limit' => $request->has('length') ? intval($request->input('length')) : 0,
            'dueDatePaymentStart' => $request->has('dueDatePaymentStart') ? (strtotime($request->input('dueDatePaymentStart') . " 00:00:00")*1000) : null,
            'dueDatePaymentEnd' => $request->has('dueDatePaymentEnd') ? (strtotime($request->input('dueDatePaymentEnd') . " 00:00:00")*1000) : null,
        );
        $returnData = Api::request('searchCustomerList', $data);
        $responseData = array();
        if(!empty($returnData['result'])){
            $total = !empty($returnData['result']['total']) ? intval($returnData['result']['total']) : count($returnData['result']['customers']);
            $listData = $returnData['result']['customers'];
            $i = $request->has('start') ? intval($request->input('start')) : 0;
            if(!empty($listData)){
                foreach($listData as $key => $item){
                    $listData[$key]['order'] = $i+1;
                    $listData[$key]['dueDate'] = empty($listData[$key]['dueDate']) ? "" : date('Y-m-d H:i',strtotime($item['dueDate']));
                    $i++;       
                }
            }else{
                $listData = [];   
            }
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'recordsTotal' => $total,
                'recordsFiltered' => $total,
                'data' => $listData,
            );        
        }else{
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'error' => $returnData['error']['message'],
                'data' => [],
            );    
        }
        return response()->json($responseData);
    }
    
    public function requestLeadData(Request $request){
        if(!$request->has('agent')){
            return response()->json(array(
                "id" => null,
                "error" => [
                    "code" => 422,
                    "message" => "Input Invalid : 'agent' not found.",
                ],
                "jsonrpc" => "2.0"
            ));    
        }
        
        $params = array(
            'agentId' => intval($request->input('agent')),
        );
        $returnData = Api::request("requestNewCustomerForLeadSource",$params);
        return response()->json($returnData);   
    }
    
    public function update($id = null, Request $request){
        if(empty($id))
            $id = -1;
        else if(intval($id)<=0)
            $id = -1;
        
        $validateList = [
            'saleAgentId' => 'required|integer',
            'custId' => 'required|integer',
            'vehId' => 'required|integer',
            'custName' => 'required',
            'vehTypeId' => 'required|integer',
            'manuId' => 'required|integer',
            'modelId' => 'required|integer',
            'vehLicenseNo' => 'required',
            'vehProvId' => 'required|integer',
            'hasCctv' => 'required|boolean',
            'hasAccessory' => 'required|boolean',
            'status' => 'required|integer',
            'phoneList' => 'array',
            'phoneListSelect' => 'integer',
            'addressList' => 'array',
            'addressListSelect' => 'integer',
            'workType' => 'required|integer',
            'contactTypeId' => 'integer',
            'orderStatus' => 'required|integer',
            'driverList' => 'required|array', 
            'paymentList' => 'required|array', 
            'payType' => 'required|integer', 
            'selectPaydown' => 'required|integer', 
        ];
        if($id <= 0){
            $validateList['sourceId'] = 'required|integer';
        }
        $validator = Validator::make($request->all(), $validateList);

        if ($validator->fails()) {
            return response()->json(array(
                "id" => null,
                "error" => [
                    "code" => 422,
                    "message" => $validator->errors(),
                ],
                "jsonrpc" => "2.0"
            ));        
        }
        
        $manuData = Api::request('getManufacturerById', array( 'manuId' => intval($request->input('manuId'))));
        $manuName = empty($manuData['result']['manuName']) ? null : $manuData['result']['manuName'];
        
        $modelData = Api::request('getModelById', array(
            'modelId' => intval($request->input('modelId')),
        ));
        $modelName = empty($modelData['result']['modelName']) ? null : $modelData['result']['modelName'];
        
        $dueDate = null;
        if($request->has('dueDate') && $request->has('dueDateHours') && $request->has('dueDateMinutes')){
            if(strlen($request->input('dueDate')) > 0 ){
                $changeDate = date('Y-m-d',strtotime($request->input('dueDate')));
                $changeHour = (intval($request->input('dueDateHours'))>9) ? $request->input('dueDateHours') : "0" . $request->input('dueDateHours');
                $changeMinus = (intval($request->input('dueDateMinutes'))>9) ? $request->input('dueDateMinutes') : "0" . $request->input('dueDateMinutes');
                $dueDate = $changeDate." ". (($changeHour == 0)?"00":$changeHour).":".(($changeMinus == 0)?"00":$changeMinus).":00";
            }
        }
        
        $phoneList = array();
        if($request->has('phoneList')){
            $phoneListAll = $request->input('phoneList');
            foreach($phoneListAll as $key => $item){
                if($request->input('phoneListSelect') == $key){
                    $item['isDefault']  = 1;   
                }else{
                    $item['isDefault']  = 0;
                }
                
                if($item["id"] <= 0){
                    $item['id']  = null;    
                }else{
                    $item['id']  = intval($item['id']);
                }
                $phoneList[] = $item;    
            }    
        }
        
        $lineList = array();
        if($request->has('lineList')){
            $lineListAll = $request->input('lineList');
            foreach($lineListAll as $key => $item){
                if($request->input('lineListSelect') == $key){
                    $item['isDefault']  = 1;   
                }else{
                    $item['isDefault']  = 0;
                }
                
                if($item["id"] <= 0){
                    $item['id']  = null;    
                }else{
                    $item['id']  = intval($item['id']);
                }
                $item['contactName'] = empty($item['contactName'])  ? "" : $item['contactName'];
                $item['lineId'] = empty($item['lineId'])  ? "" : $item['lineId'];
                $item['lineName'] = empty($item['lineName'])  ? "" : $item['lineName'];
                $lineList[] = $item;    
            }    
        }

        
        $addressList = array();
        if($request->has('addressList')){
            $addressListAll = $request->input('addressList');
            foreach($addressListAll as $key => $item){
                if($request->input('addressListSelect') == $key){
                    $item['isDefault']  = 1;   
                }else{
                    $item['isDefault']  = 0;
                }
                
                if($item["addrId"] <= 0){
                    $item['addrId']  = null;    
                }else{
                    $item["addrId"] = intval($item["addrId"]);
                }
                $item["addrTypeId"] = intval($item["addrTypeId"]);
                $item["recipientName"] = null;
                $item["phoneNo"] = null;
                $addressList[] = $item;    
            }    
        }
        
        $protectionDetail = array(
            "itemProtectId" => $request->has('itemProtectId') ? intval($request->input('itemProtectId')) : null,
            "extLifeDamagePerPerson" => $request->has('extLifeDamagePerPerson') ? floatval($request->input('extLifeDamagePerPerson')) : null,
            "extLifeDamagePerTime" => $request->has('extLifeDamagePerTime') ? floatval($request->input('extLifeDamagePerTime')) : null,
            "extAssetDamagePerTime" => $request->has('extAssetDamagePerTime') ? floatval($request->input('extAssetDamagePerTime')) : null,
            "extExcessPerTime" => $request->has('extExcessPerTime') ? floatval($request->input('extExcessPerTime')) : null,
            "vehDamagePerTime" => $request->has('vehDamagePerTime') ? floatval($request->input('vehDamagePerTime')) : null,
            "vehLostPerTime" => $request->has('vehLostPerTime') ? floatval($request->input('vehLostPerTime')) : null,
            "vehDisasterPerTime" => $request->has('vehDisasterPerTime') ? floatval($request->input('vehDisasterPerTime')) : null,
            "vehExcessPerTime" => $request->has('vehExcessPerTime') ? floatval($request->input('vehExcessPerTime')) : null,
            "permDisabilityDriver" => $request->has('permDisabilityDriver') ? floatval($request->input('permDisabilityDriver')) : null,
            "permDisabilityPassenger" => $request->has('permDisabilityPassenger') ? floatval($request->input('permDisabilityPassenger')) : null,
            "permDisabilityPassengerPerson" => $request->has('permDisabilityPassengerPerson') ? intval($request->input('permDisabilityPassengerPerson')) : null,
            "tempDisabilityDriver" => $request->has('tempDisabilityDriver') ? floatval($request->input('tempDisabilityDriver')) : null,
            "tempDisabilityPassenger" => $request->has('tempDisabilityPassenger') ? floatval($request->input('tempDisabilityPassenger')) : null,
            "tempDisabilityPassengerPerson" => $request->has('tempDisabilityPassengerPerson') ? intval($request->input('tempDisabilityPassengerPerson')) : null,
            "treatmentDriver" => $request->has('treatmentDriver') ? floatval($request->input('treatmentDriver')) : null,
            "treatmentPassenger" => $request->has('treatmentPassenger') ? floatval($request->input('treatmentPassenger')) : null,
            "treatmentPassengerPerson" => $request->has('treatmentPassengerPerson') ? intval($request->input('treatmentPassengerPerson')) : null,
            "bailDriver" => $request->has('bailDriver') ? floatval($request->input('bailDriver')) : null,
            "terrorismProtect" => $request->has('terrorismProtect') ? intval($request->input('terrorismProtect')) : null,
        );
        
        $beneficiaryList = null;
        if($request->has('beneficiary_name')){
            $beneficiaryList = array();
            $beneficiaryList[] = array(
                "beneId" => $request->has('beneficiary_id') ? intval($request->input('beneficiary_id')) : null,
                "beneName" => $request->has('beneficiary_name') ? $request->input('beneficiary_name') : null,
                "phoneNo" => null,
            );    
        }
        
        $driverList = array();
        $driverListAll = $request->input('driverList');
        foreach($driverListAll as $key => $item){
            if(!empty($item['driverName'])){
                unset($item['driveProvName']);
                $item['driveProvId'] = !empty($item['driveProvId']) ? intval($item['driveProvId']) : null;
                $item['driverId'] = !empty($item['driverId']) ? intval($item['driverId']) : null;
                $driverList[] = $item; 
            }         
        } 
        
        $orderItemList = array();
        // isCompulsory = 0
        $orderItemList[] = array(
            "itemId" => $request->has('insureId') ? intval($request->input('insureId')) : null,
            "partnerId" => intval($request->input('partnerType')),
            "prodTypeId" => intval($request->input('insureType')),
            "repairTypeId" => intval($request->input('repairType')),
            "refCode" => $request->input('insureNo'),
            "emsCode" => $request->has('postNo') ? $request->input('postNo') : null,
            "startDate" => $request->has('receiveDate') ? $request->input('receiveDate') : null,
            "endDate" => $request->has('receiveExpireDate') ? $request->input('receiveExpireDate') : null,
            "capital" => $request->has('insureRate') ? floatval($request->input('insureRate')) : 0.00,
            "price" => $request->has('totalInsure') ? floatval($request->input('totalInsure')) : 0.00,
            "discount" => 0,
            "netPrice" => $request->has('changeOfInsure') ? floatval($request->input('changeOfInsure')) : 0.00,
            "tax" => $request->has('dutyInsure') ? floatval($request->input('dutyInsure')) : 0.00,
            "vat" => $request->has('taxInsure') ? floatval($request->input('taxInsure')) : 0.00,
            "isCompulsory" => 0,
            "protectionDetail" => $protectionDetail,
            "beneficiaryList" => $beneficiaryList,
            "driverList" => $driverList,
        );
        
        // isCompulsory = 1
        $orderItemList[] = array(
            "itemId" => $request->has('caractId') ? intval($request->input('caractId')) : null,
            "partnerId" => intval($request->input('actPartnerType')) ? intval($request->input('actPartnerType')) : null,
            "prodTypeId" => intval($request->input('actType')),
            "repairTypeId" => 0,
            "refCode" => null,
            "emsCode" => $request->has('actPostcode') ? $request->input('actPostcode') : null,
            "startDate" => $request->has('actReceiveDate') ? $request->input('actReceiveDate') : null,
            "endDate" => $request->has('actReceiveExpireDate') ? $request->input('actReceiveExpireDate') : null,
            "capital" => null,
            "price" => $request->has('totalCaract') ? floatval($request->input('totalCaract')) : 0.00,
            "discount" => 0,
            "netPrice" => $request->has('changeOfCaract') ? floatval($request->input('changeOfCaract')) : 0.00,
            "tax" => $request->has('dutyCaract') ? floatval($request->input('dutyCaract')) : 0.00,
            "vat" => $request->has('taxCaract') ? floatval($request->input('taxCaract')) : 0.00,
            "isCompulsory" => 1,
        );
        
        $paymentListAll = $request->input('paymentList');
        $paymentList = array();
        $loop = 1;
        if(intval($request->input('payType'))==2){
            $loop = intval($request->input('selectPaydown'));
        }
        
        for($i=0;$i<$loop;$i++){
            $paymentList[$i] = $paymentListAll[$i+1];
            $paymentList[$i]['payId'] = empty($paymentList[$i]['payId']) ? null : intval($paymentList[$i]['payId']);
            $paymentList[$i]['payTypeId'] = intval($request->input('payType'));
            $paymentList[$i]['payChannelId'] = intval($request->input('payChannelId'));
            $paymentList[$i]['amount'] = floatval($paymentList[$i]['amount']);
            $paymentList[$i]['status'] = intval($paymentList[$i]['status']);
        }
        
        $orderList = array();
        $orderList[0] = array(
            "orderId" => $request->has('orderId') ? intval($request->input('orderId')) : null,
            "orderTypeId" => intval($request->input('workType')),
            "contactTypeId" => intval($request->input('contactTypeId')),
            "payTypeId" => intval($request->input('payType')),
            "ncbDiscount" => 0,
            "fleetDiscount" => 0,
            "agentDiscount" => $request->has('agentDiscount') ? floatval($request->input('agentDiscount')) : 0.00,
            "otherDiscount" => 0,
            "accessoryPrice" => 0,
            "totalDiscount" => 0,
            "totalPromotionPrice" => $request->has('totalGiftFree') ? floatval($request->input('totalGiftFree')) : 0.00,
            "totalPrice" => $request->has('totalPaymentInsure') ? floatval($request->input('totalPaymentInsure')) : 0.00,
            "promotion" => $request->has('campaignSpecialInsure') ? $request->input('campaignSpecialInsure') : null,
            "contractDate" => $request->has('contractDate') ? $request->input('contractDate') : null,
            "isFreeCompulsory" => $request->has('freeCarAct') ? intval($request->input('freeCarAct')) : 0,
            "status" => intval($request->input('orderStatus')),
            
            "orderDocumentList" => null,   
            "orderItemList" => $orderItemList,   
            "paymentList" => $paymentList,   
            "paymentDocumentList" => null,
            
            "totalNetPrice" => 0, //"notReady"
            "totalVat" => 0, //"notReady"
            "totalTax" => 0, //"notReady"
        );
        
        $messengerObj = array();
        $messengerObjAll = $request->input('messengerObj');
        if(is_array($messengerObjAll)){
            foreach($messengerObjAll as $key => $item){
                $sentOtherItemList = array();
                if(empty($messengerObjAll['sentOtherItemList'])){
                    $sentOtherItemList = null;            
                }else if(!is_array($messengerObjAll['sentOtherItemList'])){
                    $sentOtherItemList = null;    
                }else{
                    foreach($messengerObjAll['sentOtherItemList'] as $key2 => $item2){    
                        $sentOtherItemList[] = array(
                            "sentOtherItemId" => empty($item2['sentOtherItemId']) ? null : intval($item2['sentOtherItemId']),
                            "sentOtherDetail" => ($item2['sentOtherDetail']) ? null :$item2['sentOtherDetail'],
                            "sentOtherEms" => ""
                        );    
                    }
                }
                $messengerObj[] = array(
                    "messengerId" => empty($item['messengerId']) ? null : intval($item['messengerId']),
                    "description" => empty($item['description']) ? null : $item['description'],
                    "campaign" => empty($item['campaign']) ? false : boolval($item['campaign']),
                    "campaignDetail" => empty($item['campaignDetail']) ? "" : $item['campaignDetail'],
                    "messengerTypeId" => empty($item['messengerTypeId']) ? 0 : intval($item['messengerTypeId']),
                    "sentReqEms" => empty($item['sentReqEms']) ? "" : $item['sentReqEms'],
                    "sentReqDate" => empty($item['sentReqDate']) ? null : (strtotime( $item['sentReqDate'] . " 00:00:00")*1000),
                    "sentReqTag" => empty($item['sentReqTag']) ? false : boolval($item['sentReqTag']),
                    "sentReceiptTag" => empty($item['sentReceiptTag']) ? false : boolval($item['sentReceiptTag']),
                    //"messengerStatusFinish" => true,
                    "sentActTag" => empty($item['sentActTag']) ? false : boolval($item['sentActTag']),
                    "sentDepartmentTag" => empty($item['sentDepartmentTag']) ? false : boolval($item['sentDepartmentTag']),
                    "sentEndorseTag" => empty($item['sentEndorseTag']) ? false : boolval($item['sentEndorseTag']),
                    "sentRenewTag" => empty($item['sentRenewTag']) ? false : boolval($item['sentRenewTag']),
                    "sentFormCutCardTag" => empty($item['sentFormCutCardTag']) ? false : boolval($item['sentFormCutCardTag']),
                    "sentOtherItemList" => $sentOtherItemList 
                );  
            }        
        }else{
            $messengerObj = null;    
        }
        
        $sentMessengerObj = array();
        $sentMessengerObjAll = $request->input('sentMessengerObj');
        if(is_array($sentMessengerObjAll)){
            foreach($sentMessengerObjAll as $key => $item){
                $dueDate = null;
                if( !empty($item['dueDateSM']) && !empty($item['dueTimeSM']) ){
                    $dueDate = $item['dueDateSM'] . " " . $item['dueTimeSM'] . ":00";
                }else if(!empty($item['dueDateSM'])){
                    $dueDate = $item['dueDateSM'] . " 00:00:00";
                }
                $sentMessengerObj[] = array(
                    "sentMessengerId" => empty($item['sentMessengerId']) ? null : intval($item['sentMessengerId']),
                    "detail" => empty($item['detail']) ? null : $item['detail'],
                    "dueDate" => empty($dueDate) ? null : (strtotime($dueDate)*1000),
                    "note" => empty($item['note']) ? null : $item['note'],
                    "receiveDocTag" => empty($item['receiveDocTag']) ? false : boolval($item['receiveDocTag']),
                    "receiveDocDetail" => empty($item['receiveDocDetail']) ? null : $item['receiveDocDetail'],
                    "sentBill" => empty($item['sentBill']) ? false : boolval($item['sentBill']),
                    "sentReq" => empty($item['sentReq']) ? false : boolval($item['sentReq']),
                    "sentAct" => empty($item['sentAct']) ? false : boolval($item['sentAct']),
                    "sentDepartment" => empty($item['sentDepartment']) ? false : boolval($item['sentDepartment']),
                    "receiveFirstInstallmentTag" => empty($item['receiveFirstInstallmentTag']) ? false : boolval($item['receiveFirstInstallmentTag']),
                    "receiveFirstInstallmentDetail" => empty($item['receiveFirstInstallmentDetail']) ? null : $item['receiveFirstInstallmentDetail'],
                    "receiveLastInstallmentTag" => empty($item['receiveLastInstallmentTag']) ? false : boolval($item['receiveLastInstallmentTag']),
                    "receiveLastInstallmentDetail" => empty($item['receiveLastInstallmentDetail']) ? null : $item['receiveLastInstallmentDetail'],
                    "photographTag" => empty($item['photographTag']) ? false : boolval($item['photographTag']),
                    "photographDetail" => empty($item['photographDetail']) ? null : $item['photographDetail'],
                    "completeSentMessengerFlag" => empty($item['completeSentMessengerFlag']) ? false : boolval($item['completeSentMessengerFlag']),  
                );  
            }        
        }else{
            $sentMessengerObj = null;    
        }
        
        $dataUpdate = array(
            'saleAgentId' => intval($request->input('saleAgentId')),
            'custId' => ($id <= 0) ? null : intval($request->input('custId')),
            'custName' => $request->input('custName'),
            'birthDate' => $request->has('birthDate') ? $request->input('birthDate') : null,
            'occupation' => $request->has('occupation') ? $request->input('occupation') : null,
            'idCardNo' => $request->has('idCardNumber') ? $request->input('idCardNumber') : null,
            'fbId' => $request->has('fbId') ? $request->input('fbId') : null,
            'email' => $request->has('email') ? $request->input('email') : null,
            'vehId' => ($id <= 0) ? null : intval($id),
            'vehTypeId' => intval($request->input('vehTypeId')),
            'manuName' => $manuName,//$request->input('manuName'),
            'modelName' => $modelName,//$request->input('modelName'),
            'modelYear' => $request->has('modelYear') ? intval($request->input('modelYear')) : null,
            'vehLicenseNo' => $request->input('vehLicenseNo'),
            'vehProvId' => empty(intval($request->input('vehProvId'))) ? null : intval($request->input('vehProvId')),
            'chasisNo' => $request->has('chasisNo') ? $request->input('chasisNo') : null,
            'engineNo' => $request->has('engineNo') ? $request->input('engineNo') : null,
            'engineCapacity' => $request->has('engineCapacity') ? intval($request->input('engineCapacity')) : null,
            'bodyType' => $request->has('bodyType') ? $request->input('bodyType') : null,
            'usageType' => $request->has('usageType') ? $request->input('usageType') : null,
            'passenger' => $request->has('passenger') ? intval($request->input('passenger')) : null,
            'hasCctv' => intval($request->input('hasCctv')),
            'buyDate' => $request->has('buyDate') ? $request->input('buyDate') : null,
            'taxDate' => $request->has('taxDate') ? $request->input('taxDate') : null,
            'lastExpireDate' => $request->has('lastExpireDate') ? $request->input('lastExpireDate') : null,
            'lastPartnerName' => $request->has('lastPartnerName') ? $request->input('lastPartnerName') : null,
            'hasAccessory' => $request->has('accessory') ? 1 : 0,
            'accessory' => $request->has('accessory') ? $request->input('accessory') : null,
            'accessoryPrice' => $request->has('accessoryPrice') ? intval($request->input('accessoryPrice')) : null,
            'staffNote' => $request->has('staffNote') ? $request->input('staffNote') : null,
            'status' => intval($request->input('status')) ,
            'dueDate' => $dueDate,
            'updateAgentId' => intval(session('userId')),
            'phoneList' => $phoneList,
            'lineList' => $lineList,
            'addressList' => $addressList,
            'custDocumentList' => null, //array(),
            'orderList' => $orderList,
            'messengerObj' => $messengerObj,
            'sentMessengerObj' => $sentMessengerObj,
        );
        
        //return response()->json($dataUpdate);
        
        if($id <= 0){
            $dataUpdate['saleAgentId'] = $request->has('saleAgentId') ? intval($request->input('saleAgentId')) : 0;
            $dataUpdate['sourceId'] = intval($request->input('sourceId'));
            unset($dataUpdate['vehId']);
            unset($dataUpdate['custId']);
            unset($dataUpdate['custDocumentList']);
            unset($dataUpdate['messengerObj']);
            unset($dataUpdate['sentMessengerObj']);
            $returnData = Api::request('createCustomerDetails', $dataUpdate);
        }else{
            $returnData = Api::request('saveCustomerDetails', $dataUpdate);    
        }
        return response()->json($returnData);
          
    }
    
    public function isduplicatechasisno(Request $request) {
        $chasisNo = $request->has('chasisNo') ? $request->input('chasisNo') : null;
        $params = array(
            "chasisNo" => $chasisNo
        );
        $returnData = Api::request("checkIsDuplicate", $params);
        if(!empty($returnData['result'])) {
            echo json_encode($returnData['result']);
        } else {
            $returnData['result']['isDuplicate'] = false;
            echo json_encode($returnData['result']);
        }

    }
}
