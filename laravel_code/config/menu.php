<?php

return [
    array(
        'displayname' => 'Car Management',
        'permissionLevel' => array(1, 3),
        'name' => 'car',
        'submenu' => [
            array(
                'displayname' => 'Brand List',
                'uri' => 'brand',
                'permissionLevel' => array(1, 3),
            ),
            array(
                'displayname' => 'Model List',
                'uri' => 'model',
                'permissionLevel' => array(1, 3),
            ),
            array(
                'displayname' => 'Replace Models Name',
                'uri' => 'model/mapping',
                'permissionLevel' => array(1, 3),
            ),
            array(
                'displayname' => 'Replace Provinces Name',
                'uri' => 'province/mapping',
                'permissionLevel' => array(1, 3),
            ),
        ]
    ),
    array(
        'displayname' => 'Customer Management',
        'permissionLevel' => array(1, 2, 3, 4, 5, 6),
        'name' => 'customer',
        'submenu' => [
            array(
                'displayname' => 'Import Customer',
                'uri' => 'import',
                'permissionLevel' => array(1, 3),
            ),
            array(
                'displayname' => 'Approve New Customer',
                'uri' => 'approve',
                'permissionLevel' => array(1, 3),
            ),
            array(
                'displayname' => 'Customer List',
                'uri' => 'customer',
                'permissionLevel' => array(1 ,2, 3, 4, 5, 6),
            ),
            array(
                'displayname' => 'Create a new customer',
                'uri' => 'customer/new',
                'permissionLevel' => array(1, 3),
            ),
            array(
                'displayname' => 'Source List',
                'uri' => 'source',
                'permissionLevel' => array(1, 3),
            ),
            array(
                'displayname' => 'Lead From Website',
                'uri' => 'lead',
                'permissionLevel' => array(1, 3),
            ),
        ]
    ),
    array(
        'displayname' => 'Messenger Management',
        'permissionLevel' => array(1,7),
        'name' => 'messenger',
        'submenu' => [
            array(
                'displayname' => 'Post List',
                'uri' => 'messenger/post',
                'activeURI' => [],
                'permissionLevel' => array(1,7),
            ),
            array(
                'displayname' => 'Messenger List',
                'uri' => 'messenger/list',
                'activeURI' => [],
                'permissionLevel' => array(1),
            ),
        ]
    ),
    array(
        'displayname' => 'User Management',
        'permissionLevel' => array(1),
        'name' => 'user',
        'submenu' => [
            array(
                'displayname' => 'User List',
                'uri' => 'user',
                'activeURI' => [],
                'permissionLevel' => array(1),
            ),
        ]
    ),
    
    
];
