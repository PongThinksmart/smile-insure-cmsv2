@extends('dashboard.dashboard', [
    'activeMenu' => 'customer',
    'breadcrumb' => [
        array(
            'name' => 'Customer Management',
            'uri' => ''   
        ),
        array(
            'name' => 'Customer List',
            'uri' => 'customer' 
        ),
        array(
            'name' => ( $data['vehId'] >= 0 ) ? 'Edit a Customer' : 'Create a new Customer',
            'uri' =>  ( $data['vehId'] >= 0 ) ? 'customer/' . $data['vehId'] : 'customer/new',
        ),
        array(
            'name' => 'Quotation',
            'uri' => '' 
        )
    ]

] )
@section('css')

@endsection

@section('content')
<form action="{{ url('pdf/quotation') }}" method="post" target="_blank">
{{ csrf_field() }}
<div id="form" style="background: url('{{ url("img/smileinsure_quotation.jpg") }}');background-size: 940px 1453px;height:1453px;width:940px;margin: auto;">
    <input type="hidden" name="vehId" value="{{$data['vehId']}}">
    <input type="hidden" name="quotId" value="{{$data['quotId']}}">
    <input type="text" name="refNo" value="{{$data['refNo']}}" class="form-control" style="width: 150px;position: absolute;margin-left: 746px;margin-top: 38px;height: 1.5rem;">
    <input type="text" name="saleAgentName" value="{{$data['proposedAgentName']}}" class="form-control" style="width: 150px;position: absolute;margin-left: 746px;margin-top: 66px;height: 1.5rem;" readonly>
    <input type="text" name="custName" value="{{$data['custName']}}" class="form-control" style="width: 336px;position: absolute;margin-left: 99px;margin-top: 212px;height: 1.5rem;" readonly>
    <input type="text" name="carInfo" value="{{$data['manuName']}} / {{$data['modelName']}} / {{$data['engineCapacity']}}" class="form-control" style="width: 287px;position: absolute;margin-left: 148px;margin-top: 237px;height: 1.5rem;" readonly>
    <input type="text" name="vehLicenseNo" value="{{$data['vehLicenseNo']}} {{$data['vehProvName']}}" class="form-control" style="width: 309px;position: absolute;margin-left: 126px;margin-top: 262px;height: 1.5rem;" readonly>
    <input type="text" name="phoneNo" value="{{$data['phoneNo']}}" class="form-control" style="width: 309px;position: absolute;margin-left: 126px;margin-top: 287px;height: 1.5rem;" readonly>
    <input class="form-check-input" name="isSpecifyDriver" type="radio" value="0" style="position: absolute;margin-left: 456px;margin-top: 218px;" {{ !$data['isSpecifyDriver'] ? "checked" : "" }}>
    <input class="form-check-input" name="isSpecifyDriver" type="radio" value="1" style="position: absolute;margin-left: 561px;margin-top: 218px;" {{ $data['isSpecifyDriver'] ? "checked" : "" }}>
    @for( $i=0 ; $i<2 ; $i++ )
    <div style="position: absolute;margin-left: 626px;margin-top: {{ 210+($i*25) }}px;">
        <input type="hidden" name="driverList[{{$i}}][id]" value="{{$data['driverList'][$i]['id']}}">
        <input type="text" name="driverList[{{$i}}][driverName]" value="{{$data['driverList'][$i]['driverName']}}" class="form-control" style="width: 140px;height: 1.5rem;position: absolute;">
        <input type="text" name="driverList[{{$i}}][age]" value="{{$data['driverList'][$i]['age']}}" class="form-control" style="padding-right: 0px;width: 36px;height: 1.5rem;position: absolute;margin-left: 172px;">
    </div>
    @endfor
    <input data-provide="datepicker" id="proposedDate" name="proposedDate" type="text" class="form-control" style="width: 126px;height: 1.5rem;position: absolute;margin-left: 778px;margin-top: 285px;" value="{{$data['proposedDate']}}">
    @for( $i=0 ; $i<3 ; $i++ )
    <div style="position: absolute;margin-left: {{409+($i*136)}}px;margin-top: 386px;">
        <input type="hidden" id="quotItemId_{{$i}}" name="itemList[{{$i}}][quotItemId]" value="{{$data['itemList'][$i]['quotItemId']}}">
        <input type="text" id="quotItemName_{{$i}}" name="itemList[{{$i}}][quotItemName]" value="{{$data['itemList'][$i]['quotItemName']}}" class="form-control" style="width: 130px;height: 1.5rem;position: absolute;">
        <input type="text" id="extLifeDamagePerPerson_{{$i}}" name="itemList[{{$i}}][extLifeDamagePerPerson]" value="{{$data['itemList'][$i]['extLifeDamagePerPerson']}}" class="form-control number2Digit" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 54px;">
        <input type="text" id="extLifeDamagePerTime_{{$i}}" name="itemList[{{$i}}][extLifeDamagePerTime]" value="{{$data['itemList'][$i]['extLifeDamagePerTime']}}" class="form-control number2Digit" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 81px;">
        <input type="text" id="extAssetDamagePerTime_{{$i}}" name="itemList[{{$i}}][extAssetDamagePerTime]" value="{{$data['itemList'][$i]['extAssetDamagePerTime']}}" class="form-control number2Digit" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 108px;">
        <input type="text" id="vehDamagePerTime_{{$i}}" name="itemList[{{$i}}][vehDamagePerTime]" value="{{$data['itemList'][$i]['vehDamagePerTime']}}" class="form-control number2Digit" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 162px;">
        <input type="text" id="vehLostPerTime_{{$i}}" name="itemList[{{$i}}][vehLostPerTime]" value="{{$data['itemList'][$i]['vehLostPerTime']}}" class="form-control number2Digit" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 189px;">
        <input type="text" id="vehExcessPerTime_{{$i}}" name="itemList[{{$i}}][vehExcessPerTime]" value="{{$data['itemList'][$i]['vehExcessPerTime']}}" class="form-control number2Digit" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 216px;">
        <input type="text" id="permDisabilityPassengerPerson_{{$i}}" name="itemList[{{$i}}][permDisabilityPassengerPerson]" value="{{$data['itemList'][$i]['permDisabilityPassengerPerson']}}" class="form-control" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 243px;">
        <input type="text" id="permDisabilityPassenger_{{$i}}" name="itemList[{{$i}}][permDisabilityPassenger]" value="{{$data['itemList'][$i]['permDisabilityPassenger']}}" class="form-control number2Digit" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 270px;">
        <input type="text" id="treatmentPassenger_{{$i}}" name="itemList[{{$i}}][treatmentPassenger]" value="{{$data['itemList'][$i]['treatmentPassenger']}}" class="form-control number2Digit" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 297px;">
        <input type="text" id="bailDriver_{{$i}}" name="itemList[{{$i}}][bailDriver]" value="{{$data['itemList'][$i]['bailDriver']}}" class="form-control number2Digit" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 324px;">
        <input type="text" id="repairTypeName_{{$i}}" name="itemList[{{$i}}][repairTypeName]" value="{{$data['itemList'][$i]['repairTypeName']}}" class="form-control" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 351px;">
        <input type="text" id="promotionCustom1_{{$i}}" name="itemList[{{$i}}][promotionCustom1]" value="{{$data['itemList'][$i]['promotionCustom1']}}" class="form-control" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 403px;">
        <input type="text" id="promotionCustom2_{{$i}}" name="itemList[{{$i}}][promotionCustom2]" value="{{$data['itemList'][$i]['promotionCustom2']}}" class="form-control" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 429px;">
        <input type="text" id="promotionCustom3_{{$i}}" name="itemList[{{$i}}][promotionCustom3]" value="{{$data['itemList'][$i]['promotionCustom3']}}" class="form-control" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 455px;">
        <input type="text" id="voluntaryPrice_{{$i}}" name="itemList[{{$i}}][voluntaryPrice]" value="{{$data['itemList'][$i]['voluntaryPrice']}}" class="form-control number2Digit" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 525px;">
        <input type="text" id="compulsoryPrice_{{$i}}" name="itemList[{{$i}}][compulsoryPrice]" value="{{$data['itemList'][$i]['compulsoryPrice']}}" class="form-control number2Digit" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 552px;">
        <input type="text" id="totalPrice_{{$i}}" name="itemList[{{$i}}][totalPrice]" value="{{$data['itemList'][$i]['totalPrice']}}" class="form-control number2Digit" style="width: 130px;height: 1.5rem;position: absolute;margin-top: 583px;">
    </div>
    @endfor
    
    <div style="position: absolute;margin-left: 40px;margin-top: 788px;">
        <input type="text" name="promotionStaticRow1" value="{{$data['promotionStaticRow1']}}" class="form-control" style="width: 363px;height: 1.5rem;position: absolute;">
        <input type="text" name="promotionStaticRow2" value="{{$data['promotionStaticRow2']}}" class="form-control" style="width: 363px;height: 1.5rem;position: absolute;margin-top: 26px;">
        <input type="text" name="promotionStaticRow3" value="{{$data['promotionStaticRow3']}}" class="form-control" style="width: 363px;height: 1.5rem;position: absolute;margin-top: 52px;">
    </div>
</div>
<div class="text-center"><button type="submit" class="btn btn-success btn-lg active">Preview</button></div>
@endsection

@section('script')
<script type="text/javascript">
    autonumber_arr = new Array();
    $(document).ready(function() {
        $( ".number2Digit" ).each(function( index ) {
            var id = $(this).attr('id');
            autonumber_arr[id] = new AutoNumeric('#' + id, { 'unformatOnSubmit' : true, 'allowDecimalPadding' : 'true' , 'emptyInputBehavior' : 'zero' });    
        });
        
        $( "#proposedDate" ).datepicker({
            format: "dd-mm-yyyy"
        });
    });
</script>
@endsection

