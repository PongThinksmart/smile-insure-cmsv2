<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->middleware('auth');

Route::middleware(['auth'])->group(function () {
    
    Route::get('register', 'Auth\RegisterController@showRegistrationForm');
    
    Route::prefix('customer')->group(function () {
        Route::get('','CustomerController@index')->middleware('menu:1|2|3|4|5|6');
        Route::post('','CustomerController@update')->middleware('menu:1|3');
        
        Route::post('isDuplicateChasisNo','CustomerController@isduplicatechasisno');
        Route::get('getCustomerList','CustomerController@getCustomerList');
        Route::get('requestLeadData','CustomerController@requestLeadData')->middleware('menu:5');
        
        Route::get('new','CustomerController@new_detail')->middleware('menu:1|3');
        
        Route::get('getNewPhoneList/{id?}','CustomerController@getNewPhoneList');
        Route::get('getNewLineList/{id?}','CustomerController@getNewLineList');
        Route::get('getNewAddrList/{id?}','CustomerController@getNewAddrList');
        
        Route::put('{id}','CustomerController@update')->middleware('menu:1|2|3|4|5|6');
        Route::get('{id}','CustomerController@detail')->middleware('menu:1|2|3|4|5|6');
    });
    
    Route::prefix('quotation')->group(function () {
        Route::get('','QuotationController@index')->middleware('menu:1|6');
        Route::post('{id}','QuotationController@index')->middleware('menu:1|6');
    });
    
    Route::prefix('messenger/post')->group(function () {
        Route::get('','MessengerController@postIndex')->middleware('menu:1|7');
        Route::post('','MessengerController@postUpdate')->middleware('menu:1|7');
    });
    
    Route::prefix('messenger/list')->group(function () {
        Route::get('','MessengerController@listIndex')->middleware('menu:1');
    });
    
    Route::prefix('viriya')->group(function () {
        Route::post('','ViriyaController@index')->middleware('menu:1|6');
    });
    
    Route::prefix('pdf')->group(function () {
        Route::post('sqa','PdfController@sqa');
        Route::post('invoice','PdfController@invoice');
        Route::post('bill/{index?}','PdfController@bill');
        Route::post('receipt/{index?}','PdfController@receipt');
        Route::post('quotation','PdfController@quotation');
        Route::post('viriya','PdfController@viriya');
        Route::post('sentMessenger','PdfController@sentMessenger');
        //Route::get('sentMessenger','PdfController@sentMessenger');
    });
    
    Route::prefix('source')->group(function () {
        Route::get('','SourceController@index')->middleware('menu:1|3');
        Route::post('','SourceController@create')->middleware('menu:1|3');
        
        Route::get('getListSource','SourceController@getListSource')->middleware('menu:1|3');
        Route::get('new','SourceController@new_detail')->middleware('menu:1|3');
        
        Route::get('{id}','SourceController@detail')->middleware('menu:1|3');
        Route::put('{id}','SourceController@update')->middleware('menu:1|3');
            
    });
    
    Route::prefix('import')->group(function () {
        Route::get('','ImportCustomerController@index')->middleware('menu:1|3');
        
        Route::get('getListImport','ImportCustomerController@getListImport')->middleware('menu:1|3');
        Route::get('new','ImportCustomerController@new_detail')->middleware('menu:1|3');
        
        Route::post('convertData','ImportCustomerController@convertData')->middleware('menu:1|3');
        Route::post('importLot','ImportCustomerController@importLot')->middleware('menu:1|3');
        Route::post('importCustomer','ImportCustomerController@importCustomer')->middleware('menu:1|3');
    });
    
    Route::prefix('approve')->group(function () {
        Route::get('','ApproveController@index')->middleware('menu:1|3');
        Route::get('getCustomerList','ApproveController@getCustomerList')->middleware('menu:1|3');
        Route::put('update','ApproveController@update')->middleware('menu:1|3');
    });
    
    Route::prefix('lead')->group(function () {
        Route::get('','LeadController@index')->middleware('menu:1|3');
        Route::post('exportExcel','LeadController@exportExcel')->middleware('menu:1|3');
    });
    
    Route::prefix('brand')->group(function () {
        Route::get('','BrandController@index')->middleware('menu:1|3');
        Route::post('','BrandController@create')->middleware('menu:1|3');
        
        Route::get('getListBrand','BrandController@getListBrand');
        Route::get('new','BrandController@new_detail')->middleware('menu:1|3');
        
        Route::get('{id}','BrandController@detail')->middleware('menu:1|3');
        Route::put('{id}','BrandController@update')->middleware('menu:1|3');
    });
    
    Route::prefix('model')->group(function () {
        Route::get('','ModelController@index')->middleware('menu:1|3');
        Route::post('','ModelController@create')->middleware('menu:1|3');
        
        Route::get('getListModel','ModelController@getListModel');
        Route::get('getAllModel','ModelController@getAllModel');
        Route::get('getMismatch','ModelController@getMismatch');
        Route::get('new','ModelController@new_detail')->middleware('menu:1|3');
        Route::get('mapping','ModelController@mapping')->middleware('menu:1|3');
        Route::put('replace/{id}','ModelController@replace');
        
        Route::get('{id}','ModelController@detail')->middleware('menu:1|3');
        Route::put('{id}','ModelController@update')->middleware('menu:1|3');
        
    });
    
    Route::prefix('province')->group(function () {
        Route::get('mapping','ProvinceController@mapping')->middleware('menu:1|3');
        Route::get('getMismatch','ProvinceController@getMismatch')->middleware('menu:1|3');
        Route::put('replace/{id}','ProvinceController@replace')->middleware('menu:1|3');
    });
    
    Route::prefix('user')->group(function () {
        Route::get('','UserController@index')->middleware('menu:1');
        Route::post('','UserController@create')->middleware('menu:1');
        
        Route::get('new','UserController@new_detail')->middleware('menu:1');
        Route::get('getListUser','UserController@getListUser')->middleware('menu:1');
        Route::get('inactiveUser/{id}','UserController@inactiveUser')->middleware('menu:1');;
        
        Route::get('{id}','UserController@detail')->middleware('menu:1');
        Route::put('{id}','UserController@update')->middleware('menu:1');
    });
        
});

Route::get('', function () {
    return redirect('login');
});
