<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class MenuAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role = null)
    {   
        $arr_status = explode("|",$role);
        if(!in_array( session('userlevelId','0') . "" , $arr_status )){
            return response('Access Denide.', 502);    
        }
        return $next($request);
    }
}