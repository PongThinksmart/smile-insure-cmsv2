<?php
    
    function reMenu($listMenu, $activeMenu){
        $i = 1;
        foreach($listMenu as $item){
            if(empty($item['permissionLevel'])){
                continue;
            }
            if( in_array(intval(session('userlevelId',0)),$item['permissionLevel']) ){
                if(isset($item['submenu'])){
                    $idElement = !empty($item['name']) ? $item['name'] . 'SubMenu' : 'Submenu_' . $i;
                    $displayname = !empty($item['displayname']) ? $item['displayname'] : '';  
                    ?><li><a href="#{{ $idElement }}" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle">{{ $displayname }}</a><?php
                    ?><ul class="collapse list-unstyled show" id="{{ $idElement }}"><?php
                    reMenu($item['submenu'], $activeMenu);
                    ?></ul><?php
                    ?></li><?php  
                }else{
                    $displayname = !empty($item['displayname']) ? $item['displayname'] : '';
                    $url = !empty($item['uri']) ? url($item['uri']) : '';
                    $class = !empty($item['uri']) ? ( $item['uri']==$activeMenu ? 'active' : '') : ''; 
                    ?><a href="{{ $url }}" class="{{$class}}">{{ $displayname }}</a><?php
                }    
            }    
        }    
    }
    
    $activeMenu = !empty($activeMenu) ? $activeMenu : '';
    $menu = config('menu');
    reMenu($menu,$activeMenu);
    
?>