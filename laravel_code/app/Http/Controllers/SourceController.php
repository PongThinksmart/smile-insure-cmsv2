<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use App\Model\Api;

class SourceController extends Controller
{
    public function index(){
        return view('source.index', []);    
    }
    
    public function new_detail(Request $request){
        $optionData = Api::request("listSourceType");
        $selectOptionList['listSourceType'] = !empty($optionData['result']['sourceTypes']) ? $optionData['result']['sourceTypes'] : array();
        return view('source.detail', [ 'data' => [ 'sourceId' => -1 ], 'selectOptionList' => $selectOptionList ]);    
    }
    
    public function detail($id, Request $request){
        $params = array(
            'sourceId' => intval($id),
        );
        $returnData = Api::request('getSourceById', $params);
        if($returnData['result']==null){
            $sidebarAndBreadcrumb = [
                'activeMenu' => 'brand',
                'breadcrumb' => [
                    array(
                        'name' => 'Customer Management',
                        'uri' => ''   
                    ),
                    array(
                        'name' => 'Source List',
                        'uri' => '' 
                    ),
                    array(
                        'name' => ( intval($id) >= 0 ) ? 'Edit a Source' : 'Create a new Source',
                        'uri' => '' 
                    )
                ]
            ];
            
            return view('notfound', [ 'sidebarAndBreadcrumb' => $sidebarAndBreadcrumb, 'backlink' => [ 'url' => url('source'), 'text' => 'Back to Source List', ], ] );    
        }
        $optionData = Api::request("listSourceType");
        $selectOptionList['listSourceType'] = !empty($optionData['result']['sourceTypes']) ? $optionData['result']['sourceTypes'] : array();
        
        return view('source.detail', [ 'data' => $returnData['result'], 'selectOptionList' => $selectOptionList ]);    
    }
    
    public function getListSource(Request $request){
        $params = array(
            'status' => $request->has('status') ? intval($request->input('status')) : null,
            'offset' => $request->has('start') ? intval($request->input('start')) : 0 ,
            'limit' => $request->has('length') ? intval($request->input('length')) : 0,
        );
        $returnData = Api::request('getSource', $params);
        
        $responseData = array();
        if(!empty($returnData['result'])){
            $total = !empty($returnData['result']['total']) ? intval($returnData['result']['total']) : count($returnData['result']['sources']);
            $listData = $returnData['result']['sources'];
            $i = $request->has('start') ? intval($request->input('start')) : 0;
            if(!empty($listData)){
                foreach($listData as $key => $item){
                    $listData[$key]['order'] = $i+1;
                    $i++;       
                }
            }else{
                $listData = [];   
            }
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'recordsTotal' => $total,
                'recordsFiltered' => $total,
                'data' => $listData,
            );        
        }else{
            $responseData = array(
                'draw' => $request->has('draw') ? intval($request->input('draw')) : rand(0,1000000),
                'error' => $returnData['error']['message'],
                'data' => [],
            );    
        }
        return response()->json($responseData);
    }
    
    public function create(Request $request){  
        $data = array(
            "sourceId" => null,
            "sourceName" => $request->has('sourceName') ? $request->input('sourceName') : null,
            "sourceAbbr" => $request->has('sourceAbbr') ? $request->input('sourceAbbr') : null,
            "sourceTypeId" => $request->has('sourceTypeId') ? intval($request->input('sourceTypeId')) : null,
            "status" => $request->has('status') ? intval($request->input('status')) : 1,
            "agentId" => intval(session('userId',0)),
        );
        $returnData = Api::request('saveSource', $data);
        return response()->json($returnData);
    }
    
    public function update($id, Request $request){  
        $data = array(
            "sourceId" => intval($id),
            "sourceName" => $request->has('sourceName') ? $request->input('sourceName') : null,
            "sourceAbbr" => $request->has('sourceAbbr') ? $request->input('sourceAbbr') : null,
            "sourceTypeId" => $request->has('sourceTypeId') ? intval($request->input('sourceTypeId')) : null,
            "status" => $request->has('status') ? intval($request->input('status')) : 1,
            "agentId" => intval(session('userId',0)),
        );
        $returnData = Api::request('saveSource', $data);
        return response()->json($returnData);
    }
    
}
