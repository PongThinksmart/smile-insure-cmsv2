<tr class="centerTable">
    <input type="hidden" name="addressList[{{$id}}][addrId]" value="">
    <td class="text-center"><input type="radio" name="addressListSelect" id="addressListSelect_{{$id}}" value="{{$id}}"></td>
    <td>
        <select class="cloneDes form-control mr-2 my-1" name="addressList[{{$id}}][addrTypeId]">
        @foreach ($selectOptionList['addressTypes'] as $subitem)
        <option value="{{ $subitem['id'] }}">{{ $subitem['name'] }}</option>
        @endforeach
        </select>
    </td>
    <td><input type="text" class="cloneDes form-control-plaintext" id="addressList_{{$id}}_address" name="addressList[{{$id}}][address]" value=""></td>
    <td><input type="text" class="cloneDes form-control-plaintext" id="addressList_{{$id}}_distName" name="addressList[{{$id}}][distName]" value=""></td>
    <td><input type="text" class="cloneDes form-control-plaintext" id="addressList_{{$id}}_amphName" name="addressList[{{$id}}][amphName]" value=""></td>
    <td><input type="text" class="cloneDes form-control-plaintext" id="addressList_{{$id}}_provName" name="addressList[{{$id}}][provName]" value=""></td>
    <td><input type="text" class="cloneDes form-control-plaintext" id="addressList_{{$id}}_zipCode" name="addressList[{{$id}}][zipCode]" value=""></td>
    <td class="text-center"><i class="cloneDes listClose fas fa-times"></i></td>
</tr>